require_relative "load.rb"
require "npw/application"
require "npw/application/gn_gw"
require "npw/application/gn_nw"

class NanoWordSetting
  INVOLUTION_VSTRING = { "a" => "b", "b" => "a" }

  # a: a+, b: b+, c: a-, d: b-
  INVOLUTION_VKNOT = { "a" => "d", "b" => "c", "c" => "b", "d" => "a" }
  INVOLUTION_VKNOT_SHIFT = { "a" => "b", "b" => "a", "c" => "d", "d" => "c" }

  PROC_S_ENTIRE = Proc.new { |projections| true }
  PROC_S_DIAGONAL = Proc.new do |projections|
    projections[0] == projections[1] && projections[0] == projections[2]
  end
  VKNOT2_CORRESPONDANCE = { "a" => "c", "b" => "d", "c" => "a", "d" => "b" }
  PROC_S_VKNOT =  Proc.new do |projections|
    if projections[0] == projections[1]
      projections[1] == projections[2] || VKNOT2_CORRESPONDANCE[projections[1]] == projections[2]
    else
      projections[1] == projections[2] && VKNOT2_CORRESPONDANCE[projections[0]] == projections[1]
    end
  end

  SETTINGS =
    [
     {
       :name => "GaussOpen1",
       :class => NPW::GnOnGaussWord,
       :option => { :classification_moves => :extended },
       :message => "Open Gauss words"
     },
     {
       :name => "GaussOpen2",
       :class => NPW::GnOnGaussWord,
       :option => { :effective => true, :classification_moves => :extended },
       :message => "Open Gauss words (additional relations for effective computation)"
     },
     {
       :name => "GaussOpen3",
       :class => NPW::GnOnGaussWord,
       :option => { :ignore_3rd_relations => true },
       :message => "Open Gauss words without 3rd type relations"
     },
     {
       :name => "GaussOpen4",
       :class => NPW::GnOnGaussWord,
       :option => { :ignore_3rd_relations => true, :effective => true },
       :message => "Open Gauss words without 3rd type relations (additional relations for effective computation)"
     },
     {
       :name => "GaussClosed1",
       :class => NPW::GnOnClosedGaussWord,
       :option => { :classification_moves => :extended },
       :message => "Closed Gauss words"
     },
     {
       :name => "GaussClosed2",
       :class => NPW::GnOnClosedGaussWord,
       :option => { :effective => true, :classification_moves => :extended },
       :message => "Closed Gauss words (additional relations for effective computation)"
     },
     {
       :name => "GaussClosed3",
       :class => NPW::GnOnClosedGaussWord,
       :option => { :ignore_3rd_relations => true },
       :message => "Closed Gauss words without 3rd type relations"
     },
     {
       :name => "GaussClosed4",
       :class => NPW::GnOnClosedGaussWord,
       :option => { :ignore_3rd_relations => true, :effective => true },
       :message => "Closed Gauss words without 3rd type relations (additional relations for effective computation)"
     },
     {
       :name => "VStringOpen1",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VSTRING, PROC_S_ENTIRE],
       :message => "Open virtual string: S is entire"
     },
     {
       :name => "VStringOpen2",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VSTRING, PROC_S_DIAGONAL],
       :message => "Open virtual string: S is diagonal"
     },
     {
       :name => "VStringOpen3",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VSTRING, nil],
       :message => "Open virtual string: S is empty"
     },
     {
       :name => "VStringClosed1",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VSTRING, PROC_S_ENTIRE, INVOLUTION_VSTRING],
       :message => "Closed virtual string: S is entire"
     },
     {
       :name => "VStringClosed2",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VSTRING, PROC_S_DIAGONAL, INVOLUTION_VSTRING],
       :message => "Closed virtual string: S is diagonal"
     },
     {
       :name => "VStringClosed3",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VSTRING, nil, INVOLUTION_VSTRING],
       :message => "Closed virtual string: S is empty"
     },
     {
       :name => "VKnotOpen1",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VKNOT, PROC_S_ENTIRE],
       :message => "Open virtual knot: S is entire"
     },
     {
       :name => "VKnotOpen2",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VKNOT, PROC_S_VKNOT],
       :message => "Open virtual knot: S is specified"
     },
     {
       :name => "VKnotOpen3",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VKNOT, PROC_S_DIAGONAL],
       :message => "Open virtual knot: S is diagonal"
     },
     {
       :name => "VKnotOpen4",
       :class => NPW::GnOnNanoWord,
       :arguments => [INVOLUTION_VKNOT, nil],
       :message => "Open virtual knot: S is empty"
     },
     {
       :name => "VKnotClosed1",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VKNOT, PROC_S_ENTIRE, INVOLUTION_VKNOT_SHIFT],
       :message => "Closed virtual knot: S is entire"
     },
     {
       :name => "VKnotClosed2",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VKNOT, PROC_S_VKNOT, INVOLUTION_VKNOT_SHIFT],
       :message => "Closed virtual knot: S is specified"
     },
     {
       :name => "VKnotClosed3",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VKNOT, PROC_S_DIAGONAL, INVOLUTION_VKNOT_SHIFT],
       :message => "Closed virtual knot: S is diagonal"
     },
     {
       :name => "VKnotClosed4",
       :class => NPW::GnOnClosedNanoWord,
       :arguments => [INVOLUTION_VKNOT, nil, INVOLUTION_VKNOT_SHIFT],
       :message => "Closed virtual knot: S is empty"
     },
    ]

  def self.target_list
    SETTINGS.each do |h|
      puts "#{h[:name]}\t#{h[:message]}"
    end
  end

  def initialize(target, gw_opts)
    @gw_opts = gw_opts
    @target = target.strip
    unless @setting = SETTINGS.find { |h| h[:name] == @target }
      raise "Can not find #{@target}"
    end
  end

  def gn_obj
    if opts = @setting[:option]
      opts = opts.merge(@gw_opts)
    else
      opts = @gw_opts
    end
    @setting[:class].new(@target, *(@setting[:arguments] || []), opts)
  end

  def create(method_opts)
    method_opts[:message] = @setting[:message]
    [gn_obj, method_opts]
  end

  def invariant(path)
    obj = gn_obj
    NPW::FiniteTypeInvariant.new(path) do |str|
      gn_obj.parse_generator_string(str)
    end
  end
end
