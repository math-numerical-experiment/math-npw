require "subcommand_optparse"
require_relative "settings.rb"

def parse_argv(args, gw_opts, method_opts)
  target = args.shift
  n = args.shift.to_i
  raise if !target || n <= 1
  gn, method_opts = NanoWordSetting.new(target, gw_opts).create(method_opts)
  [gn, n, method_opts]
end

method_opts = {}
gw_opts = {}

parser = SubCmdOptParser.new(:help_command => true) do |sc|
  sc.program_name = "tpa.rb"
  sc.summary = "Determine group structures of truncated Polyak algebras."
  sc.version = "0.0.2"

  sc.global_option do |opt|
    opt.on("--process NUM", Integer, "Set number of processes in order to compute") do |v|
      NPW::Settings.max_process_number = v
    end
  end
  sc.subcommand("target-list", "Show list of word types: pairs of target name and description")
  sc.subcommand("index-dump", "Dump index nontext databases") do |opt|
    opt.usage = "ruby tpa.rb index-dump INDEX_DIR"
  end
  sc.subcommand("group-structure", "Determine group structure") do |opt|
    opt.usage = "ruby tpa.rb group-structure TARGET RANK [-o GRP_DIR]"
    opt.example = <<EXAMPLE
* Use index
  ruby tpa.rb group-structure TARGET RANK -i INDEX_DIR [-o GRP_DIR]
EXAMPLE
    opt.on("-o DIR", "--output DIR", String, "Set the output directory") do |v|
      method_opts[:output] = v
    end
    opt.on("-l", "--load", "Load data directory") do |v|
      method_opts[:load] = v
    end
    opt.on("--db", "Use database to create a matrix") do |v|
      method_opts[:db] = v
    end
    opt.on("--gap MEM", String, "Use GAP to get Smith normal form") do |v|
      gw_opts[:gap] = { :memory => v }
    end
    opt.on("--relations", "Only put relations and generators") do |v|
      method_opts[:type] = :relations
    end
    opt.on("-i INDEX_DIR", '--index INDEX_DIR', String, "Calculate from index data") do |v|
      gw_opts[:index] = v
    end
    opt.on("-c CLASSIFY_DIR", '--classify CLASSIFY_DIR', String, "Use classification database") do |v|
      method_opts[:classify] = v
    end
  end
  sc.subcommand("transformation", "Calculation transformation into SNF") do |opt|
    opt.usage = "ruby tpa.rb transformation TARGET RANK GRP_DIR"
    opt.on("--db", "Use database to create a matrix") do |v|
      method_opts[:db] = v
    end
    opt.on("--gap MEM", String, "Use GAP to get Smith normal form") do |v|
      gw_opts[:gap] = { :memory => v }
    end
  end
  sc.subcommand("index-create", "Create index") do |v|
    opt.usage = "ruby tpa.rb index-create TARGET RANK INDEX_DIR"
  end
  sc.subcommand("classify-create-db", "Create classification databases") do |opt|
    opt.usage = "ruby tpa.rb classify-classify-create-db TARGET RANK DB_DIR"
    opt.on("--progress NUM", Integer, "Output progress per the specified step") do |v|
      method_opts[:progress] = v
    end
  end
  sc.subcommand("classify-dump-db", "Dump classification database which may not be complete") do |opt|
    opt.usage = "ruby tpa.rb classify-dump-db DB_DIR [OUTPUT]"
  end
  sc.subcommand("classify-compare", "Compare two classification databases") do |opt|
    opt.usage = "ruby tpa.rb classify-compare DB_DIR1 DB_DIR2"
  end
  sc.subcommand("invariant-test", "Confirm that the invariant is valid") do |opt|
    opt.usage = "ruby tpa.rb invariant-test TARGET TSV_PATH DB_DIR"
  end
  sc.subcommand("invariant-classify", "Get invariant values of each classes in the database") do |opt|
    opt.usage = "ruby tpa.rb invariant-classify TARGET TSV_PATH DB_DIR"
    opt.on("--rank NUM", Integer, "Maximum rank number of target words") do |v|
      method_opts[:rank_max] = v
    end
  end
end
subcmd = parser.parse!

case subcmd
when "target-list"
  NanoWordSetting.target_list
when "index-dump"
  gw_opts[:index] = ARGV[0]
  raise "Index is not specified" unless gw_opts[:index]
  name = File.read(File.join(gw_opts[:index], "NAME")).strip
  gn, method_opts = NanoWordSetting.new(name, gw_opts).create({})
  gn.dump_index
when "group-structure"
  gn, n, method_opts = parse_argv(ARGV, gw_opts, method_opts)
  dir = gn.calc(n, method_opts)
  system("cat", File.join(dir, "README"))
when "transformation"
  gn, n, method_opts = parse_argv(ARGV, gw_opts, method_opts)
  raise unless dir = ARGV.shift
  gn.calc_transformation(n, dir, method_opts[:db])
when "index-create"
  gw_opts[:index] = ARGV[2] || gw_opts[:index]
  raise "Index is not specified" unless gw_opts[:index]
  gn, n, method_opts = parse_argv(ARGV, gw_opts, method_opts)
  gn.create_index(n, method_opts)
when "classify-create-db"
  method_opts[:progress] = 1000
  unless db_path = ARGV.unshift
    raise "Database path is not specified"
  end
  gn, n, tmp = parse_argv(ARGV, gw_opts, method_opts)
  path = gn.word_classify.save_classification(n, db_path, method_opts)
  puts "#{path} has been created"
when "classify-dump-db"
  db_path = ARGV.shift
  out = ARGV.shift
  unless (db_path && out)
    raise "Database path and output path are not specfied"
  end
  NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(db_path).dump(out)
when "classify-compare"
  db_path1 = ARGV.shift
  db_path2 = ARGV.shift
  unless (db_path1 && db_path2)
    raise "Two database paths are not specified"
  end
  db1 = NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(db_path1)
  db2 = NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(db_path2)
  if NPW::TruncatedPolyakAlgebra::WordClassify::DB.include?(db1, db2)
    puts "#{db_path1} includes word classes in #{db_path2}"
  else
    puts "#{db_path1} does NOT include word classes in #{db_path2}"
  end
when "invariant-test"
  unless ARGV.size == 3
    raise "Three arguments are needed"
  end
  target = ARGV.shift
  tsv_path = ARGV.shift
  db_path = ARGV.shift
  gn = NanoWordSetting.new(target, gw_opts).gn_obj
  if gn.word_classify.invariant_compatible?(tsv_path, db_path)
    puts "The invariant is compatible with words in the database."
  else
    puts "The invariant is NOT compatible with words in the database."
  end
when "invariant-classify"
  unless ARGV.size == 3
    raise "Three arguments are needed"
  end
  target = ARGV.shift
  tsv_path = ARGV.shift
  db_path = ARGV.shift
  gn = NanoWordSetting.new(target, gw_opts).gn_obj
  if gn.word_classify.invariant_classify?(tsv_path, db_path, method_opts)
    puts "The invariant completely classify words in the database."
  else
    puts "The invariant does NOT completely classify words in the database."
  end
end
