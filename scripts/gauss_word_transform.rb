require_relative "load.rb"

# Parse options.
require 'optparse'

help_message =<<HELP
Usage: ruby #{File.basename(__FILE__)}
HELP

opts_print_moves = {}

begin
  OptionParser.new(help_message) do |opt|
    # on(short [, klass [, pat [, desc]]]) {|v| ...}
    # on(long [, klass [, pat [, desc]]) {|v| ...}
    # on(short, long [, klass [, pat [, desc]]]) {|v| ...}
    opt.on("--use-extended-moves", "Use moves created by some combinations of basic moves") do |v|
      opts_print_moves[:move] = :all
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

puts <<TEXT
Transform a word to a form whose letters are A, B, C, ..., Z and shorten it by moves.
Note that we apply only moves that does not increase lengths of words.
To exit, write Ctrl-D.
TEXT

loop do
  puts "Please input Gauss word:"
  break unless str = gets
  w = NPW::Word.new(str.strip)
  if w.gauss_word?
    w.print_moves(opts_print_moves)
  else
    puts "#{w} is not Gauss word."
    next
  end
end
