require_relative "../load.rb"
require "npw/application"
require "npw/application/gn_gw"
require "npw/application/polyak_algebra/finite_type_invariant"

# Parse options.
require 'optparse'

help_message =<<HELP
Usage: ruby #{File.basename(__FILE__)} TABLE_INVARIANT [...]
HELP

word_classify_opts = {}
save_words = nil
exec_test = false
classify_output = nil

begin
  OptionParser.new(help_message) do |opt|
    opt.on("-s FILE", "--set FILE", String, "Use a file of classified words.") do |v|
      word_classify_opts[:load] = v
    end
    opt.on("-r RANK", "--rank RANK", String, "Rank arguments.") do |v|
      if /^(\d)|(\d\.+\d)$/ =~ v
        rank = eval(v)
      else
        rank = v.to_i
      end
      word_classify_opts[:rank] = rank
    end
    opt.on("--target FILE", String, "Use a file of not classified words") do |v|
      word_classify_opts[:file_words] = v
    end
    opt.on("--save OUT", String, "Save words homotopic each other.") do |v|
      save_words = v
    end
    opt.on("--shift", "Use shift move.") do |v|
      word_classify_opts[:shift] = true
    end
    opt.on("--test", "Test invariant.") do |v|
      exec_test = true
    end
    opt.on("--classify OUT", String, "Classify given sets of words.") do |v|
      classify_output = v
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

invariants = ARGV.map do |path|
  NPW::FiniteTypeInvariant.new(path) do |str|
    NPW::Word.new(str)
  end
end

word_classify = NPW::Word::GaussWordClassify.new(word_classify_opts)
if save_words
  if word_classify.save(save_words)
    puts "Save #{save_words}"
  else
    puts "Not save. File #{save_words} exists."
  end
elsif exec_test
  word_classify.check_invariant(*invariants)
elsif classify_output
  word_classify.exec(invariants, classify_output)
else
  puts "Nothing is done"
end
