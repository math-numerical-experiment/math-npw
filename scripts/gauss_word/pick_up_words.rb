require_relative "../load.rb"

rank = nil
if /^\d+$/ =~ ARGV[0]
  rank = [ARGV[0].to_i]
elsif /^\d+\.\.\d+$/ =~ ARGV[0]
  rank = eval(ARGV[0])
end
file = ARGV[1]

if !rank || !file
  puts "Pick up words of specified rank."
  puts "Usage: ruby #{__FILE__} RANK FILE"
  exit
end

open(file, "r") do |f|
  while l = f.gets
    ary = l.split
    ary.delete_if { |s| !rank.include?(s.length / 2) }
    unless ary.empty?
      puts ary.join(" ")
    end
  end
end
