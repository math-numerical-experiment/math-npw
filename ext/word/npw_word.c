#include "npw_word.h"

static VALUE r_defaultalphabet;
static char *letters;

static void init_variables ()
{
  int i, len;
  char *s;
  VALUE const_letters, str;
  r_defaultalphabet = rb_define_module_under(rb_define_class_under(rb_define_module("NPW"), "Word", rb_cObject), "DefaultAlphabet");
  const_letters = rb_const_get_at(r_defaultalphabet, rb_intern("LETTERS"));
  len = RARRAY_LEN(const_letters);
  letters = (char *) malloc(sizeof(char) * (len + 1));
  for (i = 0; i < len; i++) {
    str = rb_ary_entry(const_letters, i);
    s = StringValuePtr(str);
    letters[i] = s[0];
  }
  letters[len] = '\0';
}

static guint hash_char (gconstpointer v)
{
  return (guint) ((char *) v)[0];
}

static gboolean key_char_equal (gconstpointer v1, gconstpointer v2)
{
  return ((char *) v1)[0] == ((char *) v2)[0];
}

static VALUE adjust_string(VALUE str, GHashTable *table)
{
  VALUE word_new;
  char *s, *s_new;
  int i, len, nth_letter;
  gpointer val;
  s = StringValuePtr(str);
  len = (int)strlen(s);
  word_new = rb_str_new(0, len);
  s_new = RSTRING_PTR(word_new);
  nth_letter = 0;
  for (i = 0; i < len; i++) {
    val = g_hash_table_lookup(table, s + i);
    if (!val) {
      val = (gpointer) (letters + nth_letter);
      nth_letter += 1;
      g_hash_table_insert(table, (gpointer) (s + i), val);
    }
    s_new[i] = ((char *) val)[0];
  }
  return rb_ary_new3(2, word_new, INT2NUM(nth_letter));
}

static VALUE r_defaultalphabet_adjust_string(VALUE self, VALUE str)
{
  VALUE ary;
  GHashTable *table;
  table = g_hash_table_new(hash_char, key_char_equal);
  ary = adjust_string(str, table);
  g_hash_table_destroy(table);
  return ary;
}

static void hash_set_values (gpointer key_ptr, gpointer value_ptr, gpointer hash_ptr)
{
  VALUE hash;
  char *key, *value;
  key = (char *) key_ptr;
  value = (char *) value_ptr;
  hash = (VALUE) hash_ptr;
  rb_hash_aset(hash, rb_str_new(key, 1), rb_str_new(value, 1));
}

static VALUE r_defaultalphabet_adjust_string_with_transformation(VALUE self, VALUE str)
{
  VALUE ary, h;
  GHashTable *table;
  table = g_hash_table_new(hash_char, key_char_equal);
  ary = adjust_string(str, table);
  h = rb_hash_new();
  g_hash_table_foreach(table, hash_set_values, (gpointer) h);
  g_hash_table_destroy(table);
  return rb_ary_push(ary, h);
}

void Init_npw_word ()
{
  init_variables();
  rb_define_singleton_method(r_defaultalphabet, "adjust_string", r_defaultalphabet_adjust_string, 1);
  rb_define_singleton_method(r_defaultalphabet, "adjust_string_with_transformation", r_defaultalphabet_adjust_string_with_transformation, 1);
}
