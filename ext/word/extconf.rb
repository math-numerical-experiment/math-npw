require 'mkmf'

$CFLAGS << " " << `pkg-config glib-2.0 --cflags`.strip << " "

if have_library("glib-2.0")
  create_makefile("npw_word")
end
