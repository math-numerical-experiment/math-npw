#include <kcpolydb.h>
#include <kclangc.h>

namespace kc = kyotocabinet;

extern "C" {

#include <ruby.h>

typedef std::map<std::string, std::string> StringMap;
typedef VALUE (*METHOD)(...);

typedef struct __db_cache {
  int **ptr;
  int *ptr_size;
  int size;
} DBCache;

static VALUE module_npw, class_matrix_db, class_matrix_db_cache;

static void db_cache_free (void *p)
{
  int i;
  DBCache *cache;
  cache = (DBCache *) p;
  if (cache->size > 0) {
    for (i = 0; i < cache->size; i++) {
      if (cache->ptr[i]) {
        free(cache->ptr[i]);
      }
    }
  }
  free(cache->ptr_size);
  free(cache);
}

static VALUE db_cache_alloc (VALUE self)
{
  DBCache *cache;
  self = Data_Make_Struct(class_matrix_db_cache, DBCache, 0, db_cache_free, cache);
  cache->size = 0;
  return self;
}

static VALUE db_cache_initialize (VALUE self, VALUE path_str, VALUE num_size)
{
  int i, max_num, *vals;
  size_t vsiz;
  DBCache *cache;
  kc::PolyDB* db;
  const char* path;
  Data_Get_Struct(self, DBCache, cache);
  cache->size = NUM2INT(num_size);
  cache->ptr_size = ALLOC_N(int, cache->size);
  cache->ptr = ALLOC_N(int*, cache->size);
  db = new kc::PolyDB();
  path = RSTRING_PTR(path_str);
  if (!db->open(path, kc::PolyDB::OREADER)) {
    rb_raise(rb_eStandardError, "Can not open to read: %s", path);
  }

  for (i = 0; i < cache->size; i++) {
    vals = (int *) db->get((const char *) &i, sizeof(int), &vsiz);
    if (vals) {
      max_num = (int) (vsiz / (sizeof(int)));
      cache->ptr[i] = vals;
      cache->ptr_size[i] = max_num;
    } else {
      cache->ptr_size[i] = 0;
      cache->ptr[i] = NULL;
    }
  }

  if (!db->close()) {
    rb_raise(rb_eStandardError, "Can not close: %s", path);
  }
  delete db;
  return Qtrue;
}

static VALUE db_cache_upper_triangular_diagonal_ones_p (VALUE self)
{
  int i;
  DBCache *cache;
  Data_Get_Struct(self, DBCache, cache);
  for (i = 0; i < cache->size; i++) {
    if (cache->ptr_size[i] < 2 || cache->ptr[i][0] != i || cache->ptr[i][1] != 1) {
      return Qfalse;
    }
  }
  return Qtrue;
}

static VALUE db_cache_debug_print (VALUE self)
{
  int i, j;
  DBCache *cache;
  Data_Get_Struct(self, DBCache, cache);
  for (i = 0; i < cache->size; i++) {
    for (j = 0; j < cache->ptr_size[i]; j++) {
      if (j > 0) {
        printf(" ");
      }
      printf("%d", cache->ptr[i][j]);
    }
    printf("\n");
  }
  return Qtrue;
}

static void dump_matrix_packed_data (kc::PolyDB *db, const char *path, StringMap *mat_packed_data)
{
  if (!db->open(path, NUM2INT(rb_const_get(class_matrix_db, rb_intern("KC_OPEN_WRITE_MODE"))))) {
    rb_raise(rb_eStandardError, "Can not open to read: %s\n", path);
  }
  if (!db->set_bulk(*mat_packed_data, true)) {
    rb_raise(rb_eStandardError, "Can not set bulk\n");
  }
  if (!db->close()) {
    rb_raise(rb_eStandardError, "Can not close: %s\n", path);
  }
  mat_packed_data->clear();
}

static VALUE matrix_db_inverse_of_upper_triangular_step (VALUE self, VALUE db_cache, VALUE output_path, VALUE calc_step_size, VALUE calc_step_num)
{
  uint max_size_matrix_data;
  int i, row_size, row_num, ind_one, step_size, step_num, val_new, *row_vals, val_ind, res_ind, ind, ind2, *packed_nums, column_vals_new_rev_size;
  kc::PolyDB* db;
  const char* path;
  StringMap mat_packed_data;
  std::vector<int> column_vals_new_rev;
  DBCache *cache;
  Data_Get_Struct(db_cache, DBCache, cache);
  max_size_matrix_data = NUM2INT(rb_const_get(class_matrix_db, rb_intern("MAX_SIZE_MATRIX_DATA")));
  step_size = NUM2INT(calc_step_size);
  step_num = NUM2INT(calc_step_num);
  db = new kc::PolyDB();
  path = RSTRING_PTR(output_path);
  row_size = cache->size;
  packed_nums = ALLOC_N(int, row_size * 2);
  ind_one = row_size - 1;
  ind_one = ind_one - ind_one % step_size + step_num;
  if (ind_one >= row_size) {
    ind_one = ind_one - step_size;
  }
  for (; ind_one >= 0; ind_one -= step_size) {
    column_vals_new_rev.clear();
    row_num = ind_one;
    while (row_num >= 0) {
      val_new = (ind_one == row_num ? 1 : 0);
      row_vals = cache->ptr[row_num];
      val_ind = 2;
      res_ind = column_vals_new_rev.size() - 2;
      while (val_ind < cache->ptr_size[row_num]) {
        ind = row_vals[val_ind];
        while (res_ind >= 0) {
          ind2 = column_vals_new_rev[res_ind];
          if (ind <= ind2) {
            if (ind == ind2) {
              val_new = val_new - row_vals[val_ind + 1] * column_vals_new_rev[res_ind + 1];
              res_ind -= 2;
            }
            break;
          }
          res_ind -= 2;
        }
        val_ind += 2;
      }
      if (val_new != 0) {
        column_vals_new_rev.push_back(row_num);
        column_vals_new_rev.push_back(val_new);
      }
      row_num -= 1;
    }
    if (column_vals_new_rev.size() > 0) {
      column_vals_new_rev_size = column_vals_new_rev.size();
      for (i = 0; i < column_vals_new_rev_size; i += 2) {
        packed_nums[i] = column_vals_new_rev[column_vals_new_rev_size - i - 2];
        packed_nums[i + 1] = column_vals_new_rev[column_vals_new_rev_size - i - 1];
      }
      mat_packed_data[std::string((const char *) &ind_one, sizeof(int))] = std::string((const char *) packed_nums, sizeof(int) * column_vals_new_rev_size);
      if (mat_packed_data.size() > max_size_matrix_data) {
        dump_matrix_packed_data (db, path, &mat_packed_data);
      }
    }
  }
  if (mat_packed_data.size() > 0) {
    dump_matrix_packed_data (db, path, &mat_packed_data);
  }
  delete db;
  free(packed_nums);
  return Qtrue;
}

// db should be open beforehand.
// row_vals_new should be allocated sufficiently beforehand.
static void matrix_db_row_multiplied_by_other (int row_num, kc::PolyDB *db, DBCache *cache_other, int *row_vals_new, int *row_vals_new_size)
{
  int i, *row_vals, column_num, val_new, col_num, *column_vals, column_size, ind, ind2, ind_max;
  size_t vsiz;
  row_vals = (int *) db->get((const char *) &row_num, sizeof(int), &vsiz);
  ind_max = vsiz / sizeof(int);
  *row_vals_new_size = 0;
  if (row_vals) {
    for (column_num = 0; column_num < cache_other->size; column_num++) {
      column_size = cache_other->ptr_size[column_num];
      if (column_size > 0) {
        column_vals = cache_other->ptr[column_num];
        val_new = 0;
        col_num = 0;
        for (i = 0; i < ind_max; i += 2) {
          ind = row_vals[i];
          while (col_num < column_size) {
            ind2 = column_vals[col_num];
            if (ind < ind2) {
              break;
            }
            col_num += 2;
            if (ind == ind2) {
              val_new += (column_vals[col_num - 1] * row_vals[i + 1]);
              break;
            }
          }
          if (col_num >= column_size) {
            break;
          }
        }
        if (val_new != 0) {
          row_vals_new[*row_vals_new_size] = column_num;
          row_vals_new[*row_vals_new_size + 1] = val_new;
          *row_vals_new_size += 2;
        }
      }
    }
    free(row_vals);
  }
}

static VALUE matrix_db_mul_rows (VALUE self, VALUE self_path, VALUE row_size_self, VALUE other_db_cache, VALUE output_path, VALUE calc_step_size, VALUE calc_step_num)
{
  uint max_size_matrix_data;
  int row_size, row_num, step_size, step_num, *row_vals_new, row_vals_new_size;
  kc::PolyDB *db_self, *db_res;
  const char *path_self_chars, *path_output_chars;
  StringMap mat_packed_data;
  std::vector<int> column_vals_new_rev;
  DBCache *cache_other;
  Data_Get_Struct(other_db_cache, DBCache, cache_other);
  max_size_matrix_data = NUM2INT(rb_const_get(class_matrix_db, rb_intern("MAX_SIZE_MATRIX_DATA")));
  step_size = NUM2INT(calc_step_size);
  step_num = NUM2INT(calc_step_num);
  db_self = new kc::PolyDB();
  db_res = new kc::PolyDB();
  row_size = NUM2INT(row_size_self);
  row_vals_new = ALLOC_N(int, cache_other->size * 2);
  path_self_chars = RSTRING_PTR(self_path);
  path_output_chars = RSTRING_PTR(output_path);
  if (!db_self->open(path_self_chars, kc::PolyDB::OREADER)) {
    rb_raise(rb_eStandardError, "Can not open to read: %s", path_self_chars);
  }
  for (row_num = step_num; row_num < row_size; row_num += step_size) {
    matrix_db_row_multiplied_by_other (row_num, db_self, cache_other, row_vals_new, &row_vals_new_size);
    if (row_vals_new_size > 0) {
      mat_packed_data[std::string((const char *) &row_num, sizeof(int))] = std::string((const char *) row_vals_new, sizeof(int) * row_vals_new_size);
      if (mat_packed_data.size() > max_size_matrix_data) {
        dump_matrix_packed_data (db_res, path_output_chars, &mat_packed_data);
      }
    }
  }
  if (mat_packed_data.size() > 0) {
    dump_matrix_packed_data (db_res, path_output_chars, &mat_packed_data);
  }
  if (!db_self->close()) {
    rb_raise(rb_eStandardError, "Can not close: %s", path_self_chars);
  }
  delete db_self;
  delete db_res;
  free(row_vals_new);
  return Qtrue;
}

static void matrix_db_reduce_by_additional_rels (kc::PolyDB *db_add, int *row_vals_new, int *row_vals_new_size)
{
  int i, ind, val, *num_same;
  size_t vsiz;
  std::map<int, int> row_data;
  std::map<int, int>::const_iterator iterator;
  for (i = 0; i < *row_vals_new_size; i += 2) {
    ind = row_vals_new[i];
    val = row_vals_new[i + 1];
    num_same = (int *) db_add->get((const char *) &ind, sizeof(int), &vsiz);
    if (num_same) {
      if (*num_same >= 0) {
        iterator = row_data.find(*num_same);
        row_data[*num_same] = (iterator != row_data.end() ? iterator->second + val : val);
      }
      free(num_same);
    } else {
      iterator = row_data.find(ind);
      row_data[ind] = (iterator != row_data.end() ? iterator->second + val : val);
    }
  }
  i = 0;
  for (iterator = row_data.begin(); iterator != row_data.end(); iterator++) {
    val = iterator->second;
    if (val != 0) {
      row_vals_new[i] = iterator->first;
      row_vals_new[i + 1] = val;
      i += 2;
    }
  }
  *row_vals_new_size = i;
  if (*row_vals_new_size > 0) {
    if (row_vals_new[1] < 0) {
      for (i = 1; i < *row_vals_new_size; i += 2) {
        row_vals_new[i] = -row_vals_new[i];
      }
    }
  }
}

static VALUE matrix_db_rows_transform_by (VALUE self, VALUE self_path, VALUE row_size_self, VALUE other_db_cache, VALUE output_path, VALUE additional_rels_path, VALUE row_num_already_set_output, VALUE calc_step_size, VALUE calc_step_num)
{
  uint max_size_matrix_data;
  int row_size, row_num, step_size, step_num, *row_vals_new, row_vals_new_size, row_num_already_set, row_num_new;
  kc::PolyDB *db_self, *db_res, *db_add;
  const char *path_self_chars, *path_output_chars, *path_additional_rels_chars;
  StringMap mat_packed_data;
  std::vector<int> column_vals_new_rev;
  DBCache *cache_other;
  Data_Get_Struct(other_db_cache, DBCache, cache_other);
  max_size_matrix_data = NUM2INT(rb_const_get(class_matrix_db, rb_intern("MAX_SIZE_MATRIX_DATA")));
  step_size = NUM2INT(calc_step_size);
  step_num = NUM2INT(calc_step_num);
  db_self = new kc::PolyDB();
  db_res = new kc::PolyDB();
  db_add = new kc::PolyDB();
  row_size = NUM2INT(row_size_self);
  row_vals_new = ALLOC_N(int, cache_other->size * 2);
  path_self_chars = RSTRING_PTR(self_path);
  path_output_chars = RSTRING_PTR(output_path);
  path_additional_rels_chars = RSTRING_PTR(additional_rels_path);
  row_num_already_set = NUM2INT(row_num_already_set_output);
  if (!db_self->open(path_self_chars, kc::PolyDB::OREADER)) {
    rb_raise(rb_eStandardError, "Can not open to read: %s", path_self_chars);
  }
  if (!db_add->open(path_additional_rels_chars, kc::PolyDB::OREADER)) {
    rb_raise(rb_eStandardError, "Can not open to read: %s", path_additional_rels_chars);
  }
  for (row_num = step_num; row_num < row_size; row_num += step_size) {
    matrix_db_row_multiplied_by_other(row_num, db_self, cache_other, row_vals_new, &row_vals_new_size);
    matrix_db_reduce_by_additional_rels(db_add, row_vals_new, &row_vals_new_size);
    if (row_vals_new_size > 0) {
      row_num_new = row_num + row_num_already_set;
      mat_packed_data[std::string((const char *) &row_num_new, sizeof(int))] = std::string((const char *) row_vals_new, sizeof(int) * row_vals_new_size);
      if (mat_packed_data.size() > max_size_matrix_data) {
        dump_matrix_packed_data (db_res, path_output_chars, &mat_packed_data);
      }
    }
  }
  if (mat_packed_data.size() > 0) {
    dump_matrix_packed_data (db_res, path_output_chars, &mat_packed_data);
  }
  if (!db_self->close()) {
    rb_raise(rb_eStandardError, "Can not close: %s", path_self_chars);
  }
  if (!db_add->close()) {
    rb_raise(rb_eStandardError, "Can not close: %s", path_additional_rels_chars);
  }
  delete db_self;
  delete db_res;
  delete db_add;
  free(row_vals_new);
  return Qtrue;
}

void Init_npw_matrix_db ()
{
  module_npw = rb_define_module("NPW");
  class_matrix_db = rb_define_class_under(module_npw, "MatrixDB", rb_cObject);
  rb_define_method(class_matrix_db, "inverse_of_upper_triangular_step", (METHOD) matrix_db_inverse_of_upper_triangular_step, 4);
  rb_define_method(class_matrix_db, "mul_rows", (METHOD) matrix_db_mul_rows, 6);
  rb_define_method(class_matrix_db, "rows_transform_by", (METHOD) matrix_db_rows_transform_by, 8);

  class_matrix_db_cache = rb_define_class_under(class_matrix_db, "Cache", rb_cObject);
  rb_define_alloc_func(class_matrix_db_cache, db_cache_alloc);
  rb_define_private_method(class_matrix_db_cache, "initialize", (METHOD) db_cache_initialize, 2);
  rb_define_method(class_matrix_db_cache, "upper_triangular_diagonal_ones?", (METHOD) db_cache_upper_triangular_diagonal_ones_p, 0);
  rb_define_method(class_matrix_db_cache, "debug_print", (METHOD) db_cache_debug_print, 0);
}

}
