require "npw/version"
require "active_support/core_ext/hash"
require "rainbow"

autoload :FileUtils, "fileutils"

module NPW
  MARK_EMPTY = "@"
  MARK_SPLITTER = "|"

  autoload :Settings, "npw/settings"
  autoload :Word, "npw/word"
  autoload :Parser, "npw/parser"
  autoload :Relation, "npw/relation"
  autoload :NanoWord, "npw/nanoword"
  autoload :DBKyotoCabinet, "npw/utils/kyotocabinet"
end
