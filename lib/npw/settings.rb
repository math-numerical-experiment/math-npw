module NPW
  module Settings
    def self.max_process_number=(num)
      @max_process_number = num
      @max_process_number = 1 if @max_process_number <= 0
      @max_process_number
    end

    def self.max_process_number
      unless @max_process_number
        # n = `cat /proc/cpuinfo | grep processor | wc -l`.to_i
        n = `grep -e "core id" /proc/cpuinfo | sort | uniq | wc -l`.to_i
        self.max_process_number = n
      end
      @max_process_number
    end
  end
end
