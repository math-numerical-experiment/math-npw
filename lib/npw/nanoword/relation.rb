module NPW
  class NanoWord
    class Relation < NPW::Relation
      def self.parser
        @parser ||= NPW::Parser::NWRelationParser.new
      end

      def term_create(arg)
        NPW::NanoWord.new(*arg, @involution)
      end

      def initialize(str, involution, opts = {})
        @involution = involution
        super(str, opts)
      end
    end
  end
end
