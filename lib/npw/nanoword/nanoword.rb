require "forwardable"

module NPW
  # Note that Gauss words of NPW::NanoWord are adjusted sequence of letters.
  class NanoWord
    attr_reader :gauss_word, :alpha_alphabet, :involution

    include Comparable
    extend Forwardable

    def_delegator :@gauss_word, :empty?, :empty?
    def_delegator :@gauss_word, :length, :length
    def_delegator :@gauss_word, :rank, :rank

    # @param [String] gauss_word String of Gauss word
    # @param [String] a_alphabet String of alpha-alphabet
    # @param [Hash] involution Hash expressing an involution of alpha-alphabet
    def initialize(gauss_word, a_alphabet, involution)
      @gauss_word = (NPW::Word === gauss_word ? gauss_word : NPW::Word.new(gauss_word)).adjust_alphabet!
      @alpha_alphabet = a_alphabet
      projection                # To check arguments
      @involution = involution
    end

    def projection
      unless @projection
        @projection = {}
        unless @gauss_word.empty?
          i = 0
          @gauss_word.alphabet.each do |letter|
            unless @projection[letter] = @alpha_alphabet[i]
              raise "Invalid alpha-alphabet: #{@alpha_alphabet} for #{@gauss_word}"
            end
            i += 1
          end
          if @alpha_alphabet[i]
            raise "Invalid alpha-alphabet: #{@alpha_alphabet} for #{@gauss_word}"
          end
        end
      end
      @projection
    end

    def to_s
      "#{gauss_word.to_s}:#{alpha_alphabet}"
    end

    def has_same_involution?(other)
      @involution.equal?(other.involution)
    end

    def <=>(other)
      unless has_same_involution?(other)
        raise "We can not compare nanowords with distinct involutions"
      end
      cmp = @gauss_word <=> other.gauss_word
      return cmp if cmp != 0
      cmp = (@alpha_alphabet <=> other.alpha_alphabet)
    end

    def ==(other)
      @gauss_word == other.gauss_word && @alpha_alphabet == other.alpha_alphabet
    end

    def eql?(other)
      self == other
    end

    def hash
      raise "Not yet implemented"
      # self.to_s.hash
    end

    # Note:
    # Gauss words in nanowords must be adjust alphabets.
    # Equality of involution is checked by object ID.
    # So even if two projections are same value of hash object
    # this method returns false.
    def isomorphic?(other)
      self == other && has_same_involution?(other)
    end

    def transform_word_projection(opts)
      a_alphabet_ary = @alpha_alphabet.each_char.to_a
      gw = @gauss_word.dup
      if exchange_nums = opts[:exchange]
        gw.transform!(:exchange => exchange_nums)
        word_new, alpha_alphabet, h = NPW::Word::DefaultAlphabet.adjust_with_transformation(gw.to_s)
        ary = ((h.delete_if { |h, k| h == k }).map { |ary| ary.sort! }).uniq!
        ary.map! { |ary| ary.map! { |letter| alpha_alphabet.find_index(letter) } }
        ary.each do |n1, n2|
          w = a_alphabet_ary[n1]
          a_alphabet_ary[n1] = a_alphabet_ary[n2]
          a_alphabet_ary[n2] = w
        end
      end
      if letters = opts[:delete]
        letters = letters.each_char.to_a if String === letters
        gw.transform!(:delete => letters)
        nums = letters.map { |letter| @gauss_word.alphabet.find_index(letter) }
        nums.sort.reverse.each do |n|
          a_alphabet_ary.delete_at(n)
        end
      end
      if involution_shift = opts[:shift]
        shifted_letter = gw.to_s[0]
        gw.transform!(:shift => true)
        a_alphabet_ary_new = []
        appear = {}
        gw.to_s.each_char do |c|
          unless appear[c]
            appear[c] = true
            if c == shifted_letter
              a_alphabet_ary_new << involution_shift[projection[c]]
            else
              a_alphabet_ary_new << projection[c]
            end
          end
        end
        a_alphabet_ary = a_alphabet_ary_new
      end
      gw.adjust_alphabet!
      [gw, a_alphabet_ary.join, @involution]
    end
    private :transform_word_projection

    # @option opts [Array] :exchange An array of numbers of positions meaning letters to be exchanged
    # @option opts [Array,String] :delete Letters to be deleted
    # @option opts [Proc] :shift Shift by the specified involution
    def transform(opts = {})
      @projection = nil
      self.class.new(*transform_word_projection(opts))
    end

    def transform!(opts = {})
      @projection = nil
      @gauss_word, @alpha_alphabet, @involution = transform_word_projection(opts)
      self
    end

    # Note that @gauss_word in NPW::NanoWord must be always adjusted.
    def adjust_alphabet
      self.dup
    end

    def adjust_alphabet!
      self
    end

    def project_letters(*nums)
      nums.map { |n| projection[@gauss_word.to_s[n]] }
    end

    def words_by_shift(involution_shift)
      words = []
      if @gauss_word.to_s.size > 1
        (@gauss_word.to_s.size - 1).times do |i|
          words << (words[-1] || self).transform(:shift => involution_shift)
        end
      end
      words
    end

    # Return size of Gauss word
    def size
      @gauss_word.size
    end

    class NWGenerator
      # @option [Hash] involution Hash of mapping letters
      # @option [Hash] each_gauss_word_opts Option of NPW::Word.each_gauss_word
      def initialize(involution, each_gauss_word_opts = {})
        @involution = involution
        @alphabets = @involution.keys.sort.reverse
        @each_gauss_word_opts = each_gauss_word_opts
        @alpha_alphabet_cache = {}
      end

      def each_alpha_alphabet(rank, &block)
        return to_enum(:each_alpha_alphabet, rank) unless block_given?
        stack = @alphabets.dup
        until stack.empty?
          str = stack.pop
          if str.size == rank
            yield(str)
          else
            @alphabets.each do |s|
              stack << (str + s)
            end
          end
        end
      end
      private :each_alpha_alphabet

      def alpha_alphabet_cache(rank)
        unless @alpha_alphabet_cache[rank]
          @alpha_alphabet_cache[rank] = each_alpha_alphabet(rank).to_a
        end
        @alpha_alphabet_cache[rank]
      end
      private :alpha_alphabet_cache

      def generate(&block)
        return to_enum(:generate) unless block_given?
        NPW::Word.each_gauss_word(@each_gauss_word_opts) do |gauss_word|
          alpha_alphabet_cache(gauss_word.rank).each do |al|
            yield(NPW::NanoWord.new(gauss_word, al, @involution))
          end
        end
      end
    end

    def self.each_nanoword(involution, opts = {}, &block)
      NWGenerator.new(involution, opts).generate(&block)
    end
  end
end
