require "kyotocabinet"

class KyotoCabinet::DB
  def open_with_error_check(path, mode)
    unless self.open(path, mode)
      raise "Open error: #{self.error}"
    end
  end

  def close_with_error_check
    unless self.close
      raise "Close error: #{self.error}"
    end
  end

  def set_with_error_check(key, val)
    unless self.set(key, val)
      raise "Set error: #{self.error}"
    end
  end

  def remove_with_error_check(key)
    unless self.remove(key)
      raise "Remove error: #{self.error}"
    end
  end
end

module NPW
  module DBKyotoCabinet
    # @param [String] path Path of kyotocabinet file hash or other types of kyotocabinet database "-", "+", ":", "*", and "%"
    def set_kyotocabinet_path(path)
      @__kyotocabinet_path__ = path
    end

    # @return [String] Path of kyotocabinet database path without its options
    def get_kyotocabinet_path
      @__kyotocabinet_path__
    end

    # @return [Hash] Options of kyotocabinet database
    def get_kyotocabinet_options
      @__kyotocabinet_options__ ||= {}
    end

    def set_kyotocabinet_options(opts = {})
      get_kyotocabinet_options.merge!(opts)
    end

    def kyotocabinet_db
      @__kyotocabinet_db__
    end

    def open_kyotocabinet(mode, &block)
      if @__kyotocabinet_db__
        if kyotocabinet_on_memory?
          if block_given?
            yield(@__kyotocabinet_db__)
            return true
          else
            return @__kyotocabinet_db__
          end
        end
        @__kyotocabinet_db__.close
      else
        @__kyotocabinet_db__ = KyotoCabinet::DB::new
      end
      path = get_kyotocabinet_path.dup
      if !(opts = get_kyotocabinet_options).empty?
        opts.each do |key, val|
          path << "##{key}=#{val}"
        end
      end
      @__kyotocabinet_db__.open_with_error_check(path, mode)
      if block_given?
        yield(@__kyotocabinet_db__)
        close_kyotocabinet
      else
        @__kyotocabinet_db__
      end
    end

    KC_OPEN_WRITE_MODE = KyotoCabinet::DB::OWRITER | KyotoCabinet::DB::OCREATE
    KC_OPEN_READ_MODE = KyotoCabinet::DB::OREADER

    def open_write(&block)
      open_kyotocabinet(KC_OPEN_WRITE_MODE, &block)
    end

    def open_read(&block)
      open_kyotocabinet(KC_OPEN_READ_MODE, &block)
    end

    def kyotocabinet_on_memory?
      /^[-+:*%]/ =~ @__kyotocabinet_path__
    end

    def close_kyotocabinet
      if !kyotocabinet_on_memory? && @__kyotocabinet_db__
        @__kyotocabinet_db__.close_with_error_check
        @__kyotocabinet_db__ = nil
      end
    end

    # Delete file of database or close database on memory
    def clear_kyotocabinet
      if @__kyotocabinet_db__
        @__kyotocabinet_db__.close_with_error_check
        @__kyotocabinet_db__ = nil
      end
      unless kyotocabinet_on_memory?
        FileUtils.rm_r(@__kyotocabinet_path__)
      end
    end

    module Utils
      def pack_number(num)
        [num].pack("i")
      end
      module_function :pack_number

      def unpack_number(val)
        val.unpack("i")[0]
      end
      module_function :unpack_number

      def pack_numbers(nums)
        nums.pack("i*")
      end
      module_function :pack_numbers

      def unpack_numbers(val)
        val.unpack("i*")
      end
      module_function :unpack_numbers
    end
  end
end
