module NPW
  class Relation
    def self.parse(str)
      parser.parse(str).value
    end

    def self.parser
      raise "We must define the class method 'parser' in child class of NPW::Relation"
    end

    class Term
      attr_reader :word, :coef

      def initialize(word, coef)
        @word = word
        @coef = coef
      end

      def ==(other)
        coef == other.coef && word.isomorphic?(other.word)
      end

      def mul(num)
        @coef = @coef * num
        self
      end

      def add(coef)
        @coef += coef
        self
      end

      def delete
        @coef = 0
        self
      end

      def set_identity
        @coef = 1
        self
      end

      # Replace word.
      def replace(word)
        @word = word
      end
    end

    attr_reader :terms

    def term_create(arg)
      raise "We must define the class method 'term_create' in child class of NPW::Relation"
    end

    # @option opts [boolean] :simplify Simplify terms after initialization if the value is true.
    def initialize(str, opts = {})
      @terms = []
      self.class.parse(str).each do |ary|
        word = term_create(ary[0])
        unless word.empty?
          @terms << NPW::Relation::Term.new(word, ary[1])
        end
      end
      simplify! if opts[:simplify]
    end

    def empty?
      @terms.empty?
    end

    def to_s
      str = ""
      @terms.each do |trm|
        if trm.coef > 0
          str << " +" if str.size > 0
          str << trm.coef.to_s if trm.coef > 1
          str << trm.word.to_s
        elsif trm.coef < 0
          str << " " if str.size > 0
          str << "-"
          str << trm.coef.abs.to_s if trm.coef < -1
          str << trm.word.to_s
        end
      end
      str
    end

    # *Note* We must simplify relations before we call this method.
    def ==(other)
      return false if terms.size != other.terms.size
      terms.size.times.all? do |i|
        terms[i] == other.terms[i]
      end
    end

    def eql?(other)
      self == other
    end

    def hash
      (@terms.map { |t| [t.word.adjust_alphabet.to_s, t.coef] }).hash
    end

    # We use NPW::Relation#key_on_relation_set to classify relations by coefficients and first word strings.
    def key_on_relation_set
      (@terms.map { |t| t.coef }).unshift(@terms[0].word.to_s).pack("a*i!*")
    end

    def simplify(opts = {})
      Marshal.load(Marshal.dump(self)).simplify!(opts)
    end

    def simplified?
      @simplified
    end

    def sort_terms!
      @terms.sort_by! { |trm| trm.word }.reverse!
    end

    def adjust_alphabet!
      @terms.each { |trm| trm.word.adjust_alphabet! }
    end

    # @option opts [boolean] :adjust If the value is true, use letters from "A" as an alphabet of each term
    def simplify!(opts = {})
      unless simplified?
        i = 0
        while i < @terms.size
          j = i + 1
          while j < @terms.size
            if @terms[i].word.isomorphic?(@terms[j].word)
              @terms[i].add(@terms[j].coef)
              @terms.delete_at(j)
            else
              j += 1
            end
          end
          i += 1
        end
        @terms.delete_if { |trm| trm.coef == 0 }
        adjust_alphabet! if opts[:adjust]
        sort_terms!
        @terms.each { |trm| trm.mul(-1) } if !@terms.empty? && @terms[0].coef < 0
        @simplified = true
      end
      self
    end

    # @option opts [boolean,Hash] :simplify Simplify the terms after evaluating block. If the value is a hash, the method pass it as an argument of NPW::Relation#simplify!
    # @option opts [Integer,Range] :range Modify only terms specified by this option
    def modify!(opts = {}, &block)
      @simplified = false
      case opts[:range]
      when Fixnum
        yield(@terms[opts[:range]])
      when Range
        @terms[opts[:range]].each(&block)
      when Array
        opts[:range].each do |i|
          yield(@terms[i])
        end
      else
        @terms.each(&block)
      end
      @terms.delete_if { |trm| trm.coef == 0 || trm.word.empty? }
      simplify!(Hash === opts[:simplify] ? opts[:simplify] : {}) if opts[:simplify]
      self
    end

    # Eliminate the term by the relation.
    # The relations related to this method must be simplified.
    # @param [Integer] num Number of target term
    # @param [NPW::Relation] rel Relation to eliminate specified term
    # @return [NPW::Relation,nil] If the elimination succeeds then we get the resultant relation. Otherwise, nil.
    def eliminate!(num, rel)
      trm = terms[num]
      return nil unless trm2 = rel.terms.find { |t| trm.word.isomorphic?(t.word) }
      if trm.coef % trm2.coef == 0
        factor = - trm.coef / trm2.coef
        @terms = terms + rel.terms.map { |t| t.dup.mul(factor) }
      else
        gcd = trm.coef.gcd(trm2.coef)
        factor1 = trm2.coef / gcd
        factor2 = - trm.coef / gcd
        @terms = @terms.map { |t| t.mul(factor1) } + rel.terms.map { |t| t.dup.mul(factor2) }
      end
      @simplified = nil
      simplify!
      self
    end
  end

  class RelationSet
    include Enumerable

    MAX_TERMS = 100

    def initialize
      # Indices of @relations are number of terms
      @relations = Array.new(MAX_TERMS) { Hash.new { |h, k| h[k] = [] } }
    end

    # For debug
    def valid_data?
      @relations.each_with_index do |h, i|
        unless Hash === h
          raise "Invalid #{i}th data: #{h.inspect}"
        end
        h.each do |k, val|
          unless val.all? { |rel| NPW::Relation === rel }
            raise "Invalid relations of #{i}th data: #{val.inspect}"
          end
        end
      end
      true
    end

    def add_relation(rel)
      @relations[rel.terms.size][rel.key_on_relation_set] << rel
    end
    private :add_relation

    def add(*args)
      args.each do |arg|
        if Array === arg
          arg.each { |rel| add_relation(rel) }
        else
          add_relation(arg)
        end
      end
      self
    end

    def concat(other)
      other.each do |rel|
        self.add(rel)
      end
      self
    end

    def <<(arg)
      add(arg)
    end

    def size
      count = 0
      @relations.each do |key_rels|
        key_rels.each_value { |ary| count += ary.size }
      end
      count
    end

    def empty?
      @relations.all? { |key_rels| key_rels.each_value.all? { |ary| ary.empty? } }
    end

    def each(&block)
      return to_enum(:each) unless block_given?
      @relations.each do |key_rels|
        key_rels.each do |k, rels|
          rels.each do |rel|
            yield(rel)
          end
        end
      end
    end

    def uniq!
      @relations.each do |key_rels|
        key_rels.each do |k, rels|
          rels.uniq!
        end
      end
      self
    end

    def take_out_zero_generators
      zero_generators = self.class.new
      @relations[1].each do |k, rels|
        i = 0
        while i < rels.size
          rel = rels[i]
          if rel.terms.size == 1 && rel.terms[0].coef.abs == 1
            zero_generators.add(rel)
            rels.delete_at(i)
          else
            i += 1
          end
        end
      end
      zero_generators
    end

    def take_out_relation_of_pair
      @relations[2].each do |k, rels|
        rels.each_with_index do |rel, i|
          if rel.terms.all? { |trm| trm.coef.abs == 1 }
            return rels.delete_at(i)
          end
        end
      end
      nil
    end

    def relations_of_one_term_with_coefficient_two
      ary = []
      @relations[1].each do |k, rels|
        rels.each do |rel|
          ary << rel if rel.terms[0].coef.abs == 2
        end
      end
      ary
    end

    def relations_two_term_with_coefficients_one_two
      ary = []
      @relations[2].each do |k, rels|
        rels.each do |rel|
          ary << rel if rel.terms[0].coef.abs == 1 && rel.terms[1].coef.abs == 2
        end
      end
      ary
    end

    # @option opts [Integer,Range] :term Modify relations of which number of terms is exactly specified number.
    def modify!(opts = {}, &block)
      modified_relations = []
      case opts[:term]
      when Integer
        target_relations = [@relations[opts[:term]]]
      when Range
        target_relations = @relations[opts[:term]]
      else
        target_relations = @relations
      end
      target_relations.each do |key_rels|
        key_rels.each do |k, rels|
          i = 0
          while i < rels.size
            rel = rels[i]
            unless rel_new = yield(rel)
              i += 1
              next
            end
            unless NPW::Relation === rel_new
              raise "Block for NPW::RelationSet#modify! must return NPW::Relation or nil."
            end
            if rel_new.terms.size == 0
              rels.delete_at(i)
            elsif rel_new.key_on_relation_set != k
              rels.delete_at(i)
              modified_relations << rel_new
            else
              i += 1
            end
          end
        end
      end
      add(modified_relations)
      self.uniq!
    end

    class LazyLoading
      def initialize(path, proc_parse_relation_string)
        @path = path
        @parse_relation_string = proc_parse_relation_string
      end

      def relation_set
        unless @relation_set
          @relation_set = NPW::RelationSet.new
          Kernel.open(@path, 'r') do |f|
            while l = f.gets
              ary = l.strip.split("\t")
              @relation_set.add(@parse_relation_string.call(ary[-1]))
            end
          end
        end
        @relation_set
      end

      def method_missing(*args, &block)
        relation_set.__send__(*args, &block)
      end
    end
  end
end
