require "binary_search/pure"
require "tempfile"

class Array
  def binary_index_for_reversing(target)
    binary_chop { |v| v <=> target }
  end
end

module NPW
  class Relation
    class GeneratorFile
      # @note We must initialize database to call the method open_write before we call this method.
      def initialize(path, load, each_generator_method, rank_range, parse_word_string)
        @parse_word_string = parse_word_string
        @path = path
        FileUtils.rm(@path) if !load && File.exist?(@path)
        if File.exist?(@path)
          load(@path)
        else
          open_write do |*args|
            ranks = rank_range.each.to_a.sort.reverse
            ranks.each do |rank|
              each_generator_method.call(rank) do |generator|
                register(generator, *args)
              end
            end
          end
        end
      end

      def load(path)
        raise "We must define #{self.class.to_s}#load"
      end

      def register(generator, *args)
        raise "We must define #{self.class.to_s}#register"
      end

      def each(&block)
        raise "We must define #{self.class.to_s}#each"
      end

      def find_generator_index(db, word)
        raise "We must define #{self.class.to_s}#find_generator_index"
      end

      def number_of_generators(db)
        raise "We must define #{self.class.to_s}#number_of_generators"
      end

      def number_to_word(db, num)
        raise "We must define #{self.class.to_s}#number_to_word"
      end

      def open_write(&block)
        yield(nil)
      end

      def open_read(&block)
        yield(nil)
      end

      # Output a matrix whose rows corresponds to relations and columns corresponds to generators
      # to the specified file.
      # @param [String] path File path of an output matrix
      # @param [Object] all_rels An array of relations that must have the method 'each'
      # @param [Integer] modulo Modulo integer
      def dump_matrix_db(path, all_rels, modulo = nil)
        mat = nil
        open_read do |db|
          mat = NPW::MatrixDB.new(path, :type => :row, :row => all_rels.size, :column => number_of_generators(db), :modulo => modulo)
          mat.open("w") do |m|
            all_rels.each.with_index do |rel, row_num|
              nums = []
              rel.terms.each do |trm|
                raise "Can not find the generator: #{rel}" unless num = find_generator_index(db, trm.word)
                nums << [num, trm.coef]
              end
              nums.sort_by! { |ary| ary[0] }
              m.row_set(row_num, nums)
            end
          end
        end
        mat
      end

      # @option [String] out Output file path
      # @option [Enumerator] enum_number_value Enumerator returning arrays of number and word
      def dump_table_of_generators_values(out, dimensions, enum_number_value)
        open_read do |db|
          open(out, "w") do |f|
            f.puts "# #{dimensions.map(&:to_s).join(" ")}"
            enum_number_value.each do |num, value|
              word = number_to_word(db, num)
              f.puts "#{word}\t#{value.map(&:to_s).join(" ")}"
            end
          end
        end
      end

      def dump_transform_matrix_to_database(m, mat_data, additional_rels, additional_rels_data)
        m.row_set_bulk(mat_data)
        additional_rels.open_write
        additional_rels.set_bulk(additional_rels_data)
        additional_rels.close_kyotocabinet
        mat_data.clear
        additional_rels_data.clear
      end
      private :dump_transform_matrix_to_database

      MAX_SIZE_TRANSFORM_DATA = 10000

      # @param [String] output_path File path of an output matrix database
      # @param [String] additional_rels_path File path of additional relations database
      # @param [NPW::TruncatedPolyakAlgebra::WordClassify::DB] classification Classification database of usual words
      # @param [Integer] modulo Modulo integer
      # @return [Hash] Additional relations
      def dump_transform_matrix_by_subwords(output_path, additional_rels_path, classification, modulo, rank)
        word_len_max = rank * 2
        additional_rels = NPW::MatrixDB::AdditionalRels.new(additional_rels_path)
        num_gen = nil
        open_read do |db|
          num_gen = number_of_generators(db)
        end
        m = NPW::MatrixDB.new(output_path, :type => :row, :row => num_gen, :column => num_gen)
        NPW::Settings.max_process_number.times do |process_num|
          fork do
            classification.open("read") do |db_classify|
              row_num = 0
              mat_data = {}
              additional_rels_data = {}
              each do |word_gen|
                if (row_num % NPW::Settings.max_process_number) == process_num
                  open_read do |db|
                    if num_gen = find_generator_index(db, word_gen)
                      word_reduced = db_classify.reduced_word(word_gen)
                      need_to_transform = word_reduced && (word_len_max < word_gen.size || word_reduced == NPW::MARK_EMPTY)
                      if need_to_transform
                        if word_reduced == NPW::MARK_EMPTY
                          additional_rels_data[num_gen] = -1
                        elsif word_reduced != word_gen
                          if num_reduced = find_generator_index(db, word_reduced)
                            additional_rels_data[num_gen] = num_reduced
                          else
                            additional_rels_data[num_gen] = -1
                          end
                        end
                        trm = Hash.new { |h, k| h[k] = 0 }
                        word_gen.subwords.each do |w|
                          w.adjust_alphabet!
                          trm[w] += 1
                        end
                        nums = []
                        trm.each do |word, coef|
                          if num = find_generator_index(db, word)
                            nums << [num, coef]
                          end
                        end
                        nums.sort_by! { |ary| ary[0] }
                        mat_data[row_num] = nums
                        if word_reduced = db_classify.reduced_word(word_gen)
                          if word_reduced == NPW::MARK_EMPTY
                            additional_rels_data[num_gen] = -1
                          elsif word_reduced != word_gen
                            if num_reduced = find_generator_index(db, word_reduced)
                              additional_rels_data[num_gen] = num_reduced
                            else
                              additional_rels_data[num_gen] = -1
                            end
                          end
                        end
                      else
                        mat_data[row_num] = [[num_gen, 1]]
                      end
                    end
                  end
                  if mat_data.size > MAX_SIZE_TRANSFORM_DATA
                    dump_transform_matrix_to_database(m, mat_data, additional_rels, additional_rels_data)
                  end
                end
                row_num += 1
              end
              dump_transform_matrix_to_database(m, mat_data, additional_rels, additional_rels_data)
            end
          end
        end
        Process.waitall
        [m, additional_rels]
      end

      def dump_transform_matrix_by_subwords2(output_path, additional_rels_path, classification, modulo, rank)
        words_not_transform = classification.words_of_one_word_class(rank)
        additional_rels = NPW::MatrixDB::AdditionalRels.new(additional_rels_path)
        num_gen = nil
        open_read do |db|
          num_gen = number_of_generators(db)
        end
        m = NPW::MatrixDB.new(output_path, :type => :row, :row => num_gen, :column => num_gen)
        NPW::Settings.max_process_number.times do |process_num|
          fork do
            classification.open("read") do |db_classify|
              row_num = 0
              mat_data = {}
              additional_rels_data = {}
              each do |word_gen|
                if (row_num % NPW::Settings.max_process_number) == process_num
                  open_read do |db|
                    if num_gen = find_generator_index(db, word_gen)
                      if words_not_transform.binary_index(word_gen.to_s)
                        mat_data[row_num] = [[num_gen, 1]]
                      else
                        trm = Hash.new { |h, k| h[k] = 0 }
                        word_gen.subwords.each do |w|
                          w.adjust_alphabet!
                          trm[w] += 1
                        end
                        nums = []
                        trm.each do |word, coef|
                          if num = find_generator_index(db, word)
                            nums << [num, coef]
                          end
                        end
                        nums.sort_by! { |ary| ary[0] }
                        mat_data[row_num] = nums
                        if word_reduced = db_classify.reduced_word(word_gen)
                          if word_reduced == NPW::MARK_EMPTY
                            additional_rels_data[num_gen] = -1
                          elsif word_reduced != word_gen
                            if num_reduced = find_generator_index(db, word_reduced)
                              additional_rels_data[num_gen] = num_reduced
                            else
                              additional_rels_data[num_gen] = -1
                            end
                          end
                        end
                        # else
                        #   puts "#{word_gen} is NOT a word in generators."
                      end
                    end
                  end
                  if mat_data.size > MAX_SIZE_TRANSFORM_DATA
                    dump_transform_matrix_to_database(m, mat_data, additional_rels, additional_rels_data)
                  end
                end
                row_num += 1
              end
              dump_transform_matrix_to_database(m, mat_data, additional_rels, additional_rels_data)
            end
          end
        end
        Process.waitall
        [m, additional_rels]
      end
    end

    class GeneratorInText < NPW::Relation::GeneratorFile
      # Note that generators must be sorted for binary search
      def initialize(path, load, each_generator_method, rank_range, parse)
        @generators = []
        super(path, load, each_generator_method, rank_range, parse)
        @generators.sort!.reverse!
      end

      def load(path)
        @generators = File.read(path).split.map(&@parse_word_string)
      end

      def register(generator, f)
        @generators << generator
        f.puts generator
      end

      def each(&block)
        @generators.each(&block)
      end

      # Support generators is sorted
      def find_generator_index(db, word)
        if String === word
          word = @parse_word_string.call(word)
        end
        @generators.binary_index_for_reversing(word)
      end

      alias_method :find?, :find_generator_index

      def number_of_generators(db)
        @generators.size
      end

      def open_write(&block)
        open(@path, "w") { |f| yield(f) }
      end

      def number_to_word(db, num)
        @generators[num]
      end
    end

    class GeneratorInDB < NPW::Relation::GeneratorFile
      include NPW::DBKyotoCabinet

      NUMBER_KEY_FIRST = "#"

      def initialize(path, load, each_generator_method, rank_range, parse)
        set_kyotocabinet_path(path)
        super(path, load, each_generator_method, rank_range, parse)
      end

      def load(path)
      end

      def number_key(num)
        [NUMBER_KEY_FIRST, num].pack("ai")
      end

      def pack_number(num)
        [num].pack("i")
      end

      def unpack_number(str)
        str.unpack("i")[0]
      end

      def register(generator, db)
        word = generator.to_s
        num = number_of_generators(db)
        unless (db.set(word, pack_number(num)) && db.set(number_key(num), word))
          raise "Set error: #{db.error}"
        end
      end

      # @note To avoid KyotoCabinet::DB#get in KyotoCabinet::DB#each,
      #   we output all words to a temporary file.
      def each(&block)
        open_already = !!kyotocabinet_db
        open_read unless open_already
        tmpfile = Tempfile.new("generator_each")
        res_each = kyotocabinet_db.each_key do |key_ary|
          generator = key_ary.first
          if generator[0] != NUMBER_KEY_FIRST
            tmpfile.puts generator
          end
        end
        unless res_each
          raise "Failure in #{kyotocabinet_db.class}#each_key: #{kyotocabinet_db.error}"
        end
        close_kyotocabinet unless open_already
        tmpfile.open
        tmpfile.each_line do |l|
          yield(@parse_word_string.call(l.strip))
        end
        true
      ensure
        tmpfile.close
      end

      def find?(db, word)
        db.get(word.to_s)
      end

      # Support generators is sorted
      def find_generator_index(db, word)
        if packed_num = db.get(word.to_s)
          unpack_number(packed_num)
        else
          nil
        end
      end

      def number_of_generators(db)
        db.count / 2
      end

      def number_to_word(db, num)
        unless word = db.get(number_key(num))
          raise "Invalid number: #{num}"
        end
        word
      end
    end
  end
end
