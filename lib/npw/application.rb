gem 'filename'
autoload :FileName, 'filename'

module NPW
  autoload :TruncatedPolyakAlgebra, "npw/application/polyak_algebra/truncated_polyak_algebra"
  autoload :GnOnGaussWord, "npw/application/gn_gw"
end
