require "npw/application/polyak_algebra/matrix_db"
require "matrix"

module NPW
  class SNF
    def self.modulo_vector(vector, dimensions)
      vector.map.with_index do |v, j|
        if dimensions[j] > 0
          while v < 0
            v += dimensions[j]
          end
          v = v % dimensions[j]
        end
        v
      end
    end

    class GAP
      # @option opts [String] :memory Set -o option of GAP. Default is "2g".
      # @option opts [String] :path Path of gap command
      def initialize(opts)
        @memory = opts[:memory] || "2g"
        @path = opts[:path] || "gap"
      end

      def parse_snf_matrix_string(str)
        snf_mat = eval(str)
        count = Hash.new { |h, k| h[k] = 0 }
        i = 0
        while snf_mat[i] && snf_mat[i][i]
          count[snf_mat[i][i]] += 1
          i += 1
        end
        count
      end
      private :parse_snf_matrix_string

      def parse_result(path)
        count = parse_snf_matrix_string(File.read(path).gsub(/\s+/, ""))
        count[0] += 1                   # Part generated by empty word
        grp = ""
        count.to_a.sort_by { |ary| ary[0] }.each do |num, c|
          next if num == 1
          if num == 0
            grp << " + " if grp.size > 0
            grp << "Z"
            grp << "^#{c}" if c > 1
          else
            grp << " + " if grp.size > 0
            g = "Z/#{num}Z"
            grp << (c > 1 ? "(#{g})^#{c}" : g)
          end
        end
        grp
      end
      private :parse_result

      def gap_command(input, output)
        system("gap -q -o #{@memory} #{input} > #{output}")
      end
      private :gap_command

      def create_gap_file(mat, file_path)
        open(file_path, "w") do |f|
          f << "m:="
          mat.to_gap_string(f)
          f << ";\n"
          f.puts <<GAP
Print(SmithNormalFormIntegerMat(m));
QUIT;
GAP
        end
      end
      private :create_gap_file

      def calculate_snf(mat, file_path, out)
        create_gap_file(mat, file_path)
        gap_command(file_path, out)
        out
      end

      def exec(mat, file_path, out)
        if out = calculate_snf(mat, file_path, out)
          parse_result(out)
        else
          "Z"
        end
      end

      def calculate_transformation(gap_snf_path, gap_source_path, out)
        unless File.exist?(gap_snf_path)
          raise "GAP file does not exist: #{gap_snf_path}"
        end
        system("sed -e 's/SmithNormalFormIntegerMat/SmithNormalFormIntegerMatTransforms/g' #{gap_snf_path} > #{gap_source_path}")
        gap_command(gap_source_path, out)
        out
      end

      def each_nonzero_word_value(matrix_transform, dimensions, zero_vec, &block)
        return to_enum(:each_nonzero_word_value, matrix_transform, dimensions, zero_vec) unless block_given?
        matrix_transform.row_size.times do |i|
          vals = matrix_transform.row(i).to_a[-(dimensions.size)..-1]
          vals = NPW::SNF.modulo_vector(vals, dimensions)
          yield([i, vals]) if vals != zero_vec
        end
      end

      def get_transformation(generators, gap_snf_path, gap_source_path, out, table_file)
        path_result = calculate_transformation(gap_snf_path, gap_source_path, out)
        syms = [:normal, :rowC, :rowQ, :colC, :colQ, :rowtrans, :coltrans]
        matrix_transform = nil
        c = 0
        count = nil
        File.read(path_result).gsub(/\s+/, " ").split(":=").each do |str|
          if /(\[.*\])/ =~ str
            sym = syms[c]
            if sym == :normal
              count = parse_snf_matrix_string(Regexp.last_match[1])
            elsif sym == :coltrans
              matrix_transform = Matrix[*eval(Regexp.last_match[1])]
            end
            c += 1
          end
        end

        unless (count && matrix_transform)
          raise "Invalid GAP result"
        end

        dimensions = []
        count.to_a.sort_by { |ary| ary[0] }.each do |num, c|
          if num > 1
            c.times { |i| dimensions << num }
          end
        end
        count[0].times { |i| dimensions << 0 } if count[0] > 0
        zero_vec = [0] * dimensions.size

        e =  each_nonzero_word_value(matrix_transform, dimensions, zero_vec)
        generators.dump_table_of_generators_values(table_file, dimensions, e)
      end
    end

    class SparseSNF
      INFO_OUTPUT = 5000

      def initialize(opts = {})
        File.expand_path(File.dirname(__FILE__))
        @command = File.join(File.expand_path(File.dirname(__FILE__)).sub(/^(.*)\/lib\/npw\/(?!\/lib\/npw\/).*/, "\\1"), "/dev/math-sparse-snf/src/sparse_snf")
        unless File.exist?(@command)
          raise "#{@command} does not exist"
        end
      end

      def parse_result_file(path)
        ary = `tail -n 1 #{path}`.split(",").map { |s| s.strip.split(":").map { |t| t.strip.to_i } }
        ary.sort_by { |num, dim| num }
      end
      private :parse_result_file

      def exec(path, out)
        system("#{@command} -i #{INFO_OUTPUT} #{path} > #{out}")
        ary = parse_result_file(out)
        grp = ""
        free_part = 1
        ary.each do |p, n|
          if p > 1
            if n == 1
              grp << " + Z/#{p}Z"
            else
              grp << " + (Z/#{p}Z)^#{n}"
            end
          elsif p == 0
            free_part += n
          end
        end
        if free_part == 1
          "Z" + grp
        else
          "Z^#{free_part}" + grp
        end
      end

      def each_nonzero_word_value(path, dimensions, zero_vec)
        return to_enum(:each_nonzero_word_value, path, dimensions, zero_vec) unless block_given?
        data = []
        `tail -n #{dimensions.size} #{path}`.each_line do |line|
          data << line.split.map { |s| s.to_i }
        end
        mat = Matrix[*data]
        mat.column_size.times do |i|
          vals = NPW::SNF.modulo_vector(mat.column(i).to_a, dimensions)
          yield([i, vals]) if vals != zero_vec
        end
      end
      private :each_nonzero_word_value

      def get_transformation(generators, matrix_data_path, out, transform_out, table_file, path_rels_transformation)
        system("#{@command} -i #{INFO_OUTPUT} #{matrix_data_path} --column #{transform_out} > #{out}")
        ary = parse_result_file(out)
        free_part_dim = nil
        dimensions = []
        ary.each do |num, dim|
          if num == 0
            free_part_dim = dim
          elsif num > 1
            dimensions.concat([num] * dim)
          end
        end
        dimensions.concat([0] * free_part_dim) if free_part_dim
        zero_vec = [0] * dimensions.size
        if path_rels_transformation
          transform_out_original = transform_out.sub(/\.([^.]+)$/, "_original.\\1")
          FileUtils.mv(transform_out, transform_out_original)
          mat_rels_trans = NPW::MatrixDB.new(path_rels_transformation, :load => true)
          if mat_rels_trans.type == :column
            mat_rels_trans = mat_rels_trans.change_type(mat_rels_trans.get_kyotocabinet_path.sub(/\.kch/, "_row.kch"))
          end
          mat_transform_out_original = transform_out.sub(/\.[^.]+$/, ".kch")
          mat_rels_trans.composite_transformation(transform_out_original, mat_transform_out_original, transform_out)
        end
        e = each_nonzero_word_value(transform_out, dimensions, zero_vec)
        generators.dump_table_of_generators_values(table_file, dimensions, e)
      end

      def exec_only_reduction(path, out)
        system("#{@command} -i #{INFO_OUTPUT} --reduce-by-row-operations #{out} #{path} > #{out}.log")
      end
    end

    # @param [String] output_dir Output directory
    # @option opts [Hash] :gap An argument of NPW::SNF::GAP
    def initialize(output_dir, opts = {})
      @dir = output_dir
      if opts[:gap]
        @gap = NPW::SNF::GAP.new(opts[:gap])
      else
        @snf_command = NPW::SNF::SparseSNF.new
      end
    end

    def type
      if @gap
        "GAP"
      else
        "sparse_snf"
      end
    end

    def filepath(name)
      File.join(@dir, name)
    end
    private :filepath

    # Transform relation matrix by using classification of words
    # @param [NPW::Relation::GeneratorFile] generators Generators
    # @param [String] db_classify Path of classification database
    # @param [NPW::MatrixDB] mat_rels Matrix database of relations
    # @param [Integer] modulo Modulo number
    def transform_relations(generators, db_classify, mat_rels, modulo, rank)
      classification = NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(db_classify)
      path_trans = filepath("transform_matrix_generators.kch")
      path_additional_rels = filepath("additional_rels.kch")
      path_trans_inverse = filepath("transform_matrix_inverse.kch")
      matrix_path = filepath("relation_matrix_transformed.kch")
      if File.exist?(path_trans) && File.exist?(path_additional_rels)
        puts "Load #{path_trans}"
        mat_trans = NPW::MatrixDB.new(path_trans, :load => true)
        puts "Load #{path_additional_rels}"
        additional_rels = NPW::MatrixDB::AdditionalRels.new(path_additional_rels)
      else
        puts "Create a transform matrix and additional relations"
        mat_trans, additional_rels = generators.dump_transform_matrix_by_subwords(path_trans, path_additional_rels, classification, modulo, rank)
      end
      if File.exist?(path_trans_inverse)
        puts "Load #{path_trans_inverse}"
        mat_inv = NPW::MatrixDB.new(path_trans_inverse, :load => true)
      else
        puts "Create an inverse of the transform matrix"
        mat_inv = mat_trans.inverse_of_upper_triangular(path_trans_inverse, :test => true)
      end
      if File.exist?(matrix_path)
        puts "Load #{matrix_path}"
        NPW::MatrixDB.new(matrix_path, :load => true)
      else
        puts "Create transformation of the relation matrix"
        mat_rels.transform_by(mat_inv, additional_rels, matrix_path, modulo)
      end
    end
    private :transform_relations

    # @note The file already existing is not recreated.
    def calc_gn(all_rels, generators, modulo, db_classify, rank)
      matrix_path = filepath("relation_matrix.kch")
      if File.exist?(matrix_path)
        puts "Load #{matrix_path}"
        mat = NPW::MatrixDB.new(matrix_path, :load => true)
      else
        puts "Create relation matrix"
        mat = generators.dump_matrix_db(matrix_path, all_rels, modulo)
      end
      if @gap
        if db_classify
          mat = transform_relations(generators, db_classify, mat, modulo, rank)
        end
        grp = @gap.exec(mat, filepath("snf.gap"), filepath("snf_result.txt"))
      else
        sparse_snf_result = filepath("sparse_snf_result.txt")
        if db_classify
          path_orig = filepath("sparse_snf_data_original.txt")
          unless File.exist?(path_orig)
            puts "Dump the original relation matrix to text file"
            mat.dump_for_sparse_snf(path_orig)
          end
          path_orig_reduced = filepath("sparse_snf_data_original_reduced.txt")
          unless File.exist?(path_orig_reduced)
            puts "Reduce the relation matrix"
            @snf_command.exec_only_reduction(path_orig, path_orig_reduced)
          end
          path_orig_reduced_kch = filepath("sparse_snf_data_original_reduced.kch")
          if File.exist?(path_orig_reduced_kch)
            mat = NPW::MatrixDB.new(path_orig_reduced_kch, :load => true)
          else
            mat = NPW::MatrixDB.load_text_for_sparse_snf(path_orig_reduced_kch, path_orig_reduced)
          end
          mat = transform_relations(generators, db_classify, mat, modulo, rank)
        end
        path_sparse_snf_data = filepath("sparse_snf_data.txt")
        unless File.exist?(path_sparse_snf_data)
          puts "Dump the relation matrix to text file"
          mat.dump_for_sparse_snf(path_sparse_snf_data, :unique => true)
        end
        path_sparse_snf_data_reduced = filepath("sparse_snf_data_reduced.txt")
        unless File.exist?(path_sparse_snf_data_reduced)
          puts "Reduce the relation matrix before main calculation"
          @snf_command.exec_only_reduction(path_sparse_snf_data, path_sparse_snf_data_reduced)
        end
        grp = @snf_command.exec(path_sparse_snf_data_reduced, sparse_snf_result)
      end
      grp
    end

    def calc_transformation(generators)
      use_classification = File.exist?(filepath("relation_matrix_transformed.kch"))
      if @gap
        raise "Not implemented" if use_classification
        transform_gap = filepath("snf_transform.gap")
        transform_result = filepath("snf_result_transform_gap.txt")
        generator_table = filepath("table_gap.tsv")
        @gap.get_transformation(generators, filepath("snf.gap"), transform_gap, transform_result, generator_table)
      else
        res2 = filepath("snf_result2.txt")
        transform_result = filepath("snf_transform.txt")
        generator_table = filepath("table.tsv")
        path_rels_transformation = use_classification && filepath("transform_matrix_inverse.kch")
        @snf_command.get_transformation(generators, filepath("sparse_snf_data.txt"), res2, transform_result, generator_table, path_rels_transformation)
      end
    end
  end
end
