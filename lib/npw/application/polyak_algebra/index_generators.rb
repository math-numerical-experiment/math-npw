module NPW
  class TruncatedPolyakAlgebra
    class Index
      def self.compare_words(w1, w2)
        cmp = w1.size <=> w2.size
        if cmp == 0
          cmp = (w1 <=> w2)
        end
        cmp
      end

      class DBTemplate
        include NPW::DBKyotoCabinet
        include NPW::DBKyotoCabinet::Utils

        def initialize(path)
          set_kyotocabinet_path(path)
        end

        def count_number(count_key)
          if val = kyotocabinet_db.get(count_key)
            n = unpack_number(val)
          else
            n = 0
          end
          n += 1
          kyotocabinet_db.set_with_error_check(count_key, pack_number(n))
          n
        end
      end

      class ZeroDB < NPW::TruncatedPolyakAlgebra::Index::DBTemplate
        def set(word, rank_num)
          kyotocabinet_db.set_with_error_check(word.to_s, pack_number(rank_num))
        end

        def find?(word)
          !!kyotocabinet_db.get(word.to_s)
        end

        def rank_of_relation(word)
          if val = kyotocabinet_db.get(word.to_s)
            unpack_number(val)
          else
            nil
          end
        end

        # @param [String] output_path Output path
        # @param [Hash] opts Options
        # @option opts [Integer] :rank Specify rank of words
        def dump(output_path, opts = {})
          len = (opts[:rank] ? opts[:rank] * 2 : nil)
          open_read do |db|
            open(output_path, "w") do |f|
              db.each do |key, val|
                if !len || len == key.size
                  f.puts "#{key}\t#{unpack_number(val)}"
                end
              end
            end
          end
        end
      end

      class EqualityDB
        class WordToNumberDB < NPW::TruncatedPolyakAlgebra::Index::DBTemplate
          KEY_NUMBER_FIRST = "#"
          KEY_MAX_NUMBER = ([-1].pack("i"))

          def number_key(n)
            KEY_NUMBER_FIRST + [n].pack("i")
          end

          def number_to_word(n)
            kyotocabinet_db.get(number_key(n))
          end

          # @return [Array] Pair of word number and class number
          def get(word_str)
            if val = kyotocabinet_db.get(word_str)
              unpack_numbers(val)
            else
              nil
            end
          end

          def set(word_str, num_class)
            if nums = get(word_str)
              nums[1] = num_class
            else
              n = count_number(KEY_MAX_NUMBER)
              nums = [n, num_class]
              kyotocabinet_db.set_with_error_check(number_key(n), word_str)
            end
            kyotocabinet_db.set_with_error_check(word_str, pack_numbers(nums))
            nums
          end

          def remove(word_str)
            if nums = get(word_str)
              kyotocabinet_db.remove_with_error_check(word_str)
              kyotocabinet_db.remove_with_error_check(number_key(nums[0]))
            end
          end

          def remove_by_number(num)
            key = number_key(num)
            if word_str = kyotocabinet_db.get(key)
              kyotocabinet_db.remove_with_error_check(key)
              kyotocabinet_db.remove_with_error_check(word_str)
              return word_str
            end
            nil
          end
        end

        class ClassToRepresentativeDB < NPW::TruncatedPolyakAlgebra::Index::DBTemplate
          KEY_MAX_NUMBER = ([-1].pack("i"))

          def get(num_class)
            kyotocabinet_db.get(pack_number(num_class))
          end

          def set(num_class, word_str)
            kyotocabinet_db.set_with_error_check(pack_number(num_class), word_str)
          end

          # Register new word class
          def register(word_str)
            n = count_number(KEY_MAX_NUMBER)
            set(n, word_str)
            n
          end

          def renew(num_class, word_str)
            if w = get(num_class)
              if NPW::TruncatedPolyakAlgebra::Index.compare_words(w, word_str) > 0
                set(num_class, word_str)
              end
            end
          end

          def remove(num_class)
            kyotocabinet_db.remove_with_error_check(pack_number(num_class))
          end
        end

        class ClassToWordsDB < NPW::TruncatedPolyakAlgebra::Index::DBTemplate
          def get(num_class)
            if val = kyotocabinet_db.get(pack_number(num_class))
              unpack_numbers(val)
            else
              nil
            end
          end

          def set(num_class, word_nums)
            kyotocabinet_db.set_with_error_check(pack_number(num_class), pack_numbers(word_nums))
          end

          def add(num_class, word_num)
            nums = get(num_class) || []
            nums << word_num
            nums.sort!.uniq!
            set(num_class, nums)
          end

          def remove(num_class)
            kyotocabinet_db.remove_with_error_check(pack_number(num_class))
          end

          def get_remove(num_class)
            if nums = get(num_class)
              remove(num_class)
            end
            nums
          end

          def each(&block)
            kyotocabinet_db.each do |key, val|
              yield(unpack_number(key), unpack_numbers(val))
            end
          end
        end

        def initialize(path_word_to_class, path_class_to_rep, path_class_to_words)
          @word_to_number = WordToNumberDB.new(path_word_to_class)
          @class_to_rep = ClassToRepresentativeDB.new(path_class_to_rep)
          @class_to_words = ClassToWordsDB.new(path_class_to_words)
        end

        def open_write(&block)
          dbs = [@word_to_number.open_write, @class_to_rep.open_write, @class_to_words.open_write]
          if block_given?
            yield(self)
            dbs.each { |db| db.close_with_error_check }
          end
        end

        def open_read(&block)
          dbs = [@word_to_number.open_read, @class_to_rep.open_read, @class_to_words.open_read]
          if block_given?
            yield
            dbs.each { |db| db.close_with_error_check }
          end
        end

        # If word is a representative word, we get nil.
        # @note Representative word must be a word of smallest length in the class.
        def get_representative_word(word)
          word_str = word.to_s
          if nums = @word_to_number.get(word_str)
            s = @class_to_rep.get(nums[1])
            return s if s != word_str
          end
          nil
        end

        def word_strings_in_class(num_class)
          @class_to_words.get(num_class).map { |n| @word_to_number.number_to_word(n) }
        end
        private :word_strings_in_class

        # Return word number and class number.
        def get_word_numbers(word)
          @word_to_number.get(word.to_s)
        end

        def get_word_class(word)
          if nums = get_word_numbers(word)
            num_word, num_class = nums
            return word_strings_in_class(num_class)
          end
          nil
        end

        def remove_class(word)
          if nums = @word_to_number.get(word.to_s)
            num_word, num_class = nums
            word_strings = @class_to_words.get(num_class).map { |n| @word_to_number.remove_by_number(n) }
            @class_to_words.remove(num_class)
            @class_to_rep.remove(num_class)
            return word_strings
          end
          nil
        end

        def register_pair(word1, word2)
          nums1 = @word_to_number.get(word1.to_s)
          nums2 = @word_to_number.get(word2.to_s)
          if nums1 && nums2
            num_word1, num_class1 = nums1
            num_word2, num_class2 = nums2
            if num_class1 != num_class2
              word_str1 = word1.to_s
              word_str2 = word2.to_s
              if NPW::TruncatedPolyakAlgebra::Index.compare_words(word_str1, word_str2) < 0
                num_class_new = num_class1
                num_class_remove = num_class2
              else
                num_class_new = num_class2
                num_class_remove = num_class1
              end
              word_nums = @class_to_words.get_remove(num_class_remove)
              word_nums.each do |num|
                @word_to_number.set(@word_to_number.number_to_word(num), num_class_new)
              end
              word_nums = @class_to_words.get(num_class_new).concat(word_nums)
              @class_to_words.set(num_class_new, word_nums)
            end
          elsif !nums1 && !nums2
            word_strs = [word1.to_s, word2.to_s]
            if NPW::TruncatedPolyakAlgebra::Index.compare_words(*word_strs) > 0
              word_strs.reverse!
            end
            num_class = @class_to_rep.register(word_strs[0])
            num_word1, num_class1 = @word_to_number.set(word_strs[0], num_class)
            num_word2, num_class2 = @word_to_number.set(word_strs[1], num_class)
            @class_to_words.set(num_class, [num_word1, num_word2])
          else
            if nums1
              num_word1, num_class_exist = nums1
              w = word2.to_s
            elsif nums2
              num_word2, num_class_exist = nums2
              w = word1.to_s
            end
            num_word_new, num_class_tmp = @word_to_number.set(w, num_class_exist)
            @class_to_words.add(num_class_exist, num_word_new)
            @class_to_rep.renew(num_class_exist, w)
          end
        end

        # @param [Range] string_size_range Range of representative word, which is smallest word of a class.
        def each_class(string_size_range, &block)
          @class_to_words.each do |num_class, nums_word|
            rep = @class_to_rep.get(num_class)
            word_size = rep.sub(/:.*$/, "").size / 2
            if string_size_range.include?(word_size)
              words = nums_word.map { |n| @word_to_number.number_to_word(n) }
              yield(rep, words)
            end
          end
        end

        def dump(path, opts = {})
          open_read do |db|
            open(path, "w") do |f|
              @class_to_words.each do |num_class, nums_word|
                words = nums_word.map { |n| @word_to_number.number_to_word(n) }
                f.puts words.join(" ")
              end
            end
          end
        end
      end

      def initialize(index_name, dir, parse_relation_string, test_zero_word)
        @index_name = index_name
        @dir = dir
        @parse_relation_string = parse_relation_string
        @test_zero_word = test_zero_word
        if File.exist?(@dir)
          name = File.read(file_name).strip
          unless name == @index_name
            raise "Index name #{name} does not coincide with #{@index_name}."
          end
        else
          FileUtils.mkdir_p(@dir)
          open(file_name, "w") { |f| f.puts @index_name }
        end
        @db_zero = NPW::TruncatedPolyakAlgebra::Index::ZeroDB.new(File.join(@dir, "zero.kch"))
        @db_equality = NPW::TruncatedPolyakAlgebra::Index::EqualityDB.new(File.join(@dir, "eq_wc.kch"), File.join(@dir, "eq_cr.kch"), File.join(@dir, "eq_cw.kch"))
      end

      def file_name
        File.join(@dir, "NAME")
      end
      private :file_name

      def generator_without_zero(m, rank, &block)
        @db_zero.open_read do |db|
          m.call(rank) do |word|
            unless @db_zero.find?(word)
              yield(word)
            end
          end
        end
      end

      def file_relation(n)
        File.join(@dir, "relation_#{n}.txt")
      end

      def created?(n)
        File.exist?(file_relation(n))
      end

      # The database \@db_zero mush be open before calling this method.
      def simplify_relation_with_finding_zero(n, rel)
        num_word = n
        rel.simplify!(:adjust => true)
        rel.modify!(:simplify => true) do |trm|
          if (num = @db_zero.rank_of_relation(trm.word.to_s)) || @test_zero_word.call(trm.word)
            num_word = num if num && num_word < num
            trm.delete
          elsif word_rep = @db_equality.get_representative_word(trm.word)
            trm.replace(@parse_relation_string.call(word_rep).terms[0].word)
          end
        end
        ret = nil
        unless rel.terms.empty?
          if rel.terms.size == 1 && rel.terms[0].coef == 1
            w = rel.terms[0].word
            if word_removed = @db_equality.remove_class(w)
              word_removed.each do |w_removed|
                @db_zero.set(w_removed, num_word)
              end
            else
              @db_zero.set(w, num_word)
            end
          elsif rel.terms.size == 2 && rel.terms[0].coef == 1 && rel.terms[1].coef == -1
            @db_equality.register_pair(rel.terms[0].word, rel.terms[1].word)
          else
            ret = rel
          end
        end
        ret
      end
      private :simplify_relation_with_finding_zero

      def improve_relation(rank, file, file_sort)
        count_zero = 1
        while count_zero > 0
          count_zero = 0
          system("sort #{file} | uniq > #{file_sort}")
          open(file, "w") do |f|
            open(file_sort, "r") do |input|
              while l = input.gets
                rel = @parse_relation_string.call(l.strip)
                if rel = simplify_relation_with_finding_zero(rank, rel)
                  f.puts rel
                else
                  count_zero += 1
                end
              end
            end
          end
        end
      end
      private :improve_relation

      # Create index of generators and relations for Polyak algebra
      # @param [Integer] rank Rank number
      # @param [Proc] method_to_generate_relations Proc to generate relations of specified rank
      # @param [Hash] opts Options
      # @option opts [String] :message Message to be written in README
      def create(rank, method_to_generate_relations, opts = {})
        open(File.join(@dir, "README"), "a+") do |f|
          f.puts "Create rank #{rank} index: #{Time.now}"
          f.puts "  #{opts[:message]}" if opts[:message]
        end
        @db_zero.open_write do |db_zero|
          @db_equality.open_write do |db_eq|
            file = file_relation(rank)
            open(file, "w") do |f|
              method_to_generate_relations.call(rank) do |word, rel|
                if rel = simplify_relation_with_finding_zero(rank, rel)
                  f.puts rel
                end
              end
            end
            file_sort = "#{file}.sort"
            improve_relation(rank, file, file_sort)
            FileUtils.rm(file_sort)
          end
        end
      end

      # @param [Integer,nil] rank Rank number. If the value is nil, all data is dumped.
      def dump(rank = nil)
        @db_zero.dump(File.join(@dir, "zero.dump"), :rank => rank)
        @db_equality.dump(File.join(@dir, "eq.dump"), :rank => rank)
      end

      def each_relation(rank_range, &block)
        rank_range.each do |rank|
          unless created?(rank)
            raise "Index for rank #{rank} does not created."
          end
          open(file_relation(rank), "r") do |f|
            while l = f.gets
              l.strip!
              yield(@parse_relation_string.call(l))
            end
          end
        end
        @db_equality.open_read do |db_eq|
          @db_equality.each_class(rank_range) do |rep, words|
            words.each do |w|
              if rep != w
                yield(@parse_relation_string.call("#{rep} - #{w}"))
              end
            end
          end
        end
      end
    end
  end
end
