require "matrix"

module NPW
  class FiniteTypeInvariant
    # @param [String] tsv_path Path of tsv file
    # @yield [src] Block to parse string
    # @yieldparam src [String] Source string of a word
    # @yieldreturn [NPW::Word,NPW::NanoWord] Return some type of words
    def initialize(tsv_path, &parse)
      @parse = parse
      @filename = tsv_path
      @gamma_data = {}
      @modulo = nil
      @rank_min = nil
      @rank_max = nil
      open(@filename, "r").read.split("\n").each do |str|
        if str[0] == "#"
          @modulo = str[1..-1].split.map(&:to_i)
        else
          ary = str.split("\t")
          word = @parse.call(ary[0])
          if !@rank_min || word.rank < @rank_min
            @rank_min = word.rank
          end
          if !@rank_max || word.rank > @rank_max
            @rank_max = word.rank
          end
          @gamma_data[word.to_s] = Vector[*ary[1].split.map(&:to_i)]
        end
      end
    end

    # @param [NPW::Word,NPW::NanoWord] word Some type of words that must have the method 'subwords'
    # @return [Vector] The value of _word_
    def map(word)
      vec = Vector[*([0] * @modulo.size)]
      word.subwords(:rank => @rank_min..@rank_max).each do |w|
        w.adjust_alphabet!
        if v = @gamma_data[w.to_s]
          vec += v
        end
      end
      vec.map.with_index do |n, i|
        if @modulo[i] == 0
          n
        else
          n % @modulo[i]
        end
      end
    end
  end
end
