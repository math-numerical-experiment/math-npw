require "matrix"

module NPW
  class MatrixDB
    include NPW::DBKyotoCabinet
    include NPW::DBKyotoCabinet::Utils

    attr_reader :type, :row_size, :column_size, :modulo

    KEY_INFO = NPW::DBKyotoCabinet::Utils.pack_number(-1)
    MODULO_USE_INTEGER = 0      # Zero or negative number
    TYPE_ROW = 0
    TYPE_COLUMN = 1

    # @param [String] path File path
    # @param [Hash] opts Options
    # @option [:column,:row] :type Data type
    # @option [Integer] :row Row number
    # @option [Integer] :column Column number
    # @option [Integer] :modulo Modulo number
    # @option [boolean] :load Load from database. If the specified file does not exist, an error raises.
    # @option [Hash] :kyotocabinet Options of kyotocabinet database
    # @note We must set three options :type, :row, and :column or one option :load.
    #   Otherwise, we meet an error.
    def initialize(path, opts = {})
      set_kyotocabinet_path(path)
      set_kyotocabinet_options(opts[:kyotocabinet]) if opts[:kyotocabinet]
      if opts[:load]
        if kyotocabinet_on_memory?
          raise "Can not load database on memory: #{get_kyotocabinet_path}"
        elsif !File.exist?(get_kyotocabinet_path)
          raise "Database #{get_kyotocabinet_path} does not exist"
        end
        open_read do |db|
          if val = db.get(KEY_INFO)
            type_num, @row_size, @column_size, @modulo = unpack_numbers(val)
            @type = ((type_num == 0) ? :row : :column)
          end
        end
      else
        @type = opts[:type]
        unless (@type == :row || @type == :column)
          raise "Invalid type of matrix database: #{@type}"
        end
        @row_size = opts[:row]
        @column_size = opts[:column]
        @modulo = opts[:modulo] || MODULO_USE_INTEGER
        open_write if kyotocabinet_on_memory?
        write_matrix_prop
      end
    end

    def write_matrix_prop
      open_write do |db|
        db.set(KEY_INFO, pack_numbers([@type == :row ? TYPE_ROW : TYPE_COLUMN, @row_size, @column_size, @modulo]))
      end
    end
    private :write_matrix_prop

    def change_type(output_path)
      res = self.class.new(output_path, :type => (@type == :row ? :column : :row), :row => @row_size, :column => @column_size)
      num_items = (@type == :row ? @row_size : @column_size)
      res.open("w") do |r|
        open("r") do |mat|
          num_items.times do |n|
            unless (ary = get_elements(n)).empty?
              ary.each do |ind, val|
                r.kyotocabinet_db.append(pack_number(ind), pack_numbers([n, val]))
              end
            end
          end
        end
      end
      res
    end

    def close
      close_kyotocabinet
    end

    def open(mode, &block)
      if block_given?
        case mode
        when /^r/
          open_read do |db|
            yield(self)
          end
        when /^w/
          open_write do |db|
            yield(self)
          end
        else
          raise "Invalid mode: #{mode}"
        end
      else
        case mode
        when /^r/
          open_read
        when /^w/
          open_write
        else
          raise "Invalid mode: #{mode}"
        end
        self
      end
    end

    def [](i, j)
      if @type == :row
        db_num = i
        key_num = j
      else
        db_num = j
        key_num = i
      end
      if val = get_elements(db_num).assoc(key_num)
        val[1]
      else
        0
      end
    end

    # @param [Array] data_ary An array of arrays that is row
    def set(data_ary)
      if @type == :row
        self.open("w") do |mat|
          raise "Invalid data size" if data_ary.size != @row_size
          data_ary.each_with_index do |row_ary, num|
            raise "Invalid row data size" if row_ary.size != @column_size
            ary = []
            row_ary.each_with_index do |val, i|
              ary << [i, val] if val != 0
            end
            set_elements(num, ary)
          end
        end
      else
        self.open("w") do |mat|
          raise "Invalid data size" if data_ary.size != @row_size
          raise "Invalid row data size" unless data_ary.all? { |ary| ary.size == @column_size }
          column_size.times do |column_num|
            column_data = []
            row_size.times do |row_num|
              if (val = data_ary[row_num][column_num]) != 0
                column_data << [row_num, val]
              end
            end
            set_elements(column_num, column_data) unless column_data.empty?
          end
        end
      end
      self
    end

    def transpose!
      @type = (@type == :row ? :column : :row)
      write_matrix_prop
      self
    end

    def identity?
      return false unless @row_size == @column_size
      open("r") do |mat|
        @row_size.times do |num|
          if !(ary = get_elements(num)) || ary.size != 1 || ary[0][0] != num || ary[0][1] != 1
            return false
          end
        end
      end
      true
    end

    # Note that we should usually row(num) and column(num)
    def get_elements(num)
      elements = []
      if val = kyotocabinet_db.get(pack_number(num))
        nums = unpack_numbers(val)
        i = 0
        while ind = nums[i]
          elements << [ind, nums[i + 1]]
          i += 2
        end
      end
      elements
    end

    def set_elements(num, ary)
      kyotocabinet_db.set(pack_number(num), pack_numbers(ary.flatten))
    end
    private :set_elements

    # In this method, we automatically open the database with write mode.
    def set_elements_bulk(element_data, element_size)
      data = {}
      element_data.each do |num, ary|
        raise "Elements of number #{num} does not exist" if num < 0 || num >= element_size
        data[pack_number(num)] = pack_numbers(ary.flatten)
      end
      self.open("w") do |mat|
        kyotocabinet_db.set_bulk(data)
      end
    end
    private :set_elements_bulk

    def row(num)
      raise "The type of the matrix is different: #{@type}" unless @type == :row
      raise "Row of number #{num} does not exist" if num < 0 || num >= @row_size
      get_elements(num)
    end

    def row_set(num, ary)
      raise "The type of the matrix is different: #{@type}" unless @type == :row
      raise "Row of number #{num} does not exist" if num < 0 || num >= @row_size
      set_elements(num, ary)
    end

    def row_set_bulk(row_data)
      raise "The type of the matrix is different: #{@type}" unless @type == :row
      set_elements_bulk(row_data, @row_size)
    end

    def column(num)
      raise "The type of the matrix is different: #{@type}" unless @type == :column
      raise "Column of number #{num} does not exist" if num < 0 || num >= @column_size
      get_elements(num)
    end

    def column_set(num, ary)
      raise "The type of the matrix is different: #{@type}" unless @type == :column
      raise "Column of number #{num} does not exist" if num < 0 || num >= @column_size
      set_elements(num, ary)
    end

    def column_set_bulk(column_data)
      raise "The type of the matrix is different: #{@type}" unless @type == :column
      set_elements_bulk(column_data, @column_size)
    end

    # @param [Integer] row_num Row number of a row to be multiplied
    # @param [NPW::MatrixDB] other A matrix to be multiplied
    # @param [Hash] cache Cache of the matrix other created by NPW::MatrixDB#load_column_cache_hash
    def row_multiplied_by_other(row_num, other, cache)
      row_vals_new = []
      row_vals = self.get_elements(row_num)
      unless row_vals.empty?
        other.column_size.times do |column_num|
          column_vals = cache[column_num]
          next if column_vals.empty?
          val_new = 0
          col_num = 0
          row_vals.each do |ind, val|
            while ind2 = column_vals[col_num]
              if ind < ind2
                break
              end
              col_num += 2
              if ind == ind2
                val_new += (column_vals[col_num - 1] * val)
                break
              end
            end
            break unless ind2
          end
          row_vals_new << [column_num, val_new] if val_new != 0
        end
      end
      row_vals_new
    end
    private :row_multiplied_by_other

    def load_column_cache_hash(mat)
      cache = {}
      mat.column_size.times do |column_num|
        if val = mat.kyotocabinet_db.get(pack_number(column_num))
          cache[column_num] = unpack_numbers(val)
        else
          cache[column_num] = []
        end
      end
      cache
    end
    private :load_column_cache_hash

    def dump_row_data_to_database(m, mat_data)
      m.row_set_bulk(mat_data)
      mat_data.clear
    end
    private :dump_row_data_to_database

    def dump_packed_column_data_to_database(m, mat_data)
      raise "The type of the matrix is different: #{m.type}" unless m.type == :column
      m.open("w") do |mat|
        mat.kyotocabinet_db.set_bulk(mat_data)
      end
      mat_data.clear
    end
    private :dump_packed_column_data_to_database

    MAX_SIZE_MATRIX_DATA = 10000

    def mul_ruby(other, path, modulo = nil)
      if @column_size != other.row_size
        raise "Matrix sizes does not match"
      elsif !(@type == :row && other.type == :column)
        raise "Not implemented"
      end
      res = self.class.new(path, :type => :row, :row => @row_size, :column => other.column_size, :modulo => modulo)
      raise "Not implemented" if res.kyotocabinet_on_memory?
      cache = nil
      other.open("r") do |mat|
        cache = load_column_cache_hash(mat)
      end
      NPW::Settings.max_process_number.times do |process_num|
        fork do
          mat_data = {}
          self.open("r")
          self.row_size.times do |row_num|
            if (row_num % NPW::Settings.max_process_number) == process_num
              row_vals_new = row_multiplied_by_other(row_num, other, cache)
              mat_data[row_num] = row_vals_new if !row_vals_new.empty?
              dump_row_data_to_database(res, mat_data) if mat_data.size > MAX_SIZE_MATRIX_DATA
            end
          end
          self.close
          dump_row_data_to_database(res, mat_data)
        end
      end
      Process.waitall
      res
    end

    def create_cache
      NPW::MatrixDB::Cache.new(get_kyotocabinet_path, (@type == :row ? @row_size : @column_size))
    end

    # @param [NPW::MatrixDB] other Matrix to be multiplied
    # @param [String] path Path of new matrix database
    # @param [Integer] modulo Modulo number
    def mul(other, path, modulo = nil)
      # return mul_ruby(other, path, modulo)
      if @column_size != other.row_size
        raise "Matrix sizes does not match"
      elsif !(@type == :row && other.type == :column)
        raise "Not implemented"
      end
      res = self.class.new(path, :type => :row, :row => @row_size, :column => other.column_size, :modulo => modulo)
      raise "Not implemented" if res.kyotocabinet_on_memory?
      cache_other = other.create_cache
      NPW::Settings.max_process_number.times do |process_num|
        fork do
          mul_rows(get_kyotocabinet_path, @row_size, cache_other, path, NPW::Settings.max_process_number, process_num)
        end
      end
      Process.waitall
      res
    end

    def transform_by_ruby(transform_mat, additional_rels, path, modulo = nil)
      unless (@type == :row && transform_mat.type == :column && @column_size == transform_mat.row_size)
        raise "Not implemented"
      end
      additional_rels.open_read
      res = self.class.new(path, :type => :row, :row => @row_size + additional_rels.size, :column => transform_mat.column_size, :modulo => modulo)
      raise "Not implemented" if res.kyotocabinet_on_memory?
      cache = nil
      transform_mat.open("r") do |mat|
        cache = load_column_cache_hash(mat)
      end
      res.open("w")
      row_num_already_set = additional_rels.dump_to_matrix(res)
      res.close

      NPW::Settings.max_process_number.times do |process_num|
        fork do
          mat_data = {}
          self.open("r")
          additional_rels.open_read

          self.row_size.times do |row_num|
            if (row_num % NPW::Settings.max_process_number) == process_num
              row_num_new = row_num + row_num_already_set
              row_data = {}
              row_multiplied_by_other(row_num, transform_mat, cache).each do |ind, val|
                if num_same = additional_rels[ind]
                  if num_same >= 0
                    if row_data[num_same]
                      row_data[num_same] += val
                    else
                      row_data[num_same] = val
                    end
                  end
                else
                  if row_data[ind]
                    row_data[ind] += val
                  else
                    row_data[ind] = val
                  end
                end
              end
              row_vals_new = []
              row_data.to_a.sort_by { |ary| ary[0] }.each do |ind, val|
                row_vals_new << [ind, val] unless val == 0
              end
              if !row_vals_new.empty?
                if row_vals_new[0][1] < 0
                  row_vals_new.map! do |ary|
                    ary[1] = -ary[1]
                    ary
                  end
                end
                mat_data[row_num_new] = row_vals_new
              end
              dump_row_data_to_database(res, mat_data) if mat_data.size > MAX_SIZE_MATRIX_DATA
            end
            dump_row_data_to_database(res, mat_data)
          end

          self.close
          additional_rels.close_kyotocabinet
          dump_row_data_to_database(res, mat_data)
        end
      end
      Process.waitall
      res
    end

    # To transform matrix of relations by using classification of words.
    # @param [NPW::MatrixDB] transform_mat Matrix to be multiplied
    # @param [NPW::MatrixDB::AdditionalRels] additional_rels Pairs of numbers that coincides with each other
    # @param [String] path Path of new matrix database
    # @param [Integer] modulo Modulo number
    def transform_by(transform_mat, additional_rels, path, modulo = nil)
      # return transform_by_ruby(transform_mat, additional_rels, path, modulo)
      unless (@type == :row && transform_mat.type == :column && @column_size == transform_mat.row_size)
        raise "Not implemented"
      end
      additional_rels.open_read
      res = self.class.new(path, :type => :row, :row => @row_size + additional_rels.size, :column => transform_mat.column_size, :modulo => modulo)
      raise "Not implemented" if res.kyotocabinet_on_memory?
      res.open("w")
      row_num_already_set = additional_rels.dump_to_matrix(res)
      res.close
      additional_rels.close_kyotocabinet
      cache_transform = transform_mat.create_cache

      NPW::Settings.max_process_number.times do |process_num|
        fork do
          rows_transform_by(get_kyotocabinet_path, @row_size, cache_transform, path, additional_rels.get_kyotocabinet_path, row_num_already_set, NPW::Settings.max_process_number, process_num)
        end
      end
      Process.waitall
      res
    end

    # To get transformation of generators to lead Smith normal form
    # @param [String] transform_src Path of source file of transformation matrix
    # @param [String] transform_src_mat_path Path of matrix created from transform_src
    # @param [String] output_path Path of output file
    def composite_transformation(transform_src, transform_src_mat_path, output_path)
      raise "Matrix must be row type" if @type == :column
      m = NPW::MatrixDB.new(transform_src_mat_path, :type => :column, :row => @row_size, :column => @column_size)
      m.open("w") do |mat|
        Kernel.open(transform_src, "r") do |f|
          row_num = 0
          while l = f.gets
            l.split.map(&:to_i).each_with_index do |val, column_num|
              if val != 0
                mat.kyotocabinet_db.append(pack_number(column_num), pack_numbers([row_num, val]))
              end
            end
            row_num += 1
          end
        end
      end
      res = self.mul(m, output_path.sub(/\.[^.]+$/, "_composite.kch"), nil)
      Kernel.open(output_path, "w") do |f|
        res.debug_print(f)
      end
    end

    # @return [boolean] If the matrix is upper triangular, true. Otherwise, false.
    def upper_triangular?
      open("r") do |mat|
        if @type == :row
          mat.row_size.times do |row_num|
            if !(ary = get_elements(row_num)).empty? && ary[0][0] < row_num
              return false
            end
          end
        else
          mat.column_size.times do |column_num|
            if !(ary = get_elements(column_num)[0]).empty? && ary[-1] > column_num
              return false
            end
          end
        end
      end
      true
    end

    def inverse_of_upper_triangular_ruby(path, opts = {})
      unless (@type == :row && @row_size == @column_size)
        raise "Not implemented"
      end
      cache = nil
      self.open("r") do |mat|
        cache = load_column_cache_hash(mat)
        row_size.times do |row_num|
          row_vals = cache[row_num]
          if row_vals.size < 2 || !(row_vals[0] == row_num && row_vals[1] == 1)
            raise "Not upper triangular matrix: ind=#{row_vals[0].inspect}, val=#{row_vals[1].inspect}"
          end
        end
      end
      res = self.class.new(path, :type => :column, :row => @row_size, :column => @row_size, :modulo => @modulo)
      NPW::Settings.max_process_number.times do |process_num|
        fork do
          mat_packed_data = {}
          ind_one = row_size - 1
          while ind_one >= 0
            if (ind_one % NPW::Settings.max_process_number) == process_num
              column_vals_new = []
              row_num = ind_one
              while row_num >= 0
                val_new = (ind_one == row_num ? 1 : 0)
                row_vals = cache[row_num]
                val_ind = 2
                res_ind = 0
                while ind = row_vals[val_ind]
                  while ind2 = column_vals_new[res_ind]
                    if ind <= ind2
                      if ind == ind2
                        val_new = val_new - row_vals[val_ind + 1] * column_vals_new[res_ind + 1]
                        res_ind += 2
                      end
                      break
                    end
                    res_ind += 2
                  end
                  val_ind += 2
                end
                if val_new != 0
                  column_vals_new.unshift(row_num, val_new)
                end
                row_num -= 1
              end
              unless column_vals_new.empty?
                mat_packed_data[pack_number(ind_one)] = pack_numbers(column_vals_new)
                if mat_packed_data.size > MAX_SIZE_MATRIX_DATA
                  dump_packed_column_data_to_database(res, mat_packed_data)
                end
              end
            end
            ind_one -= 1
          end
          dump_packed_column_data_to_database(res, mat_packed_data) unless mat_packed_data.empty?
        end
      end
      Process.waitall
      res
    end

    # @param [String] path Path of created matrix database
    # @param [Hash] opts Options
    # @option [boolean] :test Test whether the matrix is upper triangular
    # @note The matrix must be upper triangular and its diagonal elements are one.
    def inverse_of_upper_triangular(path, opts = {})
      # return inverse_of_upper_triangular_ruby(path, opts)
      unless (@type == :row && @row_size == @column_size)
        raise "Not implemented"
      end
      res = self.class.new(path, :type => :column, :row => @row_size, :column => @row_size, :modulo => @modulo)
      cache = create_cache
      unless cache.upper_triangular_diagonal_ones?
        raise "Not suitable upper triangular matrix"
      end
      NPW::Settings.max_process_number.times do |process_num|
        fork do
          inverse_of_upper_triangular_step(cache, path, NPW::Settings.max_process_number, process_num);
        end
      end
      Process.waitall
      res
    end

    # @param [Hash] opts Options
    # @option opts [boolean] :unique Delete duplicated rows
    def dump_for_sparse_snf(out = nil, opts = {})
      out ||= get_kyotocabinet_path.sub(/\.kch$/, ".txt")
      if opts[:unique]
        raise "Not implemented" unless @type == :row
        Kernel.open(out, "w") do |f|
          open("r") do |mat|
            f.puts "# #{row_size} #{column_size} #{modulo}"
          end
        end
        filename = FileName.new(out)
        path_tmp = filename.create
        Kernel.open(path_tmp, "w") do |f|
          open("r") do |mat|
            @row_size.times do |row_num|
              f.puts mat.get_elements(row_num).flatten.map(&:to_s).join(" ")
            end
          end
        end
        system("sort -n #{path_tmp} | uniq >> #{out}")
        FileUtils.rm(path_tmp)
      else
        Kernel.open(out, "w") do |f|
          open("r") do |mat|
            f.puts "# #{row_size} #{column_size} #{modulo}"
            if @type == :row
              @row_size.times do |row_num|
                f.puts mat.get_elements(row_num).flatten.map(&:to_s).join(" ")
              end
            else
              raise "Not implemented"
            end
          end
        end
      end
    end

    # @param [IO,nil] out IO to output. If out is nil, we obtain string object.
    def to_gap_string(out = nil)
      if @type == :row
        io = out || ""
        io << "["
        open("r") do |mat|
          @row_size.times do |row_num|
            io << "," if row_num > 0
            io << "["
            cur_ind = 0
            mat.get_elements(row_num).each do |ind, val|
              while cur_ind < ind
                io << "," if cur_ind > 0
                io << "0"
                cur_ind += 1
              end
              io << "," if cur_ind > 0
              io << val.to_s
              cur_ind += 1
            end
            while cur_ind < @column_size
              io << "," if cur_ind > 0
              io << "0"
              cur_ind += 1
            end
            io << "]"
          end
        end
        io << "]"
        out ? true : io
      else
        # Not effective algorithm
        io = out || ""
        io << "["
        open("r") do |mat|
          @row_size.times do |row_num|
            io << "," if row_num > 0
            io << "["
            @column_size.times do |column_num|
              io << "," if column_num > 0
              io << mat[row_num, column_num].to_s
            end
            io << "]"
          end
        end
        io << "]"
        out ? true : io
      end
    end

    def debug_print(io = $stdout)
      out = io || ""
      if @type == :row
        open("r") do |mat|
          mat.row_size.times do |row_num|
            column_count = 0
            get_elements(row_num).each do |column_num, val|
              while column_count < column_num
                out << " " if column_count > 0
                out << "0"
                column_count += 1
              end
              out << " " if column_count > 0
              out << val.to_s
              column_count += 1
            end
            while column_count < @column_size
              out << " " if column_count > 0
              out << "0"
              column_count += 1
            end
            out << "\n"
          end
        end
      else
        # Not effective algorithm
        open("r") do |mat|
          mat.row_size.times do |row_num|
            mat.column_size.times do |column_num|
              out << " " if column_num > 0
              out << "#{mat[row_num, column_num]}"
            end
            out << "\n"
          end
        end
      end
      io ? true : out
    end

    def to_matrix
      # Not effective algorithm
      data = nil
      open("r") do |mat|
        data = @row_size.times.map do |row_num|
          @column_size.times.map do |column_num|
            mat[row_num, column_num]
          end
        end
      end
      Matrix[*data]
    end

    def valid?
      valid_key = true
      valid_nums = true
      item_num = (@type == :row ? @row_size : @column_size)
      open("r") do |mat|
        res = kyotocabinet_db.each_key do |packed_num_ary|
          n = unpack_number(packed_num_ary[0])
          if n < -1 || n >= item_num
            puts "Invalid key: #{n}"
            valid_key = false
          end
        end
        unless res
          puts "Error occurs in Kyotocabinet::DB#each_key"
          valid_key = false
        end
        item_num.times do |num|
          if ary = get_elements(num)
            (1...(ary.size)).each do |n|
              if ary[n - 1][0] >= ary[n][0]
                puts "Invalid value: [n-1] #{ary[n - 1][0]}, [n] #{ary[n][0]}"
                valid_nums = false
              end
            end
            ary.each do |ind, val|
              if val == 0
                puts "Invalid value: #{ind}th value is #{val}"
                valid_nums = false
              end
            end
          end
        end
      end
      valid_key && valid_nums
    end

    def self.load_text_for_sparse_snf(path_db, path_text)
      m = nil
      row_num = 0
      Kernel.open(path_text, "r") do |f|
        while l = f.gets
          if /^#/ =~ l
            ary = l.split
            ary.shift
            ary.map!(&:to_i)
            m = self.new(path_db, :type => :row, :row => ary[0], :column => ary[1], :modulo => ary[2])
            m.open("w")
          elsif !m
            raise "Invalid data of matrix"
          else
            ary = l.split.map(&:to_i).each_slice(2).to_a
            m.row_set(row_num, ary)
            row_num += 1
          end
        end
      end
      m.close if m
      m
    end

    class AdditionalRels
      include NPW::DBKyotoCabinet
      include NPW::DBKyotoCabinet::Utils

      def initialize(path, opts = {})
        set_kyotocabinet_path(path)
        set_kyotocabinet_options(opts[:kyotocabinet]) if opts[:kyotocabinet]
        open_write do |db|
        end
      end

      def [](n)
        if val = kyotocabinet_db.get(pack_number(n))
          unpack_number(val)
        else
          nil
        end
      end

      def []=(n1, n2)
        kyotocabinet_db.set(pack_number(n1), pack_number(n2))
      end

      def set_bulk(data_hash)
        data_bulk = {}
        data_hash.each do |n1, n2|
          data_bulk[pack_number(n1)] = pack_number(n2)
        end
        kyotocabinet_db.set_bulk(data_bulk)
      end

      def size
        kyotocabinet_db.count
      end

      # @param [NPW::MatrixDB] res Output matrix
      # @return [Ineteger] Number of dumped relations
      def dump_to_matrix(res)
        row_num_already_set = 0
        complete = kyotocabinet_db.each do |n1_src, n2_src|
          n1 = unpack_number(n1_src)
          n2 = unpack_number(n2_src)
          if n2 < 0               # If n2 is negative, the generator corresponding to n1 is zero.
            res.row_set(row_num_already_set, [[n1, 1]])
          else
            if n1 < n2
              ary = [[n1, 1], [n2, -1]]
            else
              ary = [[n2, 1], [n1, -1]]
            end
            res.row_set(row_num_already_set, ary)
          end
          row_num_already_set += 1
        end
        unless complete
          raise "Error raises in each"
        end
        row_num_already_set
      end
    end
  end
end

require_relative "../../../../ext/matrix_db/npw_matrix_db.so"
