require "npw/application/polyak_algebra/snf"
require "npw/application/polyak_algebra/index_generators"
require "npw/application/polyak_algebra/finite_type_invariant"
require "npw/application/polyak_algebra/word_classify"

module NPW

  # We should define the following methods in child class of NPW::TruncatedPolyakAlgebra.
  # - each_generator(rank, &block)
  # - files_enum_relations
  # - parse_generator_string(str)
  # - parse_relation_string(str)
  # - zero_word?(word)
  # - modulo_number(rank_n)
  # - each_word(rank)
  # - three_class(word)
  # - words_by_reduced_move(word)
  # 
  # The methods each_word(rank), three_class(word), and words_by_reduced_move(word) are based on
  # usual Reidemeister moves.
  # The others are linearlized Reidemeister moves.
  # Note that we use parse_generator_string in both cases.
  class TruncatedPolyakAlgebra
    class Output
      attr_reader :directory

      # @param [String] dir Output directory
      # @param [boolean] load Load existing file.
      #   If dir does not exist or load is false, we create new directory.
      def initialize(dir, load)
        if !File.exist?(dir) || !load
          @directory = FileName.create(dir, :directory => :self)
        else
          @directory = dir
        end
      end

      def filepath(name)
        File.join(@directory, name)
      end

      def open_file(name, mode = "a+", &block)
        path = filepath(name)
        if block_given?
          Kernel.open(path, mode, &block)
        else
          Kernel.open(path, mode)
        end
      end

      def open_readme(mode = "a+", &block)
        open_file("README", mode, &block)
      end

      def put_name(name)
        open_file("NAME", "w") { |f| f.puts name }
      end

      def get_name
        open_file("NAME", "r") { |f| f.read.strip }
      end
    end

    # @param [String] name A name to distinguish word types. We can use any strings.
    # @param [Hash] opts An argument of NPW::SNF except :index option
    # @option opts [String] :index Path of index directory
    def initialize(name, opts = {})
      @name = name
      if dir = opts[:index]
        @index = NPW::TruncatedPolyakAlgebra::Index.new(@name, dir, method(:parse_relation_string), method(:zero_word?))
      end
      @snf_opts = opts.except(:index)
    end

    # @option [Integer] rank Rank number of generators
    def each_generator(rank, &block)
      raise "We must define #{self.class.to_s}#each_generator"
    end

    # @return [Hash] Pairs of file name and method name to generate relations.
    # @note The argument of methods to generate relations is rank number.
    # The methods must not delete words of rank n+1 or more.
    def files_enum_relations
      raise "We must define #{self.class.to_s}#files_enum_relations"
    end

    def parse_generator_string(str)
      raise "We must define #{self.class.to_s}#parse_generator_string"
    end

    def parse_relation_string(str)
      raise "We must define #{self.class.to_s}#parse_relation_string"
    end

    def zero_word?(word)
      raise "We must define #{self.class.to_s}#zero_word?"
    end

    def each_word(rank)
      raise "We must define #{self.class.to_s}#each_word"
    end

    # @return [Array] An array of words of 3-class
    def three_class(word)
      raise "We must define #{self.class.to_s}#three_class"
    end

    # @return [Array] An array of words transformed by reduced moves
    def words_by_reduced_move(word)
      raise "We must define #{self.class.to_s}#words_by_reduced_move"
    end

    def modulo_number(rank_n)
      nil
    end

    def calc_save_snf(out, rank_n, all_rels, generators, modulo, db_classify)
      out.open_readme { |f| f.puts "\nSNF calculation starts: #{Time.now}" }
      snf = NPW::SNF.new(out.directory, @snf_opts)
      grp = snf.calc_gn(all_rels, generators, modulo, db_classify, rank_n)
      out.open_readme { |f| f.puts "Result by #{snf.type}: #{Time.now}\n  G_#{rank_n}: #{grp}" }
    end
    private :calc_save_snf

    def each_generator_with_index(rank, &block)
      @index.generator_without_zero(method(:each_generator), rank, &block)
    end

    def method_each_generator
      if @index
        method(:each_generator_with_index)
      else
        method(:each_generator)
      end
    end

    def get_generators(out, rank_n, use_db, load)
      if use_db
        path = out.filepath("generators.kch")
        generator_class = NPW::Relation::GeneratorInDB
      else
        path = out.filepath("generators.txt")
        generator_class = NPW::Relation::GeneratorInText
      end
      generator_class.new(path, load, method_each_generator, 2..rank_n, method(:parse_generator_string))
    end
    private :get_generators

    def truncate_words_more_than_n(rel, rank_n)
      rel.modify!(:simplify => { :adjust => true }) do |trm|
        if zero_word?(trm.word.to_s) || trm.word.rank >= rank_n + 1
          trm.delete
        end
      end
      rel.terms.empty? ? nil : rel
    end
    private :truncate_words_more_than_n

    # Save relations to a file. This method remove words of rank n+1 or more before saving relations.
    def generate_save_relations(out, rank_n, &check_relation)
      files_enum_relations.each do |name, method_name|
        relations_src = out.filepath(name)
        unless File.exist?(relations_src)
          open(relations_src, "w") do |f|
            (2..(rank_n + 1)).each do |rank|
              __send__(method_name, rank) do |word, rel|
                rel = truncate_words_more_than_n(rel, rank_n)
                if rel && (!check_relation || check_relation.call(word, rel))
                  f.puts "#{word}\t#{rel}"
                end
              end
            end
          end
        end
      end
    end
    private :generate_save_relations

    def filepath_relations_from_index(out)
      out.filepath("relations_from_index.txt")
    end
    private :filepath_relations_from_index

    def save_relations(out, rank_n, &check_relation)
      if @index
        path_index = filepath_relations_from_index(out)
        unless File.exist?(path_index)
          open(path_index, "w") do |f|
            @index.each_relation(2..(rank_n + 1)) do |rel|
              rel = truncate_words_more_than_n(rel, rank_n)
              if rel && (!check_relation || check_relation.call(word, rel))
                f.puts rel
              end
            end
          end
        end
      else
        generate_save_relations(out, rank_n, &check_relation)
      end
    end
    private :save_relations

    def load_all_relations(out)
      path_relations_uniq = out.filepath("relations_uniq.txt")
      unless File.exist?(path_relations_uniq)
        if @index
          path_index = filepath_relations_from_index(out)
          unless File.exist?(path_index)
            raise "File #{path_index} does not exist."
          end
          command = "sort #{path_index} | uniq > #{path_relations_uniq}"
        else
          files = files_enum_relations.map do |name, method_name|
            "\"#{out.filepath(name)}\""
          end
          command = "sed -r -e s\"/^[^\\t]+\\t?(.*)$/\\1/\" #{files.join(" ")} | sort | uniq > #{path_relations_uniq}"
        end
        # puts command
        system(command)
      end
      NPW::RelationSet::LazyLoading.new(path_relations_uniq, method(:parse_relation_string))
    end
    private :load_all_relations

    def default_message
      nil
    end
    private :default_message

    # @option opts [String] :output Output directory
    # @option opts [boolean] :load Load already existing data
    # @option opts [boolean] :db Use database of generators
    # @option opts [String,nil] :classify Path of classification database
    # @option opts [String] :message Message to output to README.
    #    We use first non-nil object in the option opts[:message], the method default_message, and the constant DEFAULT_MESSAGE.
    def calc(rank_n, opts = {})
      message = opts[:message] || default_message || self.class.const_get("DEFAULT_MESSAGE")
      out = NPW::TruncatedPolyakAlgebra::Output.new(opts[:output] || "g#{rank_n}_result", opts[:load])
      out.open_readme do |f|
        f.puts "G#{rank_n}: #{Time.now}"
        f.puts message if message
      end
      out.put_name(@name)

      generators = get_generators(out, rank_n, opts[:db], opts[:load])
      save_relations(out, rank_n)

      if opts[:type] != :relations
        all_rels = load_all_relations(out)
        calc_save_snf(out, rank_n, all_rels, generators, modulo_number(rank_n), opts[:classify])
      end
      out.directory
    end

    # @param [String] dir Output directory
    # @param [boolean] db Use database of generators
    def calc_transformation(rank_n, dir, db = nil)
      out = NPW::TruncatedPolyakAlgebra::Output.new(dir, true)
      snf = NPW::SNF.new(out.directory, @snf_opts)
      snf.calc_transformation(get_generators(out, rank_n, db, true))
    end

    def relations_for_index(rank)
      files_enum_relations.each do |name, method_name|
        __send__(method_name, rank) do |word, rel|
          yield(word, rel)
        end
      end
    end

    def create_index(rank_n, opts = {})
      unless @index
        raise "Index directory is not set."
      end
      (2..(rank_n + 1)).each do |rank|
        unless @index.created?(rank)
          @index.create(rank, method(:relations_for_index), opts)
        end
      end
    end

    def dump_index(rank = nil)
      unless @index
        raise "Index directory is not set."
      end
      @index.dump(rank)
    end

    def word_classify
      @word_classify ||= NPW::TruncatedPolyakAlgebra::WordClassify.new(method(:each_word), method(:three_class), method(:words_by_reduced_move), method(:parse_generator_string))
    end
  end
end
