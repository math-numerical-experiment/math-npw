module NPW
  class TruncatedPolyakAlgebra
    class WordClassify
      class MinWordDB
        include NPW::DBKyotoCabinet
        include NPW::DBKyotoCabinet::Utils

        def initialize(path)
          set_kyotocabinet_path(path)
        end

        # @param [String,NPW::Word] word
        # @param [Integer] number_reduced_word
        def set(word, number_reduced_word)
          kyotocabinet_db.set_with_error_check(word.to_s, pack_number(number_reduced_word))
        end

        # @param [String,NPW::Word] word
        # @return [Integer,nil] eq_class_num A number attached to an equivalence class.
        #   If the word is not registered, we get nil.
        def get(word)
          if val = kyotocabinet_db.get(word.to_s)
           unpack_number(val)
          else
            nil
          end
        end

        def set_bulk(words, eq_class_num)
          packed_equivalence_num = pack_number(eq_class_num)
          bulk_data = {}
          words.each do |word|
            bulk_data[word.to_s] = packed_equivalence_num
          end
          kyotocabinet_db.set_bulk(bulk_data)
        end

        def each(&block)
          kyotocabinet_db.each(&block)
        end
      end

      class EquivalenceDB
        include NPW::DBKyotoCabinet
        include NPW::DBKyotoCabinet::Utils

        KEY_COUNT = "count"

        def initialize(path)
          set_kyotocabinet_path(path)
        end

        def get_by_packed(eq_class_packed_num)
          kyotocabinet_db.get(eq_class_packed_num)
        end

        # @param [Integer] eq_class_num A number attached to an equivalence class
        # @return [String,nil] Word string of reduced first word in the equivalence class.
        #   If there is no an equivalence class attached the number, we get nil.
        def get(eq_class_num)
          get_by_packed(pack_number(eq_class_num))
        end

        # Register reduced word and get equivalence class number.
        # @param [String,NPW::Word] reduced_word
        # @return [Integer] Equivalence class number
        def register(reduced_word)
          if val = kyotocabinet_db.get(KEY_COUNT)
            n = unpack_number(val) + 1
          else
            n = 1
          end
          packed_num = pack_number(n)
          kyotocabinet_db.set_with_error_check(KEY_COUNT, packed_num)
          kyotocabinet_db.set_with_error_check(packed_num, reduced_word.to_s)
          n
        end

        def remove_bulk(packed_nums)
          kyotocabinet_db.remove_bulk(packed_nums)
        end

        def remove_bulk_by_number(nums)
          remove_bulk(nums.map { |n| pack_number(n) })
        end

        def each(&block)
          kyotocabinet_db.each do |eq_class_packed_num, word_str|
            yield([eq_class_packed_num, word_str]) unless eq_class_packed_num == KEY_COUNT
          end
        end
      end

      class DB
        include NPW::DBKyotoCabinet::Utils

        def initialize(dir)
          @min_word_db = NPW::TruncatedPolyakAlgebra::WordClassify::MinWordDB.new(File.join(dir, "min_word.kch"))
          @equivalence_db = NPW::TruncatedPolyakAlgebra::WordClassify::EquivalenceDB.new(File.join(dir, "equivalence.kch"))
        end

        def open(mode, &block)
          raise "Block does not given" unless block_given?
          begin
            case mode
            when /^r/
              @min_word_db.open_read
              @equivalence_db.open_read
            when /^w/
              @min_word_db.open_write
              @equivalence_db.open_write
            else
              raise "Invalid mode: #{mode}"
            end
            yield(self)
          ensure
            @min_word_db.close_kyotocabinet
            @equivalence_db.close_kyotocabinet
          end
        end

        def reduced_word(word)
          if num = @min_word_db.get(word.to_s)
            @equivalence_db.get(num)
          else
            nil
          end
        end

        # Get number attached to the equivalence class.
        # If the word is not registered, this method registers the word and get the equivalence class number.
        # @param [String] word Word string
        # @return [Integer] Number attached to the equivalence class of the word
        def equivalence_number(word)
          unless num = @min_word_db.get(word.to_s)
            num = @equivalence_db.register(word.to_s)
            @min_word_db.set(word.to_s, num)
          end
          num
        end

        # We merge some equivalence classes into one equivalence class.
        # @param [Array] words_to_delete Words to be deleted and merged into reduced_word
        # @param [String] reduced_word New reduced word
        def equivalence_class_merge(words_to_delete, reduced_word)
          words_to_delete = words_to_delete.map(&:to_s)
          reduced_word = reduced_word.to_s
          num_packed_vals = []
          equivalence_to_be_deleted = []
          @equivalence_db.each do |num_packed, word_str|
            if words_to_delete.include?(word_str)
              num_packed_vals << num_packed
              equivalence_to_be_deleted << word_str
            end
          end
          @equivalence_db.remove_bulk(equivalence_to_be_deleted)
          equivalence_num = equivalence_number(reduced_word)
          unless num_packed_vals.empty?
            words_to_be_changed = []
            @min_word_db.each do |word_str, num_packed|
              if num_packed_vals.include?(num_packed)
                words_to_be_changed << word_str
              end
            end
            unless words_to_be_changed.empty?
              @min_word_db.set_bulk(words_to_be_changed, equivalence_num)
            end
          end
        end

        # @param [Array] words_three_class An array of words of three class
        # @param [Integer] equivalence_num Number of equivalence class
        def set_reduced_words(words_three_class, equivalence_num)
          @min_word_db.set_bulk(words_three_class, equivalence_num)
        end

        # Each reduced word
        def each_reduced_word(&block)
          @equivalence_db.each do |eq_class_packed_num, word_str|
            yield(word_str)
          end
        end

        # Each pair of a word and its reduced word
        def each_word_and_reduced(&block)
          @min_word_db.each do |word_str, eq_class_packed_num|
            reduced_word_str = @equivalence_db.get_by_packed(eq_class_packed_num)
            yield([word_str, reduced_word_str])
          end
        end

        # @param [String,nil] output_file Output file path. If nil, we get an array of class of words.
        def dump(output_file = nil)
          data = Hash.new { |h, k| h[k] = [] }
          open(:read) do |db|
            @min_word_db.each do |word_str, eq_class_packed_num|
              data[eq_class_packed_num] << word_str
            end
            unless data.empty?
              if output_file
                Kernel.open(output_file, "w") do |f|
                  data.each do |eq_class_packed_num, words_str_ary|
                    reduced_word = @equivalence_db.get(unpack_number(eq_class_packed_num))
                    f.puts reduced_word
                    words_str_ary.each do |w|
                      f.puts w unless reduced_word == w
                    end
                    f.print "\n"
                  end
                end
              end
            end
          end
          if output_file
            output_file
          else
            data.values
          end
        end

        def words_of_one_word_class(rank)
          word_len = rank * 2
          count = Hash.new { |h, k| h[k] = 0 }
          open("read") do |db_classify|
            @min_word_db.each do |word, packed_num|
              if word.size <= word_len
                num = unpack_number(packed_num)
                reduced_word = @equivalence_db.get(num)
                count[reduced_word] += 1
              end
            end
          end
          words = []
          count.each do |reduced_word, c|
            if c == 1 && reduced_word.size == word_len
              words << reduced_word
            end
          end
          words.sort!
        end

        # If db1 includes db2, i.e., all words in db2 has same reduced words as them of db1.
        # @param [NPW::TruncatedPolyakAlgebra::WordClassify::DB] db1
        # @param [NPW::TruncatedPolyakAlgebra::WordClassify::DB] db2
        def self.include?(db1, db2)
          ret = true
          db1.open(:read) do |d1|
            db2.open(:read) do |d2|
              d2.each_word_and_reduced do |word, word_reduced2|
                word_reduced1 = d1.reduced_word(word)
                if !word_reduced1
                  puts "db1 does not has #{word}"
                  ret = false
                elsif word_reduced1 != word_reduced2
                  puts "Not coincide: #{word}: db1 #{word_reduced1}, db2 #{word_reduced2}"
                  ret = false
                end
              end
            end
          end
          ret
        end
      end

      def initialize(proc_each_word, proc_three_class, proc_words_by_reduced_move, proc_parse_generator_string)
        @proc_each_word = proc_each_word
        @proc_three_class = proc_three_class
        @proc_words_by_reduced_move = proc_words_by_reduced_move
        @proc_parse_generator_string = proc_parse_generator_string
      end

      # @return [Array] An array of words of 3-class
      def get_three_class(word)
        @proc_three_class.call(word)
      end

      # @return [Array] An array of words transformed by reduced moves
      def get_words_by_reduced_move(word)
        @proc_words_by_reduced_move.call(word)
      end

      # Suppose that for any word of rank n,
      # there is at most one word of rank n+1 equivalent to the word of rank n.
      def each_word(rank_max, &block)
        @proc_each_word.call(rank_max, &block)
      end

      # Get the word from the string str.
      # @param [String] str String of word
      def parse_string(str)
        @proc_parse_generator_string.call(str)
      end

      # @param [Array] words An array of words
      # @return [Array] An array of words of 3-classes from the argument words
      def get_words_of_three_class(words)
        words_three_class = []
        words.each do |word|
          words_three_class.concat(get_three_class(word))
        end
        words_three_class.sort!
        words_three_class.uniq!
        words_three_class
      end

      # @param [Array] words An array of words
      # @return [Array] An array of words transformed by reduced moves from the argument words
      def get_reduced_words(words)
        words_reduced = []
        words.each do |word|
          words_reduced.concat(get_words_by_reduced_move(word))
        end
        words_reduced.sort!
        words_reduced.uniq!
        words_reduced
      end
      private :get_reduced_words

      def get_db(dir_path)
        NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(dir_path)
      end

      # @param [NPW::TruncatedPolyakAlgebra::WordClassify::DB] db Writable database
      # @param [String] word_src Word string
      def register_word_on_db(db, word_src)
        return if db.reduced_word(word_src)

        words_three_class = []
        candidates_reduced_string = []

        words_last = [word_src]
        loop do
          words_three_class_new = get_words_of_three_class(words_last).delete_if do |w|
            if str = db.reduced_word(w)
              candidates_reduced_string << str unless candidates_reduced_string.include?(str)
              true
            else
              nil
            end
          end
          words_reduced_new = get_reduced_words(words_three_class_new)
          words_three_class.concat(words_three_class_new)
          words_last = words_reduced_new
          break if words_reduced_new.empty?
        end
        candidates_reduced_word = candidates_reduced_string.map { |str| parse_string(str) }
        words_three_class.concat(candidates_reduced_word)
        words_three_class.sort!
        words_three_class.uniq!
        # Note: words_three_class includes at least word_src.
        candidates_reduced_word << words_three_class[0]
        candidates_reduced_word.uniq!
        candidates_reduced_word.sort!
        # If candidates_reduced_string has more than one elements,
        # Gibson's algorithm to distinguish words does not hold.
        if candidates_reduced_word.size > 1
          puts "Reduced words are changed for #{word_src} and #{candidates_reduced_word.inspect}"
          db.equivalence_class_merge(candidates_reduced_string[1..-1], candidates_reduced_string[0])
        end
        equivalence_num = db.equivalence_number(candidates_reduced_word[0])
        db.set_reduced_words(words_three_class, equivalence_num)
      end

      # Save word classes classified by 3-class and reducible moves.
      # @param [Integer] rank Maximum rank number
      # @param [String] output_dir File path of database directory
      # @param [Hash] opts Options
      # @option opts [Integer] :progress Output progress per this count
      def save_classification(rank, output_dir, opts = {})
        count = 0
        if Integer === opts[:progress] && opts[:progress] > 0
          progress_count = opts[:progress]
        else
          progress_count = nil
        end
        dir_path = FileName.create(output_dir, :directory => :self)
        get_db(dir_path).open(:write) do |db|
          each_word(rank) do |word_src|
            puts "#{count}\t#{word_src}" if progress_count && ((count += 1) % progress_count) == 0
            register_word_on_db(db, word_src)
          end
        end
        dir_path
      end

      # @param [String] tsv_path Path of tsv file
      def finite_type_invariant(tsv_path)
        NPW::FiniteTypeInvariant.new(tsv_path, &@proc_parse_generator_string)
      end

      # @param [String] tsv_path Path of tsv file
      # @param [String] db_dir File path of database directory
      # @return [boolean] If the invariant is compatible with word classes in the database,
      #   we get true. Otherwise, false.
      def invariant_compatible?(tsv_path, db_dir)
        ret = true
        inv = finite_type_invariant(tsv_path)
        cache_reduced_value = Hash.new { |h, k| h[k] = inv.map(parse_string(k)) }
        get_db(db_dir).open(:read) do |db|
          db.each_word_and_reduced do |word_str, reduced_word_str|
            word = parse_string(word_str)
            unless (val = inv.map(word)) == cache_reduced_value[reduced_word_str]
              puts "Not coincide:\n#{word_str}\t#{val.inspect.strip}\n#{reduced_word_str}\t#{cache_reduced_value[reduced_word_str].inspect.strip}"
              ret = false
            end
          end
        end
        ret
      end

      # @param [String] tsv_path Path of tsv file
      # @param [String] db_dir File path of database directory
      # @return [boolean] If the invariant classify completely, then we get true. Otherwise, false.
      # @note This method does not validate the invariant itself.
      #    We must preliminarily test the invariant by the method invariant_compatible?.
      def invariant_classify?(tsv_path, db_dir, opts = {})
        rank_max = opts[:rank_max]
        invariant_values = Hash.new { |h, k| h[k] = [] }
        inv = finite_type_invariant(tsv_path)
        get_db(db_dir).open(:read) do |db|
          db.each_reduced_word do |reduced_word_str|
            if !rank_max || (reduced_word_str.size <= rank_max)
              val = inv.map(parse_string(reduced_word_str))
              invariant_values[val] << reduced_word_str
            end
          end
        end
        ret = true
        invariant_values.each do |val, words|
          unless words.size == 1
            puts "NOT distinguish: #{words.join(" ")}"
            p val.to_a
            ret = false
          end
        end
        ret
      end
    end
  end
end
