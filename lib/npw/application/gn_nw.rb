require "npw/application/polyak_algebra/truncated_polyak_algebra"

module NPW
  class GnOnNanoWord < NPW::TruncatedPolyakAlgebra
    DEFAULT_MESSAGE = "On nanowords"

    PATTERN_FIRST_TYPE = /([A-Z])\1/
    PATTERN_SECOND_TYPE = /([A-Z])([A-Z]).*\2\1/
    PATTERN_THIRD_TYPE = /([A-Z])([A-Z]).*\1([A-Z]).*(\2\3)/

    # @param [Hash] opts An argument of NPW::SNF
    def initialize(name, involution, check_set_s, opts = {})
      @involution = involution
      @check_set_s = check_set_s
      super(name, opts)
    end

    def non_zero_nanoword(ranks, &block)
      return to_enum(:non_zero_nanoword, ranks) unless block_given?
      ranks = [ranks] unless ranks.respond_to?(:each)
      ranks.each do |rank|
        NPW::NanoWord.each_nanoword(@involution, :rank => rank, :exclude => [PATTERN_FIRST_TYPE], &block)
      end
    end

    def zero_word?(word)
      PATTERN_FIRST_TYPE =~ word.to_s
    end

    def each_word(rank, &block)
      NPW::NanoWord.each_nanoword(@involution, :rank => rank, &block)
    end

    def each_generator(rank, &block)
      non_zero_nanoword(rank, &block)
    end

    def relations_2nd_from_one_word(word)
      words = []
      word.gauss_word.each_match(PATTERN_SECOND_TYPE) do |match_data|
        a, b = word.project_letters(match_data.begin(1), match_data.begin(2))
        if @involution[a] == b
          letters = match_data[1..2]
          s = "#{word} + #{word.transform(:delete => letters[0])} + #{word.transform(:delete => letters[1])}"
          words << NPW::NanoWord::Relation.new(s, @involution)
        end
      end
      words
    end

    def each_2nd_relations(rank, &block)
      return to_enum(:each_2nd_relations) unless block_given?
      non_zero_nanoword(rank) do |word|
        relations_2nd_from_one_word(word).each do |rel|
          yield([word, rel])
        end
      end
    end

    def relations_3rd_from_one_word(word)
      words = []
      word.gauss_word.each_match(PATTERN_THIRD_TYPE) do |match_data|
        ary = word.project_letters(match_data.begin(1), match_data.begin(2), match_data.begin(3))
        if @check_set_s.call(ary)
          exchanges = [[match_data.begin(1), match_data.begin(2)],
                       [match_data.begin(3) - 1, match_data.begin(3)],
                       [match_data.begin(4), match_data.begin(4) + 1]]
          letters = match_data[1..3]
          word2 = word.transform(:exchange => exchanges)
          str = "#{word} + #{word.transform(:delete => letters[0])} + #{word.transform(:delete => letters[1])} + #{word.transform(:delete => letters[2])} - #{word2} - #{word2.transform(:delete => letters[0])} - #{word2.transform(:delete => letters[1])} - #{word2.transform(:delete => letters[2])}"
          words << NPW::NanoWord::Relation.new(str, @involution)
        end
      end
      words
    end

    def each_3rd_relations(rank, &block)
      return to_enum(:each_3rd_relations) unless block_given?
      non_zero_nanoword(rank) do |word|
        relations_3rd_from_one_word(word).each do |rel|
          yield([word, rel])
        end
      end
    end

    def files_enum_relations
      ret = {
        "relations_2nd_src.txt" => "each_2nd_relations"
      }
      if @check_set_s
        ret["relations_3rd_src.txt"] = "each_3rd_relations"
      end
      ret
    end

    def parse_generator_string(str)
      NPW::NanoWord.new(*str.split(":"), @involution)
    end

    def parse_relation_string(str)
      NPW::NanoWord::Relation.new(str, @involution)
    end
  end

  class GnOnClosedNanoWord < GnOnNanoWord
    DEFAULT_MESSAGE = "On closed Nonowords"

    def initialize(name, involution, check_set_s, involution_shift, opts = {})
      @involution_shift = involution_shift
      super(name, involution, check_set_s, opts)
    end

    def each_shift_relations(rank, &block)
      return to_enum(:each_shift_relations) unless block_given?
      non_zero_nanoword(rank) do |word|
        rel = parse_relation_string("#{word.to_s} - #{word.transform(:shift => @involution_shift).to_s}")
        yield([word, rel])
      end
    end

    def files_enum_relations
      ret = super
      ret["relations_shift.txt"] = "each_shift_relations"
      ret
    end
  end
end
