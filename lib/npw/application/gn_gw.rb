require "npw/application/polyak_algebra/truncated_polyak_algebra"

module NPW
  class GnOnGaussWord < NPW::TruncatedPolyakAlgebra
    PATTERN_FIRST_TYPE = /([A-Z])\1/
    PATTERN_SECOND_TYPE = /([A-Z])([A-Z]).*\2\1/
    PATTERN_THIRD_TYPE = /([A-Z])([A-Z]).*\1([A-Z]).*(\2\3)/

    # For effective computation of truncated Polyak algebra, we use the following three patterns.
    # xAByABz is isomorphic to xAByBAz
    PATTERN_AB_AB = /([A-Z][A-Z]).*\1/
    # xABCByCAz is isomorphic to xABACyBCz
    PATTERN_ABCB_CA = /([A-Z])([A-Z])([A-Z])\2.*(\3\1)/
    # xAByCBCAz is isomorphic to xAByACBCz
    PATTERN_AB_CBCA =/([A-Z])([A-Z]).*([A-Z])\2(\3\1)/

    # @param [Hash] opts An argument of NPW::SNF
    # @option opts [boolean] :ignore_3rd_relations Ignore third type relations of linearlized Reidemeister moves
    # @option opts [:normal,:extended] :classification_moves Type of moves in classifying words.
    #   :normal uses only moves in definition of Gauss words.
    #   :extended uses similar moves derived from the definition in addition.
    # @option opts [boolean] :effective Use additional relations to compute effectively
    def initialize(name, opts = {})
      @ignore_3rd_relations = opts[:ignore_3rd_relations]
      @move_in_classification = opts[:classification_moves] || :normal
      @effective = opts[:effective]
      super(name, opts.except!(:ignore_3rd_relations))
    end

    def non_zero_gauss_word(ranks, &block)
      return to_enum(:non_zero_gauss_word, ranks) unless block_given?
      ranks = [ranks] unless ranks.respond_to?(:each)
      ranks.each do |rank|
        NPW::Word.each_gauss_word(:rank => rank, :exclude => [PATTERN_FIRST_TYPE], &block)
      end
    end

    def zero_word?(word)
      PATTERN_FIRST_TYPE =~ word.to_s
    end

    def each_generator(rank, &block)
      non_zero_gauss_word(rank, &block)
    end

    def each_word(rank, &block)
      NPW::Word.each_gauss_word(:rank => rank, &block)
    end

    def relations_2nd_from_one_word(word)
      word.each_match(PATTERN_SECOND_TYPE).map do |match_data|
        letters = match_data[1..2]
        NPW::Word::Relation.new("#{word} + 2#{word.transform(:delete => letters[0])}")
      end
    end

    def each_2nd_relations(rank, &block)
      return to_enum(:each_2nd_relations) unless block_given?
      non_zero_gauss_word(rank) do |word|
        relations_2nd_from_one_word(word).each do |rel|
          yield([word, rel])
        end
      end
    end

    def relations_3rd_from_one_word(word)
      word.each_match(PATTERN_THIRD_TYPE).map do |match_data|
        exchanges = [[match_data.begin(1), match_data.begin(2)],
                     [match_data.begin(3) - 1, match_data.begin(3)],
                     [match_data.begin(4), match_data.begin(4) + 1]]
        letters = match_data[1..3]
        word2 = word.transform(:exchange => exchanges)
        str = "#{word} + #{word.transform(:delete => letters[0])} + #{word.transform(:delete => letters[1])} + #{word.transform(:delete => letters[2])} - #{word2} - #{word2.transform(:delete => letters[0])} - #{word2.transform(:delete => letters[1])} - #{word2.transform(:delete => letters[2])}"
        NPW::Word::Relation.new(str)
      end
    end

    def each_3rd_relations(rank, &block)
      return to_enum(:each_3rd_relations) unless block_given?
      non_zero_gauss_word(rank) do |word|
        relations_3rd_from_one_word(word).each do |rel|
          yield([word, rel])
        end
      end
    end

    # If @effective is true then this method is used.
    def each_additional_relations(rank, &block)
      return to_enum(:each_additional_relations) unless block_given?
      non_zero_gauss_word(rank) do |word|
        unless @ignore_3rd_relations
          word.each_match(PATTERN_AB_AB) do |match_data|
            word_other = word.transform(:exchange => [match_data.begin(1), match_data.begin(1) + 1]).adjust_alphabet!
            rel = NPW::Word::Relation.new("#{word.to_s} - #{word_other.to_s}")
            yield([word, rel])
          end
          [PATTERN_ABCB_CA, PATTERN_AB_CBCA].each do |pattern|
            word.each_match(pattern).map do |match_data|
              exchanges = [[match_data.begin(1), match_data.begin(2)],
                           [match_data.begin(3), match_data.begin(3) + 1],
                           [match_data.begin(4), match_data.begin(4) + 1]]
              word_other = word.transform(:exchange => exchanges).adjust_alphabet!
              rel = NPW::Word::Relation.new("#{word.to_s} - #{word_other.to_s}")
              yield([word, rel])
            end
          end
        end
        if PATTERN_SECOND_TYPE =~ word.to_s
          match_data = Regexp.last_match
          if match_data.begin(0) != 0
            letter = match_data[1]
            word_other = word.transform(:delete => letter, :replace => { "A" => "A#{letter}" })
            word_other.transform!(:exchange => [0, 1])
            rel = NPW::Word::Relation.new("#{word.to_s} - #{word_other.to_s}")
            yield([word, rel])
          end
        end
      end
    end

    def files_enum_relations
      ret = {
        "relations_2nd_src.txt" => "each_2nd_relations"
      }
      unless @ignore_3rd_relations
        ret["relations_3rd_src.txt"] = "each_3rd_relations"
      end
      if @effective
        ret["relations_additional.txt"] = "each_additional_relations"
      end
      ret
    end

    def parse_generator_string(str)
      NPW::Word.new(str)
    end

    def parse_relation_string(str)
      NPW::Word::Relation.new(str)
    end

    def irreducible_moves
      unless @irreducible_moves
        if @move_in_classification == :extended
          @irreducible_moves = [:third, :third_reverse, :third_similar1, :third_similar1_reverse,
                                :third_similar2, :third_similar2_reverse, :third_similar3, :third_similar3_reverse]
        else
          @irreducible_moves = [:third, :third_reverse]
        end
      end
      @irreducible_moves
    end

    # @param [Array] tc_src An array of words
    def trace_three_classes(tc_src)
      tc = tc_src.dup
      until tc_src.empty?
        tc_add = []
        tc_src.each do |w|
          w.each_move_match(:move => irreducible_moves).each do |org, type, chars, word_new|
            unless tc.include?(word_new)
              tc_add << word_new
            end
          end
        end
        tc.concat(tc_add)
        tc_src = tc_add
      end
      tc.sort!
    end
    private :trace_three_classes

    def three_class(word)
      raise "Not implemented for no 3rd relation case" if @ignore_3rd_relations
      tc = word.each_move_match(:move => irreducible_moves).map { |org, type, chars, word_new| word_new }
      tc << word.adjust_alphabet
      tc.uniq!
      trace_three_classes(tc)
    end

    def words_by_reduced_move(word)
      if @move_in_classification == :extended
        moves = [:first, :second, :second_similar]
      else
        moves = [:first, :second]
      end
      word.each_move_match(:move => moves).map { |org, type, chars, word_new| word_new }.uniq.sort!
    end

    def modulo_number(rank_n)
      2 ** rank_n
    end

    def default_message
      if @effective
        "On Gauss words (use additional relations)"
      else
        "On Gauss words"
      end
    end
  end

  class GnOnClosedGaussWord < GnOnGaussWord
    def relations_shift_move(word)
      [NPW::Word::Relation.new("#{word.to_s} - #{word.transform(:shift => true).to_s}")]
    end

    def each_shift_relations(rank, &block)
      return to_enum(:each_shift_relations) unless block_given?
      non_zero_gauss_word(rank) do |word|
        rel = relations_shift_move(word)
        yield([word, rel[0]])
      end
    end

    def files_enum_relations
      ret = super
      ret["relations_shift.txt"] = "each_shift_relations"
      ret
    end

    def words_by_shift_move(word)
      words = [word.adjust_alphabet].concat(word.words_by_shift.map { |w| w.adjust_alphabet! })
      words.uniq!
      words
    end
    private :words_by_shift_move

    def three_class(word)
      tc = [word.adjust_alphabet]
      tc_src = tc
      until tc_src.empty?
        tc_shift = []
        tc_add = []
        tc_src.each do |w|
          tc_shift.concat(words_by_shift_move(w))
        end
        tc_shift.uniq!
        tc_add = trace_three_classes(tc_shift)
        tc_src = tc_add - tc
        tc.concat(tc_add).uniq!
      end
      tc.sort!
    end

    def default_message
      if @effective
        "On closed Gauss words (use additional relations)"
      else
        "On closed Gauss words"
      end
    end
  end
end
