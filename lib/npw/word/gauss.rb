require "binary_search/pure"

module NPW
  class Word
    class GWGenerator
      # @option opts [Array] :alphabet An array of characters
      # @option opts [Symbol] :type :string or :word
      # @option opts [Array] :exclude Array of patterns to exclude
      def initialize(rank, opts = {})
        @rank = rank
        @type = opts[:type] || :word
        @exclude = opts[:exclude]
        if @alphabet = opts[:alphabet]
          raise "Not yet implemented"
          unless @alphabet.size == @rank
            raise "Invalid number of letters"
          end
        else
          @alphabet = NPW::Word::DefaultAlphabet.get(@rank)
        end
      end

      def pair_of_numbers(nums)
        first_num = nums[0]
        nums[1..-1].map do |i|
          [first_num, i]
        end
      end
      private :pair_of_numbers

      # Order of generated words are not same as string order of words.
      def generate(&block)
        return to_enum(:generate) unless block_given?
        word_length = @rank * 2
        nums = word_length.times.to_a
        stack = pair_of_numbers(nums).reverse.map { |pair| [pair] }
        until stack.empty?
          n = stack.pop
          n_flatten = n.flatten
          if n_flatten.size == word_length
            string_ary = []
            n.each_with_index do |nums, i|
              nums.each do |j|
                string_ary[j] = @alphabet[i]
              end
            end
            val = string_ary.join
            val = NPW::Word.new(val) if @type == :word
            if !@exclude || @exclude.all? { |reg| reg !~ val.to_s }
              yield(val)
            end
          else
            rest_nums = nums - n_flatten
            seq = pair_of_numbers(rest_nums).reverse.map { |pair| n.dup << pair }
            stack += seq
          end
        end
      end
    end

    # Eval for each isotopy class
    def self.each_gauss_word(opts = {}, &block)
      rank = opts[:rank]
      gen_opts = opts.slice(:type, :alphabet, :exclude)
      GWGenerator.new(rank, gen_opts).generate(&block)
    end

    class GWGenerator2
      def index_pair_of_inset_pos(start_num, word_size)
        inds = []
        (start_num..word_size).to_a.reverse.each do |i|
          ((i + 1)..(word_size + 1)).to_a.reverse.each do |j|
            inds << [i, j]
          end
        end
        inds
      end
      private :index_pair_of_inset_pos

      # @param src_subword [String,Array] String or array of strings
      def words_including_subword(src_subword, letters, &block)
        return to_enum(:words_including_subword, src_subword, add_letter_num) unless block_given?
        letter_num = letters.size
        word_src = src_subword.dup
        ind_stack = index_pair_of_inset_pos(0, word_src.size).map { |ind| [ind] }
        until ind_stack.empty?
          ind = ind_stack.pop
          if ind.size == letter_num
            word = word_src.dup
            ind.each_with_index do |ary, i|
              ary.each { |j| word.insert(j, letters[i]) }
            end
            word = word.join if Array === word
            yield(NPW::Word.new(word))
          else
            index_pair_of_inset_pos(ind[-1][0] + 1, word_src.size + 2 * ind.size).each do |ind_new|
              ind_dup = ind.dup
              ind_dup << ind_new
              ind_stack << ind_dup
            end
          end
        end
      end
    end

    def self.gauss_words_including_subword(*args, &block)
      NPW::Word::GWGenerator2.new.words_including_subword(*args, &block)
    end

    class GaussWordClassify

      # @option opts [Array,Range,Fixnum] :rank Rank of words
      # @option opts [boolean] :load Path of file that saves classified words
      # @option opts [boolean] :shift Use shift move
      # @option opts [String] :file_words Path of file that saves not classified words
      def initialize(opts = {})
        @rank = opts[:rank]
        @rank = [@rank] if Fixnum === @rank
        @load = opts[:load]
        @shift = opts[:shift]
        @file_words = opts[:file_words]
        @word_sets = nil
      end

      # @param [String] out Output file path
      def save(out)
        return nil if File.exist?(out)
        open(out, 'w') do |f|
          words_homotopic_by_one_move.each do |words|
            f.puts words.join(" ")
          end
        end
        true
      end

      def load_file
        if @load
          sets = []
          open(@load, "r") do |f|
            while l = f.gets
              sets << l.split
            end
          end
        end
        sets
      end

      def word_strings_by_one_move(word)
        words = word.words_by_move_once(:move => :all)
        words << word
        if @shift
          words.map! { |w| w.words_by_shift.map!(&:adjust_alphabet) << w }
          words.flatten!.uniq!
        end
        words.map!(&:to_s).sort!
      end
      private :word_strings_by_one_move

      def each_word_by_rank(&block)
        if @rank
          @rank.each do |r|
            NPW::Word.each_gauss_word(:rank => r).each do |word|
              yield(word)
            end
          end
        elsif @file_words
          open(@file_words, "r") do |f|
            while l = f.gets
              l.strip!
              if l[0] != "#"
                yield(NPW::Word.new(l))
              end
            end
          end
        else
          raise "Can not create words; Specify :rank option."
        end
      end
      private :each_word_by_rank

      # Return an array of string arrays
      def words_homotopic_by_one_move
        return @word_sets if @word_sets
        if @word_sets = load_file
          return @word_sets
        end

        word_class_trivial = ["@"]
        word_classes = []
        each_word_by_rank do |word|
          word_string_ary = word_strings_by_one_move(word)
          if word_string_ary.any? { |str| word_class_trivial.binary_index(str) }
            words_subtract = word_string_ary - word_class_trivial
            unless words_subtract.empty?
              words_to_concat = words_subtract.dup
              until words_subtract.empty?
                w = words_subtract.shift
                while n = word_classes.find_index { |set| set.binary_index(w) }
                  deleted = word_classes.delete_at(n)
                  words_to_concat.concat(deleted)
                  words_subtract = words_subtract - deleted
                end
              end
              words_to_concat.sort!.uniq!
              word_class_trivial.concat(words_to_concat).sort!
            end
            next
          end

          match_indices = []
          catch(:found) do
            word_classes.each_with_index do |word_set, i|
              word_string_ary.each_with_index do |w|
                if word_set.binary_index(w)
                  match_indices << i
                  if (word_string_ary - word_set).empty?
                    throw :found
                  else
                    break
                  end
                end
              end
            end
          end
          if match_indices.empty?
            word_classes << word_string_ary.sort!
          else
            if match_indices.size > 1
              match_indices.sort![1..-1].reverse.each do |ind|
                word_classes[match_indices[0]].concat(word_classes.delete_at(ind))
              end
            end
            word_classes[match_indices[0]].concat(word_string_ary).sort!.uniq!
          end
        end
        word_classes.unshift(word_class_trivial) if word_class_trivial.size > 1
        @word_sets = word_classes
      end

      def check_invariant(*invariants)
        words_homotopic_by_one_move.each_with_index do |word_set, i|
          invariants_word_set = nil
          word_set.each do |str|
            w = NPW::Word.new(str)
            vals = {}
            invariants.each_with_index do |inv, n|
              vals[n] = inv.map(w)
            end
            if invariants_word_set
              invariants.each_with_index do |inv, n|
                if invariants_word_set[n] != vals[n]
                  puts "#{w}\t#{vals[n].inspect.strip}"
                  puts "#{invariants_word_set["Word"]}\t#{invariants_word_set[n].inspect.strip}"
                  # raise "Gamma#{n} is not #{n} invariant"
                end
              end
            else
              invariants_word_set = vals
              invariants_word_set["Word"] = w.to_s
            end
          end
          puts "Homotopy class #{i} (#{word_set.size}): #{Time.now.to_s}\n #{invariants_word_set.inspect.strip}"
        end
      end

      # @param [Array] invariants An array of invariants that should be tested ahead of time
      # @param [String] out Output file path
      def exec(invariants, out = nil)
        classification = Hash.new { |k, v| k[v] = [] }
        values = {}
        words_homotopic_by_one_move.each_with_index do |word_set, i|
          word_first = NPW::Word.new(word_set[0])
          v = invariants.map { |inv| inv.map(word_first).to_a }
          key = (v.inject([]) { |ary, val| ary.concat(val.to_a) }).map(&:to_s).join(" ")
          classification[key] << i
          values[i] = v
        end
        if out
          open(out, "w") do |f|
            classification.each do |key, nums|
              n = nums[0]
              f.puts "#{values[n].map { |vec| vec.inspect.strip }.join(" ")}\t#{nums.map(&:to_s).join(" ")}"
            end
          end
        end
        classification
      end
    end
  end
end
