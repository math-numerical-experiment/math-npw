module NPW
  class Word
    module DefaultAlphabet
      LETTERS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                 "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                 "U", "V", "W", "X", "Y", "Z"]

      @cache = {}

      def self.get(num)
        @cache[num] ||= LETTERS[0...num].freeze
      end

      def self.adjust_with_transformation(word)
        word_new, i, h = NPW::Word::DefaultAlphabet.adjust_string_with_transformation(word)
        [word_new, NPW::Word::DefaultAlphabet.get(i), h]
      end

      def self.adjust(word)
        word_new, i = NPW::Word::DefaultAlphabet.adjust_string(word)
        [word_new, NPW::Word::DefaultAlphabet.get(i)]
      end
    end

    include Comparable

    def initialize(word)
      @word = word
      if !@word || @word.length == 0
        @word = NPW::MARK_EMPTY
      elsif @word.include?(NPW::MARK_EMPTY) && @word.length > 1
        raise ArgumentError, "Invalid string expressing a word"
      end
      @adjusted = false
      @cache_empty = check_empty
    end

    def dup
      self.class.new(@word.dup)
    end

    def check_empty
      (NPW::MARK_EMPTY == @word || @word.size == 0)
    end
    private :check_empty

    # We must call this method when @word is changed.
    def clear_cache
      @adjusted = false
      @length = nil
      @alphabet = nil
      @cache_isomorphic = nil
      @cache_empty = check_empty
    end
    private :clear_cache

    def empty?
      @cache_empty
    end

    def length
      unless @length
        @length = 0
        if !empty?
          l = @word.size
          i = 0
          while i < l
            @length += 1 if @word[i] != NPW::MARK_SPLITTER
            i += 1
          end
        end
      end
      @length
    end

    def <=>(other)
      if self.empty?
        if other.empty?
          0
        else
          -1
        end
      elsif other.empty?
        1
      elsif (n = (self.length <=> other.length)) != 0
        n
      else
        w1 = (self.adjusted? ? self : self.adjust_alphabet)
        w2 = (other.adjusted? ? other : other.adjust_alphabet)
        (n = w1.to_s <=> w2.to_s) != 0 ? n : self.to_s <=> other.to_s
      end
    end

    def gauss?
      # Return invalid value for empty and splitter
      count = Hash.new { |h, k| h[k] = 0 }
      @word.each_char do |c|
        count[c] += 1
      end
      count.all? { |k, v| v == 2 }
    end

    alias_method :gauss_word?, :gauss?

    def alphabet
      unless @alphabet
        @alphabet = @word.each_char.to_a.sort.uniq
        @alphabet.delete(MARK_EMPTY)
        @alphabet.delete(MARK_SPLITTER)
      end
      @alphabet
    end

    def rank
      alphabet.size
    end

    def adjusted?
      @adjusted
    end

    def set_adjusted
      @adjusted = true
    end
    protected :set_adjusted

    def adjusted_word
      if empty?
        [@word.dup, []]
      else
        NPW::Word::DefaultAlphabet.adjust(@word)
      end
    end
    private :adjusted_word

    def adjust_alphabet
      if @adjusted
        self.class.new(@word.dup)
      else
        w, alphabet = adjusted_word
        word_new = self.class.new(w)
        word_new.set_adjusted
        word_new
      end
    end

    # Use letters from "A"
    def adjust_alphabet!
      unless @adjusted
        @word, @alphabet = adjusted_word
        set_adjusted
      end
      self
    end

    def to_s
      @word.size > 0 ? @word : (@word = NPW::MARK_EMPTY)
    end
    alias_method :to_str, :to_s

    def colored_str(color, chars_to_color)
      w = to_s.dup
      (chars_to_color.sort { |a1, a2| a2[0] <=> a1[0] }).each do |chars|
        w[*chars] = Rainbow(w[*chars]).foreground(color)
      end
      w
    end

    alias_method :colored_string, :colored_str

    # Not support pattern having nested parentheses.
    def colored_string_with_match(color, pattern)
      if pattern =~ to_s
        m = Regexp.last_match
        colored_str(color, ((1...(m.size)).each.map { |i| [m.begin(i), m.end(i) - m.begin(i)] }).reverse)
      else
        false
      end
    end

    # Use to check isomorphic relation to other word
    def cache_isomorphic
      unless @cache_isomorphic
        cache = []
        @word.each_char.with_index do |c, i|
          if ary = cache.assoc(c)
            ary << i
          else
            cache << [c, i]
          end
        end
        # @cache_isomorphic = cache.map { |ary| ary[1..-1] }
        @cache_isomorphic = cache.inject([]) { |ary, val| ary.concat(val[1..-1]) << -1 }.pack("s*")
      end
      @cache_isomorphic
    end
    protected :cache_isomorphic

    def ==(other)
      to_s == other.to_s
    end

    def eql?(other)
      self == other
    end

    def hash
      self.to_s.hash
    end

    def hash_under_isomorphism
      cache_isomorphic.hash
    end

    def isomorphic?(other)
      cache_isomorphic == other.cache_isomorphic
    end

    def size
      (empty? ? 0 : @word.size)
    end

    # @note This method destroy words_ary
    def equivalence_class_isomorphic_words(words_ary)
      ary = []
      until words_ary.empty?
        equivalence_class = [words_ary.shift]
        i = 0
        while i < words_ary.size
          if equivalence_class[-1].isomorphic?(words_ary[i])
            equivalence_class << words_ary.delete_at(i)
          else
            i += 1
          end
        end
        ary << equivalence_class
      end
      ary
    end
    private :equivalence_class_isomorphic_words

    def delete_isomorphic_words(words_ary)
      equivalence_class_isomorphic_words(words_ary).map do |ary|
        ary.first
      end
    end
    private :delete_isomorphic_words

    def subwords_by_deleting_one_letter
      if alphabet.size > 1
        words_new = alphabet.map do |c|
          self.class.new(@word.gsub(c, ""))
        end
        delete_isomorphic_words(words_new)
      else
        []
      end
    end
    protected :subwords_by_deleting_one_letter

    # Return subwords modulo isomorphism: we get @, AABB and AA for AABB.
    def subwords_mod_isomorphism
      words = Hash.new { |h, k| h[k] = [] }
      words[0] << self.class.new(NPW::MARK_EMPTY)
      words[length] << Marshal.load(Marshal.dump(self))
      subwords_ary = subwords_by_deleting_one_letter
      until subwords_ary.empty?
        subwords_ary.each do |sword|
          words[sword.length] << sword
        end
        subwords_ary.map! { |w| w.subwords_by_deleting_one_letter }
        subwords_ary = delete_isomorphic_words(subwords_ary.flatten)
      end
      swords = []
      words.to_a.sort_by { |a| a[0] }.each do |len, sw|
        swords += delete_isomorphic_words(sw).map(&:adjust_alphabet!).sort
      end
      swords
    end
    private :subwords_mod_isomorphism

    def subwords_with_duplications_cache
      unless @subwords_with_duplications_cache
        @subwords_with_duplications_cache = [NPW::MARK_EMPTY, @word.dup]
        (1...alphabet.size).each do |i|
          alphabet.combination(i).each do |letters|
            @subwords_with_duplications_cache << @word.gsub(/[#{letters.join}]/, "")
          end
        end
        @subwords_with_duplications_cache.map! do |s|
          self.class.new(s)
        end
        @subwords_with_duplications_cache.sort!
      end
      @subwords_with_duplications_cache
    end
    private :subwords_with_duplications_cache

    # Return subwords with duplications: we get AABB, AA and BB for AABB.
    def subwords_with_duplications
      subwords_with_duplications_cache.dup
    end
    private :subwords_with_duplications

    # @option opts [boolean] :modulo Return subwords modulo isomorphism if the value is true
    # @option opts [Integer,Array,Range] :rank Return subwords of specified rank
    # @option opts [boolean] :clear_cache Clear cache of subwords
    def subwords(opts = {})
      if opts[:clear_cache]
        @subwords_with_duplications_cache = nil
      end
      if opts[:modulo]
        words = subwords_mod_isomorphism
      else
        words = subwords_with_duplications
      end
      if rank_cond = opts[:rank]
        rank_cond = [rank_cond] if Fixnum === rank_cond
        words.delete_if do |w|
          !rank_cond.include?(w.rank)
        end
      end
      words
    end

    def word_by_deleting(w, letters)
      word_new = w
      letters.each do |c|
        word_new = word_new.gsub(c, "")
      end
      word_new
    end
    private :word_by_deleting

    def transform_word(opts)
      w = @word
      if exchange_nums = opts[:exchange]
        exchange_nums = [exchange_nums] if Integer === exchange_nums[0]
        exchange_nums.each do |nums|
          word_new = w.dup
          nums.size.times do |i|
            word_new[nums[(i + 1) % nums.size]] = w[nums[i]]
          end
          w = word_new
        end
      end
      if letters = opts[:delete]
        letters = letters.each_char.to_a if String === letters
        w = word_by_deleting(w, letters)
      end
      if letters_to_replace = opts[:replace]
        word_new = ''
        w.each_char do |c|
          word_new << (letters_to_replace[c] || c)
        end
        w = word_new
      end
      if opts[:shift]
        w = "#{w[1..-1]}#{w[0]}"
      end
      w
    end
    private :transform_word

    # @note Order of transformation is :exchange, :delete, :replace, and :shift
    # 
    # @option opts [Array] :exchange An array of numbers of positions meaning letters to be exchanged
    # @option opts [Array,String] :delete Letters to be deleted
    # @option opts [Hash] :replace Pairs of letters to be replaced
    # @option opts [boolean] :shift Move first letter to last
    # 
    # @example Exchange letters: E -> C, B -> E, C -> B
    #  NPW::Word.new("ABCDEF").transform(:exchange => [2, 4, 1]) # => NPW::Word.new("AEBDCF")
    def transform(opts = {})
      self.class.new(transform_word(opts))
    end

    def transform!(opts = {})
      @word = transform_word(opts)
      clear_cache
      self
    end

    def words_by_shift
      words = []
      if @word.size > 1
        (@word.size - 1).times do |i|
          words << (words[-1] || self).transform(:shift => true)
        end
      end
      words
    end

    def angle_bracket_with(other)
      count = 0
      grouping = Hash.new { |h, k| h[k] = [] }
      other.subwords(:modulo => false).each do |sw|
        grouping[sw.length] << sw
      end
      grouping.each do |len, words_ary|
        equivalence_class_isomorphic_words(words_ary).each do |ec|
          if self.isomorphic?(ec[0])
            count += ec.size
          end
        end
      end
      count
    end

    def each_match(pattern, &block)
      return to_enum(:each_match, pattern) unless block_given?
      pos = 0
      while pattern.match(self.to_s, pos)
        match_data = Regexp.last_match
        pos = match_data.end(1)
        yield(match_data)
      end
    end

    def self.angle_bracket(w1, w2)
      w1.angle_bracket_with(w2)
    end
  end
end

# We must require npw_word.so after the definition of NPW::Word::DefaultAlphabet::LETTERS
require_relative "../../../ext/word/npw_word.so"
