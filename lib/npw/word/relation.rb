module NPW
  class Word
    class Relation < NPW::Relation
      def self.parser
        @parser ||= NPW::Parser::GWRelationParser.new
      end

      def term_create(arg)
        NPW::Word.new(arg)
      end
    end
  end
end
