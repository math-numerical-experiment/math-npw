# NPW (NanoPhrases and nanoWords)

Programs to calculated nanowords and truncated Polyack algebra:
Ruby scripts with C++ extended libraries to deal with nanowords
and commands of C to calculate Smith normal forms of sparse matrices.

Note that nanophrases are not yet implemented.

## Installation

NPW uses C libraries (glib and kyotocabinet) and also uses programs "gap" or "sparse_snf" to get Smith normal form of matrices.

    git clone --recursive https://git.gitorious.org/math-numerical-experiment/math-npw.git
    cd math-npw

To compile extended libraries and some commands, some libraries are rquired.
We execute the following command on Ubuntu 13.04;

    apt-get install gap kyotocabinet-utils libkyotocabinet-dev libglib2.0-dev

To setup required gems, we also execute the following;

    bundle update

To execute programs, we use bundle command like

    bundle exec ruby scripts/tpa.rb --help

## Usage

Typical usages are shown in the following.

### List of prepared word types

Some types of nanowords are prepared.
We get the list by the command

    bundle exec ruby scripts/tpa.rb target-list

### Determine truncated Polyack algebra and finite type invariant

To get group structure of truncated Polyack algebra of usual Gauss word of rank 5, we execute

    bundle exec ruby scripts/tpa.rb group-structure GaussOpen1 5 -o OutGaussOpen1_5

The directory OutGaussOpen1 includes generators, relations, the matrix defined by them, and results.
In addition, to get finite type invariant, we execute

    bundle exec ruby scripts/tpa.rb transformation GaussOpen1 5 OutGaussOpen1_5

The file table.tsv including data of invariant is added in the directory OutGaussOpen1.

### Classify nanowords by finite type invariant

First, we create a database of sets of nanowords that are transformed to each other by homotopy moves.
The algorithm to create the database is heuristic and there is a possibility that
two nanowords in a homotopy class are classified to two different sets in the database.

    bundle exec ruby scripts/tpa.rb classify-create-db GaussOpen1 5 classifyGaussOpen1_5 --progress 100

If universal finite type invariant that is constructed numerically by the data in a file table.tsv
confirms differences of sets of nanowords in the database,
we get the classification of the nanowords.
To try to do so, we execute the following command;

    bundle exec ruby scripts/tpa.rb invariant-classify GaussOpen1 OutGaussOpen1_5/table.tsv classifyGaussOpen1_5

Regretfully, the case of the above commands do not give the complete classification of Gauss words of rank 5.
To get the complete classification, we need to use universal finite type invariant of rank 7.
We must execute the following commands of which computation on the author's computer takes about a day.

    bundle exec ruby scripts/tpa.rb group-structure GaussOpen1 7 -o OutGaussOpen1_7
    bundle exec ruby scripts/tpa.rb transformation GaussOpen1 7 OutGaussOpen1_7
    bundle exec ruby scripts/tpa.rb classify-create-db GaussOpen1 5 classifyGaussOpen1_5 --progress 100
    bundle exec ruby scripts/tpa.rb invariant-classify GaussOpen1 OutGaussOpen1_7/table.tsv classifyGaussOpen1_5

### irb with NPW

Bash script to execute irb with load of npw is prepared;

    bundle exec scripts/irb_with_npw

### Interactively calculate equivalent words

First, we execute

    bundle exec ruby scripts/gauss_word_transform.rb

If we input a Gauss word to the running program,
the program shows transformations of the word and a set of words equivalent to the input word.
It searches parts of moves and apply the corresponding moves.
It does not transformations that make words longer.

If we want to use moves created by some combinations of basic moves, i.e., ABAB -> empty and so on,
we add the option --use-extended-moves.

    bundle exec ruby scripts/gauss_word_transform.rb --use-extended-moves

Note that it is not proven that the algorithm of this program returns the equivalent class of a specified word.
