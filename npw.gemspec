# -*- encoding: utf-8 -*-
require File.expand_path('../lib/npw/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Takayuki YAMAGUCHI"]
  gem.email         = ["d@ytak.info"]
  gem.description   = "Nanophrase and nanoword"
  gem.summary       = "Some libraries to calculate nanophrase and nanoword"
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "npw"
  gem.require_paths = ["lib"]
  gem.version       = NPW::VERSION

  gem.add_development_dependency "rspec"
  gem.add_development_dependency "yard"
  gem.add_development_dependency "treetop"
  gem.add_development_dependency "extconf-task"
  gem.add_runtime_dependency "activesupport"
  gem.add_runtime_dependency "filename"
  gem.add_runtime_dependency "binary_search"
  gem.add_runtime_dependency "rainbow"
  gem.add_runtime_dependency "rb-readline"
  gem.add_runtime_dependency "kyotocabinet-ruby"
  gem.add_runtime_dependency "subcommand-optparse"
end
