require "spec_helper"

describe NPW::Word::DefaultAlphabet do
  it "should get 5 letters" do
    expect(described_class.get(5)).to eq(["A", "B", "C", "D", "E"])
  end
end

describe NPW::Word do
  context "when checking Gauss word" do
    ["ABAB", "ACBCAB"].each do |w|
      it "should return true" do
        expect(described_class.new(w).gauss?).to be_truthy
      end
    end

    ["ABAAB", "ACBCABB"].each do |w|
      it "should not return true" do
        expect(described_class.new(w).gauss?).not_to be_truthy
      end
    end
  end

  context "when getting alphabet" do
    it "should get an array of alphabet" do
      expect(described_class.new("CDECCAA").alphabet).to eq(["A", "C", "D", "E"])
    end
  end

  context "when check equality" do
    it "should return true" do
      expect(described_class.new("ABC")).to eq(described_class.new("ABC"))
    end

    it "should return false" do
      expect(described_class.new("ABC")).not_to eq(described_class.new("BCD"))
    end

    it "should return true" do
      expect(described_class.new("ABC").isomorphic?(described_class.new("BCD"))).to be_truthy
    end

    it "should return true" do
      expect(described_class.new("ABAB").isomorphic?(described_class.new("BCBC"))).to be_truthy
    end

    it "should return true" do
      expect(described_class.new("ABC").isomorphic?(described_class.new("ABB"))).not_to be_truthy
    end
  end

  context "when getting subwords" do
    it "should get all subwords with duplications" do
      swords = ["@", "AA", "BB", "CC", "AACC", "ABAB", "BCBC", "ABACBC"].map { |s| described_class.new(s) }
      expect(described_class.new("ABACBC").subwords).to eq(swords)
    end

    it "should get all subwords modulo isomorphism" do
      subwords = ["@", "ABACBC", "BCBC", "AACC", "CC"].map { |w| described_class.new(w) }
      described_class.new("ABACBC").subwords(:modulo => true).each do |w|
        expect(subwords.any? { |w2| w2.isomorphic?(w) }).to be_truthy
      end
    end
  end

  context "when adjusting alphabet" do
    it "should change letters" do
      w = described_class.new("CBA")
      expect(w.adjust_alphabet!.to_s).to eq("ABC")
    end

    it "should create new word" do
      w = described_class.new("bcbaca")
      w2 = w.adjust_alphabet
      expect(w2).not_to eq(w)
      expect(w2.isomorphic?(w)).to be_truthy
      expect(w.to_s).to eq("bcbaca")
    end
  end

  context "when comparing" do
    it "should compare by length" do
      expect(described_class.new("ABCAB")).to be > described_class.new("ABC")
    end

    it "should compare by order of adjusted letters" do
      expect(described_class.new("ABCCAB")).to be > described_class.new("AABCCB")
    end
  end

  context "when transforming words" do
    it "should exchange letters" do
      expect(described_class.new("ABCDEF").transform(:exchange => [2, 4, 1]).to_s).to eq("AEBDCF")
    end

    it "should exchange letters" do
      expect(described_class.new("ABCDEF").transform(:exchange => [[0, 1], [3, 4]]).to_s).to eq("BACEDF")
    end

    it "should delete a letter" do
      expect(described_class.new("ABCAB").transform(:delete => ["A"]).to_s).to eq("BCB")
    end

    it "should delete letters with an array argument" do
      expect(described_class.new("ABDCDAB").transform(:delete => ["B", "C"]).to_s).to eq("ADDA")
    end

    it "should delete letters with a string argument" do
      expect(described_class.new("ABDCDAB").transform(:delete => "BC").to_s).to eq("ADDA")
    end

    it "should replace a letter" do
      expect(described_class.new("ABDCDAB").transform(:replace => { "B" => "E" }).to_s).to eq("AEDCDAE")
    end

    it "should replace two letters" do
      expect(described_class.new("ABDCDAB").transform(:replace => { "A" => "a", "D" => "d" }).to_s).to eq("aBdCdaB")
    end

    it "should exchange, delete and replace letters" do
      expect(described_class.new("ABDCDAB").transform(:exchange => [3, 5], :delete => "BD", :replace => { "A" => "a", "D" => "d" }).to_s).to eq("aaC")
    end

    it "should shift" do
      expect(described_class.new("ABDCDAB").transform(:shift => true).to_s).to eq("BDCDABA")
    end

    it "should shift itself" do
      w = described_class.new("ABDCDAB")
      w.transform!(:shift => true)
      expect(w.to_s).to eq("BDCDABA")
    end

    it "should return words by shift" do
      w = described_class.new("ABDCDAB")
      words = w.words_by_shift
      expect(words.size).to eq(w.to_s.size - 1)
      words.map! { |word| word.adjust_alphabet! }
      expect(w.transform(:shift => true).adjust_alphabet).to eq(words[0])
      (1...(words.size)).each do |i|
        expect(words[i - 1].transform(:shift => true).adjust_alphabet).to eq(words[i])
      end
    end
  end

  context "when calculating angle bracket" do
    it "should return 1 for empty word" do
      expect(described_class.angle_bracket(described_class.new("@"), described_class.new("ABCBAC"))).to eq(1)
    end

    it "should return 1" do
      expect(described_class.angle_bracket(described_class.new("ABBA"), described_class.new("ABCBAC"))).to eq(1)
    end

    it "should return 2" do
      expect(described_class.angle_bracket(described_class.new("ACAC"), described_class.new("ABCBAC"))).to eq(2)
    end

    it "should return 1" do
      w1 = NPW::Word.new("ABACDCBD")
      w2 = NPW::Word.new("DBADCAECBE").transform(:delete => "A")
      expect(w1.isomorphic?(w2)).to be_truthy
      expect(NPW::Word.angle_bracket(w1, w2)).to eq(1)
    end

    it "should return 0 for words with smaller rank" do
      w1 = NPW::Word.new("ABACDCBD")
      NPW::Word.each_gauss_word(:rank => 3) do |word|
        expect(NPW::Word.angle_bracket(w1, word)).to eq(0)
      end
    end

    it "should return 1 for isomorphic words with same rank" do
      w1 = NPW::Word.new("ABCACB")
      NPW::Word.each_gauss_word(:rank => 3) do |word|
        expect(NPW::Word.angle_bracket(w1, word)).to eq(word.isomorphic?(w1) ? 1 : 0)
      end
    end
  end

  context "when deleting duplicated words" do
    it "should not delete by Array#uniq!" do
      ary = [described_class.new("ABAB"), described_class.new("CDCD")]
      ary.uniq!
      expect(ary.size).to eq(2)
    end

    it "should delete by Array#uniq!" do
      ary = [described_class.new("ABAB"), described_class.new("ABAB")]
      ary.uniq!
      expect(ary.size).to eq(1)
    end

    it "should delete by NPW::Word#hash_under_isomorphism Array#uniq!" do
      ary = [described_class.new("ABAB"), described_class.new("CDCD")]
      ary.uniq! { |w| w.hash_under_isomorphism }
      expect(ary.size).to eq(1)
    end
  end

  context "when duplicating" do
    it "should return other object" do
      word = described_class.new("ABAB")
      duplicated = word.dup
      expect(word.to_s).to eq(duplicated.to_s)
      expect(word.object_id).not_to eq(duplicated.object_id)
      expect(word.to_s.object_id).not_to eq(duplicated.to_s.object_id)
    end
  end
end
