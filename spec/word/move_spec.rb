require "spec_helper"

describe NPW::Word::Move do
  context "when creating words by homotopy move once" do
    it "should create by second move" do
      word = NPW::Word.new("DABCDBAC")
      expect(word.words_by_move_once(:move => :all)).to eq([NPW::Word.new("ABAB"), NPW::Word.new("ADBDCBCA").adjust_alphabet])
    end
  end
end
