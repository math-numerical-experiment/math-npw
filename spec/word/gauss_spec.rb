require "spec_helper"

describe NPW::Word::GWGenerator do
  it "should get all Gauss words of rank 2" do
    expect(NPW::Word.each_gauss_word(:rank => 2, :type => :string).to_a).to eq(["AABB", "ABAB", "ABBA"])
  end

  it "should get all Gauss words of rank 3" do
    expect(NPW::Word.each_gauss_word(:rank => 3, :type => :string).to_a).to eq(["AABBCC", "AABCBC", "AABCCB", "ABABCC", "ABACBC", "ABACCB", "ABBACC", "ABCABC", "ABCACB", "ABBCAC", "ABCBAC", "ABCCAB", "ABBCCA", "ABCBCA", "ABCCBA"])
  end

  it "should get all Gauss words of rank 4" do
    n = (8 * 7 * 6 * 5 * 4 * 3 * 2) / (2 * 2 * 2 * 2 * 4 * 3 * 2)
    expect(NPW::Word.each_gauss_word(:rank => 4).to_a.size).to eq(n)
  end
end

describe NPW::Word::GWGenerator2 do
  it "should generate words that can be applied H3 move" do
    count = 0
    NPW::Word.gauss_words_including_subword(["AB", "AC", "BC"], ["D"]) do |word|
      count += 1
      expect(word.rank).to eq(4)
      expect(word.to_s).to match(/AB.*AC.*BC/)
    end
    expect(count).to eq(10)
  end

  it "should generate words from string" do
    count = 0
    NPW::Word.gauss_words_including_subword("ABAB", ["C"]) do |word|
      count += 1
      expect(word.rank).to eq(3)
      expect(word.to_s).to match("A.*B.*A.*B.*")
    end
    expect(count).to eq(15)
  end
end

describe NPW::Word::GaussWordClassify do
  it "should return words homotopic to empty" do
    classify = described_class.new(:rank => 2..3)
    word_sets = classify.words_homotopic_by_one_move
    expect(word_sets.size).to eq(1)
    word_sets[0].any? { |word| word.to_s == "AA" }
  end

  it "should return words of rank 2 or less" do
    classify = described_class.new(:rank => 2)
    word_sets = classify.words_homotopic_by_one_move
    expect(word_sets.size).to eq(1)
    expect(word_sets[0]).to eq(["@", "AA", "AABB", "ABAB", "ABBA"].map { |w| NPW::Word.new(w) })
  end
end
