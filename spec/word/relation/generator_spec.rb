require "spec_helper"
require "npw/application"

shared_examples_for "common_tests" do
  it "should get nil for the empty word" do
    @gen.open_read do |db|
      expect(@gen.find_generator_index(db, "@")).to be_nil
    end
  end

  it "should get nil for the empty word" do
    @gen.open_read do |db|
      num = @gen.find_generator_index(db, "ABACBC")
      expect(num).to be_an_instance_of Fixnum
      expect(num).to be >= 0
    end
  end

  it "should yield each word" do
    count = 0
    @gen.each do |word|
      count += 1
      expect(word).to be_an_instance_of NPW::Word
    end
    expect(count).to be > 0
  end
end

describe NPW::Word::Relation::GeneratorInText do
  before(:all) do
    @dir = FileName.create("inText.tmp", :directory => :self)
    @path = FileName.create("#{@dir}/out.txt")
    gn_gw = NPW::GnOnGaussWord.new("tmp")
    @gen = described_class.new(@path, false, gn_gw.method(:each_generator), 2..4, gn_gw.method(:parse_generator_string))
  end

  it_should_behave_like "common_tests"

  after(:all) do
    FileUtils.rm_r(File.dirname(@path))
  end
end

describe NPW::Word::Relation::GeneratorInDB do
  before(:all) do
    @dir = FileName.create("inDB.tmp", :directory => :self)
    @path = FileName.create("#{@dir}/out.kch")
    gn_gw = NPW::GnOnGaussWord.new("tmp")
    @gen = described_class.new(@path, false, gn_gw.method(:each_generator), 2..4, gn_gw.method(:parse_generator_string))
  end

  it_should_behave_like "common_tests"

  after(:all) do
    FileUtils.rm_r(File.dirname(@path))
  end
end
