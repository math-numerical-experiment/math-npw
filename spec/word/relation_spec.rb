require "spec_helper"

describe NPW::Word::Relation do
  context "when converting string" do
    it "should convert to string" do
      rel = described_class.new("-ABC")
      expect(rel.to_s).to eq("-ABC")
    end

    it "should get convert to string" do
      rel = described_class.new("3AB - 9BC")
      expect(rel.to_s).to match(/3AB\s*-9BC/)
    end
  end

  context "when simplifying terms" do
    it "should unite two terms" do
      rel = described_class.new("3AB - 9BC")
      rel.simplify!
      expect(rel.to_s).to eq("6AB")
    end

    it "should sort terms" do
      rel = described_class.new("3AC - 9ABC")
      rel.simplify!
      expect(rel.to_s).to eq("9ABC -3AC")
    end
  end

  context "when checking equality" do
    it "should return empty relation" do
      expect(described_class.new("@").empty?).to be_truthy
    end

    it "should raise error for empty string" do
      expect do
        described_class.new("")
      end.to raise_error
    end

    it "should return empty relation" do
      expect(described_class.new("3AB - 3BC", :simplify => true).empty?).to be_truthy
    end

    it "should return true" do
      rel1 = described_class.new("3AC - 9ABC + 2EF", :simplify => true)
      rel2 = described_class.new("- 9ABC + 1BD + 4EF", :simplify => true)
      expect(rel1).to eq(rel2)
    end
  end

  context "when eliminating a term" do
    it "should substitute" do
      rel1 = described_class.new("ABCBAC +ABCACB -ABCABC +ABACBC")
      rel2 = described_class.new("ABACBC +ABAB")
      expect(rel1.eliminate!(3, rel2)).to eq(described_class.new("ABCBAC +ABCACB -ABCABC -ABAB"))
    end

    it "should eliminate without multiplying" do
      rel1 = described_class.new("9ABC + 2AB", :simplify => true)
      rel2 = described_class.new("3ABC + 4ABCD", :simplify => true)
      expect(rel1.eliminate!(0, rel2)).to eq(described_class.new("12ABCD - 2AB"))
    end

    it "should eliminate with multiplying" do
      rel1 = described_class.new("6ABC + 2AB", :simplify => true)
      rel2 = described_class.new("4ABC + 4ABCD", :simplify => true)
      expect(rel1.eliminate!(0, rel2)).to eq(described_class.new("12ABCD - 4AB"))
    end
  end

  context "when modifying terms" do
    it "should modify all terms" do
      rel = described_class.new("2A + 3BC")
      rel.modify! do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("6A + 9BC"))
    end

    it "should modify terms specified by range" do
      rel = described_class.new("2A + 3BC + 4CDD + 5CCDD")
      rel.modify!(:range => 1..2) do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("2A + 9BC + 12CDD + 5CCDD"))
    end

    it "should modify terms specified by a number" do
      rel = described_class.new("2A + 3BC + 4CDD + 5CCDD")
      rel.modify!(:range => 1) do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("2A + 9BC + 4CDD + 5CCDD"))
    end

    it "should modify terms specified by an array" do
      rel = described_class.new("2A + 3BC + 4CDD + 5CCDD")
      rel.modify!(:range => [1, 3]) do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("2A + 9BC + 4CDD + 15CCDD"))
    end

    it "should delete terms" do
      rel = described_class.new("2AB + 3BC")
      rel.modify! do |trm|
        trm.delete
      end
      expect(rel.empty?).to be_truthy
    end

    it "should replace a term" do
      rel = described_class.new("2ABC + 3BC")
      rel.modify!(:range => 1, :simplify => true) do |trm|
        trm.replace(NPW::Word.new("ABC"))
      end
      expect(rel).to eq(described_class.new("5ABC"))
    end

    it "should replace terms" do
      rel = described_class.new("1ABC + 4BC - 3AA")
      rel.modify!(:simplify => { :adjust => true }) do |trm|
        trm.replace(NPW::Word.new("BA"))
      end
      expect(rel).to eq(described_class.new("2AB"))
    end
  end

  context "when deleting duplicated relations" do
    it "should delete by Array#uniq!" do
      ary = [described_class.new("2AB + 3ACD"), described_class.new("2BC + 3CAD")]
      ary.each { |rel| rel.simplify! }
      expect(ary.uniq.size).to eq(1)
    end
  end
end
