require "spec_helper"

describe NPW::Parser::NWRelationParser do
  subject do
    NPW::Parser::NWRelationParser.new
  end

  it "should parse empty nanoword" do
    expect(subject.parse("@").value).to eq([[["@", nil], 1]])
  end

  it "should parse relation of one term without sign and number" do
    expect(subject.parse("ABAB:ab").value).to eq([[["ABAB", "ab"], 1]])
  end

  it "should parse relation of one term with plus sign" do
    expect(subject.parse("+AB:aa").value).to eq([[["AB", "aa"], 1]])
  end

  it "should parse relation of one term with minus sign" do
    expect(subject.parse("-AB:ba").value).to eq([[["AB", "ba"], -1]])
  end

  it "should parse relation of one term with plus sign and number" do
    expect(subject.parse("+ 9 ABB:ab").value).to eq([[["ABB", "ab"], 9]])
  end

  it "should parse relation of one term with minus sign and number" do
    expect(subject.parse("- 7 ABB:aa").value).to eq([[["ABB", "aa"], -7]])
  end

  it "should parse relation of one term with sign and number" do
    expect(subject.parse("-6CAB:abc").value).to eq([[["CAB", "abc"], -6]])
  end

  it "should parse relation of two terms." do
    expect(subject.parse("8AB:aa - 9BB:bb").value).to eq([[["AB", "aa"], 8], [["BB", "bb"], -9]])
  end

  it "should parse relation of three terms." do
    expect(subject.parse("8AB:aa +9AB:ab - 9BB:bb").value).to eq([[["AB", "aa"], 8], [["AB", "ab"], 9], [["BB", "bb"], -9]])
  end

  # it "should not raise error for long expression" do
  #   lambda do
  #     subject.parse("-1ABCBDACD+1ABCBDADC-1ABACBDCEDE+1ABACBDECDE-1ABACBDEDCE+1ABACDBCEDE-1ABACDBDECE+1ABACDCBEDE-1ABACDCEBDE+1ABACDCEBED-1ABACDECBDE+1ABACDCEDBE-1ABACDECEBD+1ABCADBECDE-1ABCADBECED+1ABCADBDECE-1ABCADBEDCE+1ABCADBEDEC-1ABCACDBEDE+1ABCADEBDCE-1ABCADCEBED+1ABCADEDBEC-1ABCACDEDBE+1ABCADCDEBE-1ABCADECDBE+1ABCADCEDEB-1ABCADEDCEB+1ABCDAEBDEC-1ABCDACEBDE+1ABCDAEDBEC-1ABCDACEDBE+1ABCBDAECED-1ABCBDADECE+1ABCBDCEDAE-1ABCBDECEAD+1ABCDBDECAE-1ABCDBEDEAC+1ABCDEDBEAC").value
  #   end.should_not raise_error
  # end
end
