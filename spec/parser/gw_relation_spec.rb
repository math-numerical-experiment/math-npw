require "spec_helper"

describe NPW::Parser::GWRelationParser do
  subject do
    NPW::Parser::GWRelationParser.new
  end

  it "should parse relation of one term without sign and number" do
    expect(subject.parse("ABAB").value).to eq([["ABAB", 1]])
  end

  it "should parse relation of one term with plus sign" do
    expect(subject.parse("+AB").value).to eq([["AB", 1]])
  end

  it "should parse relation of one term with minus sign" do
    expect(subject.parse("-AB").value).to eq([["AB", -1]])
  end

  it "should parse relation of one term with plus sign and number" do
    expect(subject.parse("+ 9 ABB").value).to eq([["ABB", 9]])
  end

  it "should parse relation of one term with minus sign and number" do
    expect(subject.parse("- 7 ABB").value).to eq([["ABB", -7]])
  end

  it "should parse relation of one term with sign and number" do
    expect(subject.parse("-6CAB").value).to eq([["CAB", -6]])
  end

  it "should parse relation of two terms." do
    expect(subject.parse("8AB - 9BB").value).to eq([["AB", 8], ["BB", -9]])
  end

  it "should parse relation of three terms." do
    expect(subject.parse("8AB +9AB - 9BB").value).to eq([["AB", 8], ["AB", 9], ["BB", -9]])
  end

  it "should not raise error for long expression" do
    expect do
      subject.parse("-1ABCBDACD+1ABCBDADC-1ABACBDCEDE+1ABACBDECDE-1ABACBDEDCE+1ABACDBCEDE-1ABACDBDECE+1ABACDCBEDE-1ABACDCEBDE+1ABACDCEBED-1ABACDECBDE+1ABACDCEDBE-1ABACDECEBD+1ABCADBECDE-1ABCADBECED+1ABCADBDECE-1ABCADBEDCE+1ABCADBEDEC-1ABCACDBEDE+1ABCADEBDCE-1ABCADCEBED+1ABCADEDBEC-1ABCACDEDBE+1ABCADCDEBE-1ABCADECDBE+1ABCADCEDEB-1ABCADEDCEB+1ABCDAEBDEC-1ABCDACEBDE+1ABCDAEDBEC-1ABCDACEDBE+1ABCBDAECED-1ABCBDADECE+1ABCBDCEDAE-1ABCBDECEAD+1ABCDBDECAE-1ABCDBEDEAC+1ABCDEDBEAC").value
    end.not_to raise_error
  end
end
