require "spec_helper"

describe NPW::NanoWord do
  before(:all) do
    @involution = { "a" => "b", "b" => "a" }
    @involution2 = { "a" => "b", "b" => "a", "c" => "d", "d" => "c"}
    @involution3 = { "a" => "c", "b" => "d", "c" => "a", "d" => "b"}
  end

  context "when creating nanowords" do
    it "should raise an error" do
      expect do
        NPW::NanoWord.new("ABACBC", "ab", @involution2)
      end.to raise_error
    end

    it "should raise an error" do
      expect do
        NPW::NanoWord.new("ABACBC", "abcd", @involution2)
      end.to raise_error
    end
  end

  context "when executing delegated method" do
    it "should not return true" do
      nanoword = NPW::NanoWord.new("ABAB", "ab", @involution)
      expect(nanoword.empty?).not_to be_truthy
    end

    it "should return true" do
      nanoword = NPW::NanoWord.new("", "", @involution)
      expect(nanoword.empty?).to be_truthy
    end

    it "should return true" do
      nanoword = NPW::NanoWord.new("@", "", @involution)
      expect(nanoword.empty?).to be_truthy
    end
  end

  context "when executing methods" do
    it "should return a string" do
      nanoword = NPW::NanoWord.new("ABAB", "ab", @involution)
      expect(nanoword.to_s).to eq("ABAB:ab")
    end

    it "should yield each nanoword" do
      ary = ["AABB:aa", "AABB:ab", "AABB:ba", "AABB:bb",
             "ABAB:aa", "ABAB:ab", "ABAB:ba", "ABAB:bb",
             "ABBA:aa", "ABBA:ab", "ABBA:ba", "ABBA:bb"]
      ary.map! { |str| NPW::NanoWord.new(*str.split(":"), @involution) }
      ary.sort!
      ary2 = NPW::NanoWord.each_nanoword(@involution, :rank => 2).to_a.sort
      expect(ary2).to eq(ary)
    end

    it "should delete a letter" do
      nw = NPW::NanoWord.new("ABACBC", "abc", @involution2)
      expect(nw.transform(:delete => "B")).to eq(NPW::NanoWord.new("AABB", "ac", @involution2))
    end

    it "should exchange letters" do
      nw = NPW::NanoWord.new("ABACBC", "abc", @involution2)
      expect(nw.transform(:exchange => [0, 3, 4])).to eq(NPW::NanoWord.new("AABBCC", "bac", @involution2))
    end

    it "should returne projections" do
      nw = NPW::NanoWord.new("ABACBC", "abc", @involution2)
      expect(nw.project_letters(2, 3, 4)).to eq(["a", "c", "b"])
    end

    it "should shift" do
      expect(described_class.new("ABDCDCAB", "abab", @involution).transform(:shift => @involution).to_s).to eq("ABCBCDAD:babb")
    end

    it "should shift itself" do
      w = described_class.new("ABCADCDB", "abcd", @involution2)
      w.transform!(:shift => @involution3)
      expect(w.to_s).to eq("ABCDBDAC:bccd")
    end

    it "should return words by shift" do
      w = described_class.new("ABCDCADB", "aabb", @involution2)
      words = w.words_by_shift(@involution2)
      expect(words.size).to eq(w.gauss_word.to_s.size - 1)
      words.map! { |word| word.adjust_alphabet! }
      expect(w.transform(:shift => @involution2).adjust_alphabet).to eq(words[0])
      (1...(words.size)).each do |i|
        expect(words[i - 1].transform(:shift => @involution2).adjust_alphabet).to eq(words[i])
      end
    end
  end
end
