require "spec_helper"

describe NPW::NanoWord::Relation do
  before(:all) do
    @involution = { "a" => "b", "b" => "a", "c" => "d", "d" => "c" }
  end

  context "when converting string" do
    it "should convert to string" do
      rel = described_class.new("-ABC:abc", @involution)
      expect(rel.to_s).to eq("-ABC:abc")
    end

    it "should get convert to string" do
      rel = described_class.new("3AB:aa - 9BC:bb", @involution)
      expect(rel.to_s).to match(/3AB:aa\s*-9AB:bb/)
    end
  end

  context "when simplifying terms" do
    it "should unite two terms" do
      rel = described_class.new("3AB:ab - 9BC:ab", @involution)
      rel.simplify!
      expect(rel.to_s).to eq("6AB:ab")
    end

    it "should sort terms" do
      rel = described_class.new("3AC:bb - 9ABC:ccc", @involution)
      rel.simplify!
      expect(rel.to_s).to eq("9ABC:ccc -3AB:bb")
    end
  end

  context "when checking equality" do
    it "should return empty relation" do
      expect(described_class.new("@", @involution).empty?).to be_truthy
    end

    it "should raise error for empty string" do
      expect do
        described_class.new("", @involution)
      end.to raise_error
    end

    it "should return empty relation" do
      expect(described_class.new("3AB:ab - 3BC:ab", @involution, :simplify => true).empty?).to be_truthy
    end

    it "should return true" do
      rel1 = described_class.new("3AC:ab - 9ABC:aaa + 2EF:ab", @involution, :simplify => true)
      rel2 = described_class.new("- 9ABC:aaa + 1BD:ab + 4EF:ab", @involution, :simplify => true)
      expect(rel1).to eq(rel2)
    end
  end

  context "when eliminating a term" do
    it "should substitute" do
      rel1 = described_class.new("ABCBAC:abc +ABCACB:abc -ABCABC:abc +ABACBC:abc", @involution)
      rel2 = described_class.new("ABACBC:abc +ABAB:ab", @involution)
      expect(rel1.eliminate!(3, rel2)).to eq(described_class.new("ABCBAC:abc +ABCACB:abc -ABCABC:abc -ABAB:ab", @involution))
    end

    it "should eliminate without multiplying" do
      rel1 = described_class.new("9ABC:abc + 2AB:ab", @involution, :simplify => true)
      rel2 = described_class.new("3ABC:abc + 4ABCD:abcd", @involution, :simplify => true)
      expect(rel1.eliminate!(0, rel2)).to eq(described_class.new("12ABCD:abcd - 2AB:ab", @involution))
    end

    it "should eliminate with multiplying" do
      rel1 = described_class.new("6ABC:abc + 2AB:ab", @involution, :simplify => true)
      rel2 = described_class.new("4ABC:abc + 4ABCD:abcd", @involution, :simplify => true)
      expect(rel1.eliminate!(0, rel2)).to eq(described_class.new("12ABCD:abcd - 4AB:ab", @involution))
    end
  end

  context "when modifying terms" do
    it "should modify all terms" do
      rel = described_class.new("2A:a + 3BC:aa", @involution)
      rel.modify! do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("6A:a + 9BC:aa", @involution))
    end

    it "should modify terms specified by range" do
      rel = described_class.new("2A:a + 3BC:bc + 4CDD:bc + 5CCDD:cc", @involution)
      rel.modify!(:range => 1..2) do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("2A:a + 9BC:bc + 12CDD:bc + 5CCDD:cc", @involution))
    end

    it "should modify terms specified by a number" do
      rel = described_class.new("2A:a + 3BC:bb + 4CDD:cc + 5CCDD:cd", @involution)
      rel.modify!(:range => 1) do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("2A:a + 9BC:bb + 4CDD:cc + 5CCDD:cd", @involution))
    end

    it "should modify terms specified by an array" do
      rel = described_class.new("2A:b + 3BC:aa + 4CDD:bb + 5CCDD:cc", @involution)
      rel.modify!(:range => [1, 3]) do |trm|
        trm.mul(3)
      end
      expect(rel).to eq(described_class.new("2A:b + 9BC:aa + 4CDD:bb + 15CCDD:cc", @involution))
    end

    it "should delete terms" do
      rel = described_class.new("2AB:ab + 3BC:cc", @involution)
      rel.modify! do |trm|
        trm.delete
      end
      expect(rel.empty?).to be_truthy
    end

    it "should replace a term" do
      rel = described_class.new("2ABC:aaa + 3BC:bb", @involution)
      rel.modify!(:range => 1, :simplify => true) do |trm|
        trm.replace(NPW::NanoWord.new("ABC", "aaa", @involution))
      end
      expect(rel).to eq(described_class.new("5ABC:aaa", @involution))
    end

    it "should replace terms" do
      rel = described_class.new("1ABC:aaa + 4BC:ab - 3AA:c", @involution)
      rel.modify!(:simplify => { :adjust => true }) do |trm|
        trm.replace(NPW::NanoWord.new("BA", "cc", @involution))
      end
      expect(rel).to eq(described_class.new("2AB:cc", @involution))
    end
  end

  context "when deleting duplicated relations" do
    it "should delete by Array#uniq!" do
      ary = [described_class.new("2AB:aa + 3ACD:bbb", @involution), described_class.new("2BC:aa + 3CAD:bbb", @involution)]
      ary.each { |rel| rel.simplify! }
      expect(ary.uniq.size).to eq(1)
    end

    it "should not delete by Array#uniq!" do
      ary = [described_class.new("2AB:aa + 3ACD:bcd", @involution), described_class.new("2BC:aa + 3CAD:bbb", @involution)]
      ary.each { |rel| rel.simplify! }
      expect(ary.uniq.size).to eq(2)
    end
  end
end
