require "spec_helper"
require "filename"
require "matrix"
require "npw/application/polyak_algebra/matrix_db"

describe NPW::MatrixDB do
  before(:all) do
    @directory = "matrix_db.tmp"
    FileUtils.mkdir(@directory)
  end

  after(:all) do
    FileUtils.rm_r(@directory)
  end

  shared_examples_for "shared_tests" do
    it "should raise error" do
      path = get_new_path
      expect do
        described_class.new(path)
      end.to raise_error
    end

    it "should create new matrix database" do
      path = get_new_path
      m = described_class.new(path, :type => :row, :row => 3, :column => 2)
      expect(m.type).to eq(:row)
      expect(m.row_size).to eq(3)
      expect(m.column_size).to eq(2)
    end

    it "should set row" do
      path = get_new_path
      m = described_class.new(path, :type => :row, :row => 3, :column => 2)
      m.open("w")
      expect(m.row(0)).to eq([])
      m.row_set(0, [[0, 10], [1, 20]])
      expect(m.row(0)).to eq([[0, 10], [1, 20]])
      m.close
    end

    it "should set column" do
      path = get_new_path
      m = described_class.new(path, :type => :column, :column => 3, :row => 2)
      m.open("w")
      expect(m.column(0)).to eq([])
      m.column_set(0, [[1, 10], [2, 20]])
      expect(m.column(0)).to eq([[1, 10], [2, 20]])
      m.close
    end

    it "should get elements of row data type" do
      m = described_class.new(get_new_path, :type => :row, :row => 2, :column => 3)
      m.open("w") do |mat|
        mat.row_set(0, [[1, 10], [2, 20]])
        mat.row_set(1, [[0, -1], [1, -2]])
      end
      res = [[0, 10, 20], [-1, -2, 0]]
      m.open("r") do |mat|
        mat.row_size.times do |row_num|
          mat.column_size.times do |column_num|
            expect(mat[row_num, column_num]).to eq(res[row_num][column_num])
          end
        end
      end
    end

    it "should get elements of column data type" do
      m = described_class.new(get_new_path, :type => :column, :column => 2, :row => 3)
      m.open("w") do |mat|
        mat.column_set(0, [[1, 10], [2, 20]])
        mat.column_set(1, [[0, -1], [1, -2]])
      end
      res = [[0, -1], [10, -2], [20, 0]]
      m.open("r") do |mat|
        mat.row_size.times do |row_num|
          mat.column_size.times do |column_num|
            expect(mat[row_num, column_num]).to eq(res[row_num][column_num])
          end
        end
      end
    end

    [:row, :column].each do |type|
      it "should change data type" do
        m = described_class.new(get_new_path, :type => type, :column => 3, :row => 4)
        m.set([[-2, 0, -1], [0, 0, 3], [0, 8, 0], [2, -3, -9]])
        m2 = m.change_type(get_new_path)
        expect(m2.type).to eq(type == :row ? :column : :row)
        expect(m2.valid?).to be_truthy
        m.open("r")
        m2.open("r")
        m.row_size.times do |row_num|
          m.column_size.times do |column_num|
            expect(m[row_num, column_num]).to eq(m2[row_num, column_num])
          end
        end
        m.close
        m2.close
      end

      it "should debug print" do
        m = described_class.new(get_new_path, :type => type, :column => 3, :row => 4)
        m.set([[-2, 0, -1], [0, 0, 3], [0, 8, 0], [2, -3, -9]])
        expect(m.debug_print(nil)).to eq <<PRINT
-2 0 -1
0 0 3
0 8 0
2 -3 -9
PRINT
      end
    end

    it "should return true for an identity matrix" do
      m = described_class.new(get_new_path, :type => :column, :column => 3, :row => 3)
      m.set([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
      expect(m.identity?).to be_truthy
    end

    it "should return false for a non-identity matrix" do
      m = described_class.new(get_new_path, :type => :column, :column => 3, :row => 3)
      m.set([[1, 0, 0], [0, 0, 1], [0, 0, 1]])
      expect(m.identity?).to be_falsey
    end

    it "should dump for GAP" do
      path = get_new_path
      m = described_class.new(path, :type => :row, :row => 4, :column => 3)
      m.open("w")
      m.row_set(0, [[1, -1], [2, -3]])
      m.row_set(2, [[0, 3]])
      m.row_set(3, [[1, 10]])
      m.close
      expect(m.to_gap_string).to eq("[[0,-1,-3],[0,0,0],[3,0,0],[0,10,0]]")
    end

    it "should dump for sparse_snf" do
      path_text = File.join(@directory, "dump_sparse_snf.txt")
      m = described_class.new(get_new_path, :type => :row, :column => 3, :row => 4)
      m.set([[-2, 0, -1], [0, 0, 3], [0, 8, 0], [2, -3, -9]])
      Kernel.open(path_text, "w") do |f|
        m.dump_for_sparse_snf(f)
      end
      m2 = described_class.load_text_for_sparse_snf(get_new_path, path_text)
      expect(m2.type).to eq(m.type)
      expect(m2.modulo).to eq(m.modulo)
      expect(m2.valid?).to be_truthy
      m.open("r")
      m2.open("r")
      m.row_size.times do |row_num|
        m.column_size.times do |column_num|
          expect(m[row_num, column_num]).to eq(m2[row_num, column_num])
        end
      end
      m.close
      m2.close
    end

    [[[[2, 7], [-2, -3]],
      [[3, -2, 1], [9, 2, 0]]],
     [[[0, 1, 3, 0, 2], [0, -2, 0, -7, 0]],
      [[0, 1, 2], [-3, -2, 1], [0, 0, 8], [1, 0, 3], [2, 0, 0]]],
     [[[0, 1, 0, 3, 0, 2], [0, -2, 0, 0, -7, 0]],
      [[-3, 0, 0, 9], [0, 1, 2, 0], [-3, 0, -2, 1], [0, 0, 8, -2], [1, 0, 3, 0], [2, 0, -8, 0]]],
     [eval(File.read(File.join(File.dirname(__FILE__), "matrix_data/matrix1.txt"))),
      eval(File.read(File.join(File.dirname(__FILE__), "matrix_data/matrix2.txt")))]
    ].each_with_index do |data, i|
      it "should be multiplied by other matrix (#{i})" do
        data1, data2 = data
        mat1 = Matrix[*data1]
        mat2 = Matrix[*data2]
        res = mat1 * mat2
        db1 = described_class.new(get_new_path, :type => :row, :row => mat1.row_size, :column => mat1.column_size)
        db2 = described_class.new(get_new_path, :type => :column, :row => mat2.row_size, :column => mat2.column_size)
        pending "Not implemented" if db1.kyotocabinet_on_memory?
        db1.set(data1)
        db2.set(data2)
        db = db1.mul(db2, get_new_path)
        db.open("r") do |m|
          m.row_size.times do |row_num|
            m.column_size.times do |column_num|
              expect(m[row_num, column_num]).to eq(res[row_num, column_num])
            end
          end
        end
      end
    end

    it "should be multiplied by other matrix" do
      m1 = described_class.new(get_new_path, :type => :row, :row => 2, :column => 3)
      pending "Not implemented" if m1.kyotocabinet_on_memory?
      m1.open("w") do |mat|
        mat.row_set(0, [[1, -1], [2, -3]])
        mat.row_set(1, [[0, 3], [1, 1]])
      end
      m2 = described_class.new(get_new_path, :type => :column, :row => 3, :column => 2)
      m2.open("w") do |mat|
        mat.column_set(0, [[0, 2], [1, 1], [2, 1]])
        mat.column_set(1, [[0, 2], [1, -1]])
      end
      m = m1.mul(m2, get_new_path)
      m.open("r") do |mat|
        expect(m.row(0)).to eq([[0, -4], [1, 1]])
        expect(m.row(1)).to eq([[0, 7], [1, 5]])
      end
    end

    it "should be true for an upper triangular matrix" do
      m = described_class.new(get_new_path, :type => :row, :row => 3, :column => 3)
      m.open("w") do |mat|
        mat.row_set(0, [[0, 10], [1, -1], [2, -3]])
        mat.row_set(1, [[2, -3]])
        mat.row_set(2, [[2, 10]])
      end
      expect(m.upper_triangular?).to be_truthy
    end

    it "should be false for a matrix that is not upper triangular" do
      m = described_class.new(get_new_path, :type => :row, :row => 3, :column => 3)
      m.open("w") do |mat|
        mat.row_set(0, [[0, 10], [1, -1], [2, -3]])
        mat.row_set(1, [[0, -3]])
        mat.row_set(2, [[2, 10]])
      end
      expect(m.upper_triangular?).to be_falsey
    end

    it "should get inverse matrix (1)" do
      m = described_class.new(get_new_path, :type => :row, :row => 3, :column => 3)
      pending "Not implemented" if m.kyotocabinet_on_memory?
      m.open("w") do |mat|
        mat.row_set(0, [[0, 1]])
        mat.row_set(1, [[1, 1]])
        mat.row_set(2, [[2, 1]])
      end
      inv = m.inverse_of_upper_triangular(get_new_path)
      inv.open("r") do |mat|
        expect(mat.column(0)).to eq([[0, 1]])
        expect(mat.column(1)).to eq([[1, 1]])
        expect(mat.column(2)).to eq([[2, 1]])
      end
    end

    it "should get inverse matrix (2)" do
      m = described_class.new(get_new_path, :type => :row, :row => 3, :column => 3)
      pending "Not implemented" if m.kyotocabinet_on_memory?
      m.open("w") do |mat|
        mat.row_set(0, [[0, 1], [1, -1], [2, -3]])
        mat.row_set(1, [[1, 1], [2, 3]])
        mat.row_set(2, [[2, 1]])
      end
      inv = m.inverse_of_upper_triangular(get_new_path)
      e = m.mul(inv, get_new_path)
      e.open("r") do |mat|
        expect(mat.row(0)).to eq([[0, 1]])
        expect(mat.row(1)).to eq([[1, 1]])
        expect(mat.row(2)).to eq([[2, 1]])
      end
    end

    it "should get inverse matrix (3)" do
      m = described_class.new(get_new_path, :type => :row, :row => 3, :column => 3)
      pending "Not implemented" if m.kyotocabinet_on_memory?
      m.open("w") do |mat|
        mat.row_set(0, [[0, 1], [1, 10]])
        mat.row_set(1, [[1, 1], [2, -5]])
        mat.row_set(2, [[2, 1]])
      end
      inv = m.inverse_of_upper_triangular(get_new_path)
      e = m.mul(inv, get_new_path)
      e.open("r") do |mat|
        expect(mat.row(0)).to eq([[0, 1]])
        expect(mat.row(1)).to eq([[1, 1]])
        expect(mat.row(2)).to eq([[2, 1]])
      end
    end
  end

  context "when using file database" do
    before(:all) do
      @filename = FileName.new(File.join(@directory, "test.kch"), :directory => :parent, :position => :middle, :add => :always)
    end

    def get_new_path
      @filename.create
    end

    it_should_behave_like "shared_tests"

    it "should load matrix database" do
      path = get_new_path
      m = described_class.new(path, :type => :row, :row => 3, :column => 2)
      m2 = described_class.new(path, :load => true)
      expect(m2.type).to eq(:row)
      expect(m2.row_size).to eq(3)
      expect(m2.column_size).to eq(2)
    end

    describe NPW::MatrixDB::Cache do
      before(:all) do
        @path = get_new_path
        @matrix = NPW::MatrixDB.new(@path, :type => :row, :row => 3, :column => 3)
        @matrix.set([[1, -2, 2], [0, 1, -3], [0, 0, 1]])
        @path2 = get_new_path
        @matrix2 = NPW::MatrixDB.new(@path2, :type => :row, :row => 3, :column => 3)
        @matrix2.set([[1, -2, 2], [0, 1, -3], [0, 3, 1]])
      end

      it "should print for debug" do
        cache = NPW::MatrixDB::Cache.new(@path, @matrix.row_size)
        expect do
          cache.debug_print
        end.not_to raise_error
      end

      it "should return true" do
        cache = NPW::MatrixDB::Cache.new(@path, @matrix.row_size)
        expect(cache.upper_triangular_diagonal_ones?).to be_truthy
      end

      it "should return false" do
        cache = NPW::MatrixDB::Cache.new(@path2, @matrix2.row_size)
        expect(cache.upper_triangular_diagonal_ones?).to be_falsey
      end
    end
  end

  context "when using cache database" do
    def get_new_path
      "*"
    end

    it_should_behave_like "shared_tests"

    it "should raise error when we try to road cache database" do
      expect do
        described_class.new("*", :load => true)
      end.to raise_error
    end
  end
end
