require "spec_helper"
require "npw/application"

describe NPW::TruncatedPolyakAlgebra::Index::ZeroDB do
  before(:all) do
    @path = FileName.create(File.dirname(__FILE__), "zero_db_tmp.kch")
    @db = described_class.new(@path)
    @db.open_write {}
  end

  subject do
    @db
  end

  context "when setting and getting words" do
    it "should get nil" do
      subject.open_read do |db|
        expect(subject.rank_of_relation("EMPTY")).to be_nil
      end
    end

    it "should set" do
      w = "AA"
      n = 10
      subject.open_write do |db|
        subject.set(w, n)
        expect(subject.rank_of_relation(w)).to eq(n)
      end
    end

    it "should set new value" do
      w = "AB"
      n = 10
      subject.open_write do |db|
        subject.set(w, 0)
        subject.set(w, n)
        expect(subject.rank_of_relation(w)).to eq(n)
      end
    end
  end

  after(:all) do
    FileUtils.rm(@path)
  end
end

describe NPW::TruncatedPolyakAlgebra::Index::EqualityDB::WordToNumberDB do
  before(:all) do
    @path = FileName.create(File.dirname(__FILE__), "wn_db_tmp.kch")
    @db = described_class.new(@path)
    @db.open_write {}
  end

  subject do
    @db
  end

  it "should get nil" do
    subject.open_read do |db|
      expect(subject.get("EMPTY")).to be_nil
    end
  end

  it "should get a number" do
    subject.open_write do |db|
      w = "AA"
      n = 10
      nums = subject.set(w, n)
      expect(nums.size).to eq(2)
      expect(nums[1]).to eq(n)
      expect(nums[0]).to be_an_instance_of Fixnum
      expect(subject.get(w)).to eq(nums)
    end
  end

  it "should change a number" do
    subject.open_write do |db|
      w = "AAA"
      n = 100
      nums = subject.set(w, 10)
      nums = subject.set(w, 100)
      expect(nums.size).to eq(2)
      expect(nums[1]).to eq(n)
      expect(nums[0]).to be_an_instance_of Fixnum
      expect(subject.get(w)).to eq(nums)
    end
  end

  it "should return distinct numbers" do
    subject.open_write do |db|
      w1 = "AAABBB"
      w2 = "AAACCC"
      n = 10
      nums1 = subject.set(w1, n)
      nums2 = subject.set(w2, n)
      expect(nums1[1]).to eq(n)
      expect(nums2[1]).to eq(n)
      expect(nums1[0]).not_to eq(nums2[0])
    end
  end

  it "should remove" do
    subject.open_write do |db|
      w = "AB"
      nums = subject.set(w, 11)
      subject.remove(w)
      expect(subject.get(w)).to be_nil
    end
  end

  it "should remove by number" do
    subject.open_write do |db|
      w = "AC"
      nums = subject.set(w, 12)
      expect(subject.remove_by_number(nums[0])).to eq(w)
      expect(subject.get(w)).to be_nil
    end
  end

  after(:all) do
    FileUtils.rm(@path)
  end
end

describe NPW::TruncatedPolyakAlgebra::Index::EqualityDB::ClassToRepresentativeDB do
  before(:all) do
    @path = FileName.create(File.dirname(__FILE__), "cr_db_tmp.kch")
    @db = described_class.new(@path)
    @db.open_write {}
  end

  subject do
    @db
  end

  it "should get nil" do
    subject.open_read do |db|
      expect(subject.get(10)).to be_nil
    end
  end

  it "should register" do
    subject.open_write do |db|
      w = "AA"
      n = subject.register(w)
      expect(n).to be_an_instance_of Fixnum
      expect(subject.get(n)).to eq(w)
    end
  end

  it "should set" do
    subject.open_write do |db|
      w = "AB"
      n = 100
      subject.set(n, w)
      expect(subject.get(n)).to eq(w)
    end
  end

  it "should remove" do
    subject.open_write do |db|
      w = "AC"
      n = 100
      subject.set(n, w)
      subject.remove(n)
      expect(subject.get(n)).to be_nil
    end
  end

  it "should renew word" do
    subject.open_write do |db|
      w1 = "AE"
      w2 = "AD"
      n = subject.register(w1)
      subject.renew(n, w2)
      expect(subject.get(n)).to eq(w2)
    end
  end

  it "should not renew word" do
    subject.open_write do |db|
      w1 = "AF"
      w2 = "AG"
      n = subject.register(w1)
      subject.renew(n, w2)
      expect(subject.get(n)).to eq(w1)
    end
  end

  after(:all) do
    FileUtils.rm(@path)
  end
end

describe NPW::TruncatedPolyakAlgebra::Index::EqualityDB::ClassToWordsDB do
  before(:all) do
    @path = FileName.create(File.dirname(__FILE__), "cw_db_tmp.kch")
    @db = described_class.new(@path)
    @db.open_write {}
  end

  subject do
    @db
  end

  it "should get nil" do
    subject.open_read do |db|
      expect(subject.get(10)).to be_nil
    end
  end

  it "should set" do
    subject.open_write do |db|
      n = 11
      nums = [2, 4, 5, 8]
      subject.set(n, nums)
      expect(subject.get(n)).to eq(nums)
    end
  end

  it "should add" do
    subject.open_write do |db|
      n = 12
      nums = [2, 4]
      num = 8
      subject.set(n, nums)
      subject.add(n, num)
      nums << num
      expect(subject.get(n)).to eq(nums)
    end
  end

  it "should remove" do
    subject.open_write do |db|
      n = 13
      nums = [2, 4, 5, 8]
      subject.set(n, nums)
      subject.remove(n)
      expect(subject.get(n)).to be_nil
    end
  end

  it "should get and remove" do
    subject.open_write do |db|
      n = 14
      nums = [2, 4, 5, 8]
      subject.set(n, nums)
      expect(subject.get_remove(n)).to eq(nums)
      expect(subject.get(n)).to be_nil
    end
  end

  it "should yield each entries" do
    subject.open_write do |db|
      subject.set(20, [1, 2, 3])
      subject.each do |num_class, nums_word|
        expect(num_class).to be_an_instance_of Fixnum
        expect(nums_word).to be_an_instance_of Array
        expect(nums_word.all? { |n| Fixnum === n }).to be_truthy
      end
    end
  end

  after(:all) do
    FileUtils.rm(@path)
  end
end

describe NPW::TruncatedPolyakAlgebra::Index::EqualityDB do
  before(:all) do
    @dir = FileName.create(File.dirname(__FILE__), "eq_dir_tmp", :directory => :self)
    @db = described_class.new(File.join(@dir, "db1.kch"), File.join(@dir, "db2.kch"), File.join(@dir, "db3.kch"))
    @db.open_write {}
  end

  subject do
    @db
  end

  it "should return nil" do
    subject.open_read do |db|
      expect(subject.get_representative_word("AA")).to be_nil
    end
  end

  it "should return a word" do
    subject.open_write do |db|
      subject.register_pair("AAA", "ABC")
      expect(subject.get_representative_word("AAA")).to be_nil
      expect(subject.get_representative_word("ABC")).to eq("AAA")
      expect(subject.get_word_class("AAA").sort).to eq(["AAA", "ABC"])
      expect(subject.get_word_class("ABC").sort).to eq(["AAA", "ABC"])
    end
  end

  it "should add a word to a class" do
    subject.open_write do |db|
      subject.register_pair("BAA", "BAC")
      subject.register_pair("BAC", "BABB")
      expect(subject.get_representative_word("BAA")).to be_nil
      expect(subject.get_representative_word("BAC")).to eq("BAA")
      expect(subject.get_representative_word("BABB")).to eq("BAA")
      expect(subject.get_word_class("BAA").sort).to eq(["BAA", "BAC", "BABB"].sort)
      expect(subject.get_word_class("BAC").sort).to eq(["BAA", "BAC", "BABB"].sort)
      expect(subject.get_word_class("BABB").sort).to eq(["BAA", "BAC", "BABB"].sort)
    end
  end

  it "should add a word to a class" do
    subject.open_write do |db|
      subject.register_pair("BBC", "BBBB")
      subject.register_pair("BBA", "BBC")
      expect(subject.get_representative_word("BBA")).to be_nil
      expect(subject.get_representative_word("BBC")).to eq("BBA")
      expect(subject.get_representative_word("BBBB")).to eq("BBA")
      expect(subject.get_word_class("BBA").sort).to eq(["BBA", "BBC", "BBBB"].sort)
      expect(subject.get_word_class("BBC").sort).to eq(["BBA", "BBC", "BBBB"].sort)
      expect(subject.get_word_class("BBBB").sort).to eq(["BBA", "BBC", "BBBB"].sort)
    end
  end

  it "should concatenate two word classes" do
    subject.open_write do |db|
      subject.register_pair("BCA", "BCB")
      subject.register_pair("BCC", "BCD")
      subject.register_pair("BCC", "BCB")
      expect(subject.get_representative_word("BCA")).to be_nil
      expect(subject.get_representative_word("BCB")).to eq("BCA")
      expect(subject.get_representative_word("BCC")).to eq("BCA")
      expect(subject.get_representative_word("BCD")).to eq("BCA")
      expect(subject.get_word_class("BCA").sort).to eq(["BCA", "BCB", "BCC", "BCD"].sort)
      expect(subject.get_word_class("BCB").sort).to eq(["BCA", "BCB", "BCC", "BCD"].sort)
      expect(subject.get_word_class("BCC").sort).to eq(["BCA", "BCB", "BCC", "BCD"].sort)
      expect(subject.get_word_class("BCD").sort).to eq(["BCA", "BCB", "BCC", "BCD"].sort)
    end
  end

  it "should add a word to a class and change a representative word" do
    subject.open_write do |db|
      subject.register_pair("CCC", "CCBB")
      expect(subject.get_representative_word("CCBB")).to eq("CCC")
      subject.register_pair("CCB", "CCC")
      expect(subject.get_representative_word("CCBB")).to eq("CCB")
    end
  end

  after(:all) do
    FileUtils.rm_r(@dir)
  end
end
