require "spec_helper"
require "npw/application"

describe NPW::TruncatedPolyakAlgebra::WordClassify::MinWordDB do
  subject do
    described_class.new("test_min_word_db.kch")
  end

  before(:all) do
    subject.open_write do
    end
  end

  it "should get nil" do
    subject.open_read do
      expect(subject.get("AA")).to be_nil
    end
  end

  it "should set an integer" do
    subject.open_write do
      subject.set("ABC", 10)
      expect(subject.get("ABC")).to eq(10)
      subject.kyotocabinet_db.delete("ABC")
    end
  end

  it "should set bulk" do
    subject.open_write do
      words = ["BB", "BC", "BD"]
      subject.set_bulk(words, 125)
      words.each do |word|
        expect(subject.get(word)).to eq(125)
      end
      words.each do |word|
        subject.kyotocabinet_db.delete(word)
      end
    end
  end

  it "should yield each words" do
    words = ["CA", "CAB", "CDD"]
    subject.open_write do
      subject.set_bulk(words, 200)
    end
    words_dup = words.dup
    subject.open_read do
      subject.each do |word_str, eq_class_packed_num|
        expect(words_dup.delete(word_str)).to eq(word_str)
      end
    end
    expect(words_dup).to be_empty
    subject.open_write do
      words.each do |word|
        subject.kyotocabinet_db.delete(word)
      end
    end
  end

  after(:all) do
    FileUtils.rm_r(subject.get_kyotocabinet_path)
  end
end

describe NPW::TruncatedPolyakAlgebra::WordClassify::EquivalenceDB do
  subject do
    described_class.new("test_equivalence_db.kch")
  end

  before(:all) do
    subject.open_write do
    end
  end

  it "should get nil" do
    subject.open_read do
      expect(subject.get(100)).to be_nil
    end
  end

  it "should register words" do
    subject.open_write do
      n = subject.register("ABC")
      expect(subject.get(n)).to eq("ABC")
      n2 = subject.register("ABCD")
      expect(n2).to eq(n + 1)
      expect(subject.get(n2)).to eq("ABCD")
      subject.remove_bulk_by_number([n, n2])
      expect(subject.get(n)).to be_nil
      expect(subject.get(n2)).to be_nil
    end
  end

  it "should yield each word" do
    words = ["BA", "BB", "BC"]
    eq_class_packed_nums = []
    subject.open_write do
      words.each do |word_str|
        subject.register(word_str)
      end
    end
    subject.open_read do
      subject.each do |eq_class_packed_num, reduced_word_str|
        eq_class_packed_nums << eq_class_packed_num
      end
    end
    expect(eq_class_packed_nums.size).to eq(words.size)
    subject.open_write do
      subject.remove_bulk(eq_class_packed_nums)
    end
  end

  after(:all) do
    FileUtils.rm_r(subject.get_kyotocabinet_path)
  end
end

describe NPW::TruncatedPolyakAlgebra::WordClassify::DB do
  subject do
    described_class.new("test_word_classify_db.tmp")
  end

  before(:all) do
    FileUtils.mkdir("test_word_classify_db.tmp")
    @all_words = ["ABAB", "ABBA", "@"]
    subject.open(:write) do |db|
      n = db.equivalence_number("@")
      db.set_reduced_words(@all_words, n)
    end
  end

  it "should yield for each reduced word" do
    words = ["@"]
    subject.open(:read) do |db|
      db.each_reduced_word do |reduced_word_str|
        expect(words.delete(reduced_word_str)).to be_truthy
      end
    end
    expect(words).to be_empty
  end

  it "should yield for each reduced word" do
    words = @all_words.dup
    subject.open(:read) do |db|
      db.each_word_and_reduced do |word_str, reduced_word_str|
        expect(words.delete(word_str)).to be_truthy
      end
    end
    expect(words).to be_empty
  end

  after(:all) do
    FileUtils.rm_r("test_word_classify_db.tmp")
  end
end
