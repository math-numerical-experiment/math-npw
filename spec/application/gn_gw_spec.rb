require "spec_helper"

require "npw/application"

describe NPW::GnOnGaussWord do
  context "when we deal with normal Gauss words" do
    subject do
      described_class.new("TestGnOnGW")
    end

    let :subject_extended do
      described_class.new("TestGnOnGWExtended", :classification_moves => :extended)
    end

    context "when creating second type relations" do
      it "should return a relation of second type" do
        word = NPW::Word.new("ABCBAC")
        rels = subject.relations_2nd_from_one_word(word)
        expect(rels.size).to eq(1)
        expect(rels[0].simplify!).to eq(NPW::Word::Relation.new("ABCBAC + ACAC + BCBC").simplify!)
      end

      it "should return two relations of second type" do
        word = NPW::Word.new("ABCDBADC")
        rels = subject.relations_2nd_from_one_word(word)
        expect(rels.size).to eq(2)
        [NPW::Word::Relation.new("ABCDBADC + ACDADC + BCDBDC"), NPW::Word::Relation.new("ABCDBADC + ABCBAC + ABDBAD")].each do |rel|
          rel.simplify!
          expect(rels).to include(rel)
        end
      end
    end

    context "when creating third type relations" do
      it "should return a relation of third type" do
        word = NPW::Word.new("ABDACDBC")
        rels = subject.relations_3rd_from_one_word(word)
        expect(rels.size).to eq(1)
        expect(rels[0].simplify!).to eq(NPW::Word::Relation.new("ABDACDBC + BDCDBC + ADACDC + ABDADB - BADCADCB - BDCDCB - ADCADC - BADADB").simplify!)
      end
    end

    context "when creating non trivial gauss words" do
      it "should return words of rank 2" do
        ary = ["ABAB"]
        expect(subject.non_zero_gauss_word(2).to_a.map(&:to_s).sort).to eq(ary)
      end

      it "should return words of rank 3" do
        ary = ["ABACBC", "ABCABC", "ABCACB", "ABCBAC", "ABCBCA"].sort
        expect(subject.non_zero_gauss_word(3).to_a.map(&:to_s).sort).to eq(ary)
      end
    end

    context "when investigating homotopy class" do
      it "should get a three class that has only the original word." do
        w = NPW::Word.new("ABCABC")
        expect(subject.three_class(w)).to eq([w])
      end

      it "should get a three class" do
        res = ["ABACBC", "BACACB"].map { |str| NPW::Word.new(str).adjust_alphabet! }.sort!
        expect(subject.three_class(NPW::Word.new("ABACBC"))).to eq(res)
      end

      it "should get same three classes by extended moves" do
        w1 = NPW::Word.new("ABCDBDEACE")
        w2 = NPW::Word.new("ABCDBECAED")
        expect(subject_extended.three_class(w1).sort).to eq(subject_extended.three_class(w2).sort)
      end

      it "should get no reduced words" do
        w = NPW::Word.new("ABACBC")
        expect(subject.words_by_reduced_move(w)).to be_empty
      end

      it "should get reduced words" do
        res = ["ACCA", "ADBABD"].map { |str| NPW::Word.new(str).adjust_alphabet! }.sort!
        expect(subject.words_by_reduced_move(NPW::Word.new("ADBCCABD"))).to eq(res)
      end
    end

    context "when we classify normal Gauss words" do
      it "should save classification" do
        path = subject.word_classify.save_classification(3, "test_class_00.tmp")
        expect(File.exist?(path)).to be_truthy
        classification = NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(path).dump
        [["@", "AA", "AABB", "AABBCC", "AABCCB", "ABBA", "ABBACC",
          "ABCACB", "ABCBAC", "ABBCCA", "ABCCBA"],
         ["ABAB", "AABCBC", "ABABCC", "ABACCB", "ABBCAC", "ABCCAB"],
         ["ABACBC", "ABCBCA"], ["ABCABC"]].each do |words|
          w = NPW::Word.new(words[0])
          classification.each do |words_cl|
            if words_cl.include?(w)
              expect(words_cl.map(&:to_s).sort).to eq(words.sort)
              break
            end
          end
        end
        FileUtils.rm_r(path)
      end

      it "should save classification" do
        path = subject_extended.word_classify.save_classification(3, "test_class_01.tmp")
        expect(File.exist?(path)).to be_truthy
        classification = NPW::TruncatedPolyakAlgebra::WordClassify::DB.new(path).dump
        expect(classification.size).to eq(1)
        words = ["@", "AA", "AABB", "AABBCC", "AABCCB", "ABBA", "ABBACC",
                 "ABCACB", "ABCBAC", "ABBCCA", "ABCCBA",
                 "ABAB", "AABCBC", "ABABCC", "ABACCB", "ABBCAC", "ABCCAB",
                 "ABACBC", "ABCBCA", "ABCABC"]
        expect(classification[0].map(&:to_s).sort).to eq(words.sort)
        FileUtils.rm_r(path)
      end
    end

    it "should have third type relation" do
      ary = subject.files_enum_relations.values.sort
      expect(ary).to eq(["each_2nd_relations", "each_3rd_relations"].sort)
    end
  end

  context "when we do not use third type relation" do
    subject do
      described_class.new("TestGnOnGWIgnore3rd", :ignore_3rd_relations => true)
    end

    it "should not have third type relation" do
      ary = subject.files_enum_relations.values.sort
      expect(ary).to eq(["each_2nd_relations"])
    end
  end

  context "when we use effective way" do
    subject do
      NPW::GnOnGaussWord.new("TestGnOnGaussWordEffectively", :effective => true)
    end

    it "should not have third type relation" do
      ary = subject.files_enum_relations.values
      expect(ary).to eq(["each_2nd_relations", "each_3rd_relations", "each_additional_relations"].sort)
    end

    # TODO: test each_additional_relations
  end

  context "when we use effective way without third type relation" do
    subject do
      NPW::GnOnGaussWord.new("TestGnOnGaussWordEffectivelyNo3rd", :ignore_3rd_relations => true, :effective => true)
    end

    it "should not have third type relation" do
      ary = subject.files_enum_relations.values
      expect(ary).to eq(["each_2nd_relations", "each_additional_relations"].sort)
    end
  end

  context "when we use shift moves" do
    subject do
      NPW::GnOnClosedGaussWord.new("TestGnOnClosedGaussWord", :effective => true)
    end

    context "when creating relations of shift moves" do
      it "should return a relation of shift moves" do
        word = NPW::Word.new("ABACBC")
        rels = subject.relations_shift_move(word)
        expect(rels.size).to eq(1)
        expect(rels[0].simplify!).to eq(NPW::Word::Relation.new("ABACBC - BACBCA").simplify!)
      end
    end

    it "should not have third type relation" do
      ary = subject.files_enum_relations.values
      expect(ary).to eq(["each_2nd_relations", "each_3rd_relations", "each_additional_relations", "each_shift_relations"].sort)
    end
  end

  context "when we use effective way without third type relation" do
    subject do
      NPW::GnOnClosedGaussWord.new("TestGnOnClosedGaussWordNo3rd", :ignore_3rd_relations => true, :effective => true)
    end

    it "should not have third type relation" do
      ary = subject.files_enum_relations.values
      expect(ary).to eq(["each_2nd_relations", "each_additional_relations", "each_shift_relations"].sort)
    end
  end
end
